import "./assets/libs/boxicons-2.1.1/css/boxicons.min.css";
import "./scss/app.scss";
import "boxicons";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./layouts/Layout";
import Login from "./pages/authorization/Login";
import Register from "./pages/authorization/Register";
import ResetPassword from "./pages/authorization/ResetPassword";
import Feed from "./components/feed/Feed";
import TwoStepVerification from "./pages/authorization/TwoStepVerification";
// import BlankPage from "./pages/BlankPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/auth/login" element={<Login />} />
        <Route path="/auth/register" element={<Register />} />
        <Route path="/auth/reset-password" element={<ResetPassword />} />
        <Route path="/auth/two-step-verification" element={<TwoStepVerification />} />
        <Route path="/" element={<Layout />}>
          <Route index element={<Feed />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
