import React, { useContext } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import "./boxNotification.scss";
import Notification from "./notification/Notification";
import "./boxNotification.scss";

const BoxNotification = ({ show }) => {
  // Auth Context
  const {
    state: { user, listNotifications },
    seenNotification
  } = useContext(AuthContext);

  const handleSeenNotification = (notificationId) => {
    seenNotification(notificationId);
  }

  return (
    <>
      {user && (
        <div className={`box-notification ${show === true ? "show" : ""}`}>
          {listNotifications.map((notification, index) => (
            <Notification
              notification={notification}
              key={index}
              handleSeenNotification={handleSeenNotification}
            />
          ))}
        </div>
      )}
    </>
  );
};

export default BoxNotification;
