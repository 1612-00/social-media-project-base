import React, { useState } from "react";
import { useEffect } from "react";
import logo from "../../../assets/imgs/logo.png";
import { detailUserById } from "../../../controllers/UserController";
import "./notification.scss";
import { format } from "timeago.js";

const Notification = ({ notification, handleSeenNotification }) => {
  const [sender, setSender] = useState(null);

  useEffect(() => {
    detailUserById(notification.sender.object.toString(), setSender);
  }, []);
  return (
    <div className="notification" onClick={() => handleSeenNotification(notification._id.toString())}>
      <div className="notification__image">
        <img
          src={
            sender &&
            sender.avatars &&
            sender.avatars.length > 0 &&
            sender.avatars.find((avatar) => avatar.isActive)
              ? sender.avatars.find((avatar) => avatar.isActive).url
              : logo
          }
          alt=""
        />
      </div>
      <div className="notification__content">
        <div className="notification__content__text">
          {`${sender && sender.fullName} ${notification.type}`}
          <div
            className={`notification__content__createdAt ${
              notification.seen ? "" : "notSeen"
            }`}
          >
            {format(notification.updatedAt)}
          </div>
        </div>
        {notification.seen ? null : (
          <div className="notification__content__blue-dot"></div>
        )}
      </div>
    </div>
  );
};

export default Notification;
