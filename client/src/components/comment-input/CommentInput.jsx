import React, { useEffect, useState } from "react";
import logo from "../../assets/imgs/logo.png";
import "./commentInput.scss";

const CommentInput = ({
  socket,
  userId,
  post,
  comment = null,
  handleSendNotification
}) => {
  const [commentValue, setCommentValue] = useState("");
  const [currUser, setCurrUser] = useState({});

  // // Auth Context
  // const {
  //   state: { user }
  // } = useContext(AuthContext);

  let timeout;
  const handleChangeInput = (event) => {
    setCommentValue(event.target.value);
    emitTyping(true);
    timeout = setTimeout(() => {
      emitTyping(false);
    }, 2000);
  };

  const getUserById = (author) => {
    socket.emit("getUserById", { userId: author }, (response) => {
      setCurrUser(response);
    });
  };

  const emitTyping = (isTyping) => {
    socket.emit("typing", {
      userId: userId,
      postId: post._id.toString(),
      status: isTyping
    });
  };

  const handleCreateComment = (event) => {
    event.preventDefault();
    socket.emit(
      "createCommentPost",
      {
        postInfo: { userId, postId: post._id.toString(), content: commentValue }
      },
      () => {
        setCommentValue("");
      }
    );
    handleSendNotification(post.author.toString());
  };

  const handleReplyComment = (event) => {
    event.preventDefault();
    socket.emit(
      "replyCommentPost",
      {
        postInfo: {
          userId,
          postId: post._id.toString(),
          content: commentValue,
          commentId: comment._id.toString()
        }
      },
      () => {
        setCommentValue("");
      }
    );
    handleSendNotification(post.author.toString());
    handleSendNotification(comment.author.toString());
  };

  useEffect(() => {
    getUserById(userId);
  });

  return (
    <div>
      <div className="post-comment-form">
        <div className="avatar">
          <img
            src={
              currUser.avatars &&
              currUser.avatars.length > 0 &&
              currUser.avatars.find((avatar) => avatar.isActive)
                ? currUser.avatars.find((avatar) => avatar.isActive).url
                : logo
            }
            alt=""
          />
        </div>
        <form
          className="comment-input"
          onSubmit={
            comment ? handleReplyComment : handleCreateComment
          }
        >
          <input
            type="text"
            onChange={handleChangeInput}
            value={commentValue}
          />
          <button type="submit">Send</button>
        </form>
      </div>
    </div>
  );
};

export default CommentInput;
