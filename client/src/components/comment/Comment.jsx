import React, { useEffect, useState } from "react";
import "./comment.scss";
import logo from "../../assets/imgs/logo.png";
import { format } from 'timeago.js';

const Comment = ({ socket, index, handleReplyComment, commentId }) => {
  const [detailComment, setDetailComment] = useState({});
  const [author, setAuthor] = useState({});

  const getComment = (commentId) => {
    socket.emit("getCommentById", { commentId: commentId }, (response) => {
      setDetailComment(response);
      getUserById(response.author);
    });
  };

  const getUserById = (author) => {
    socket.emit("getUserById", { userId: author }, (response) => {
      setAuthor(response);
    });
  };

  useEffect(() => {
    getComment(commentId);
  }, []);

  return (
    <div className="comment">
      <div className="comment__author">
        <div className="comment__author__avatar">
          <div className="avatar">
            <img
              src={
                author.avatars &&
                author.avatars.length > 0 &&
                author.avatars.find((avatar) => avatar.isActive)
                  ? author.avatars.find((avatar) => avatar.isActive).url
                  : logo
              }
              alt=""
            />
          </div>
        </div>
        <div className="comment__box">
          <div className="comment__box__name">{author.fullName}</div>
          <div className="comment__content">
            {detailComment.content}
          </div>
        </div>
      </div>
      <div className="comment__actions">
        <div className="comment__action comment__react">Like</div>
        <div
          className="comment__action comment__reply"
          onClick={() => handleReplyComment(index)}
        >
          Reply
        </div>
        <div className="comment__action comment__created-date">{format(detailComment.createdAt)}</div>
      </div>
    </div>
  );
};

export default Comment;
