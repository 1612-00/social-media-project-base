import React, { useContext, useEffect } from "react";
import { io } from "socket.io-client";
import { LOCAL_STORAGE_ACCESS_TOKEN_NAME } from "../../constants/constant";
import { AuthContext } from "../../contexts/AuthContext";
import Post from "../post/Post";
import "./feed.scss";

const Feed = () => {
  const socket = io("http://localhost:8080", { transports: ["websocket"] });

  // Auth Context
  const {
    state: { user, listPost },
    getNotifications
  } = useContext(AuthContext);

  const joinLoginRoom = (userId) => {
    socket.emit("joinRoomLogin", { userId: userId }, (response) => {
      console.log(response);
    });
  };

  const socketAuthentication = (accessToken) => {
    socket.auth = { accessToken };
    socketReconnect();
  };

  useEffect(() => {
    socketAuthentication(localStorage[LOCAL_STORAGE_ACCESS_TOKEN_NAME]);
  }, []);

  useEffect(() => {
    if (user) joinLoginRoom(user._id.toString());
    socket.on("listNotifications", async () => {
      await getNotifications(user._id.toString());
    });
    socket.on("authentication", (data) => {
      if (data.success) {
        console.log(data.message);
      } else {
        alert(`Socket authentication failed: ${data.message}`);
      }
    });
  }, [user, socket]);

  const socketReconnect = () => {
    socket.disconnect();
    socket.connect();
  };

  return (
    <div className="feed">
      <div className="feed__content">
        {listPost.length > 0 &&
          listPost.map((post, index) => (
            <Post post={post} key={index} socket={socket} />
          ))}
      </div>
    </div>
  );
};

export default Feed;
