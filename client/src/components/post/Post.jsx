import React, { useContext, useEffect, useState } from "react";
import logo from "../../assets/imgs/logo.png";
import { AuthContext } from "../../contexts/AuthContext";
import Comment from "../comment/Comment";
import CommentInput from "../comment-input/CommentInput";
import "./post.scss";
import { format } from "timeago.js";
import { detailAuthorPost } from "../../controllers/PostController";

const REACT_HEART = "heart";
const REACT_LIKE = "like";

const Post = ({ post, socket }) => {
  // Auth Context
  const {
    state: { user },
    updateListPost,
  } = useContext(AuthContext);

  const [replyComment, setReplyComment] = useState({
    status: false,
    index: ""
  });
  const [countComment, setCountComment] = useState(0);
  const [reactPost, setReactPost] = useState(null);
  const [someoneTyping, setSomeoneTyping] = useState("");
  const [author, setAuthor] = useState({});

  const handleReplyComment = (index) => {
    setReplyComment({ status: true, index: index });
  };

  const handleCountComment = () => {
    let count = 0;
    post.comments.forEach((comment) => {
      count += 1;
      count += comment.commentReplies.length;
    });
    setCountComment(count);
  };

  const handleReactPost = (type) => {
    if (reactPost === type) setReactPost(null);
    else {
      setReactPost(type);
      handleSendNotification(post.author.toString());
    }
    emitReactPost(type);
  };

  const emitReactPost = (type) => {
    socket.emit(
      "reactPost",
      {
        reactInfo: {
          userId: user._id.toString(),
          postId: post._id.toString(),
          type: type === REACT_LIKE ? "like" : "heart"
        }
      },
      () => {}
    );
  };

  const joinPost = (postId) => {
    socket.emit("joinPost", { postId: postId }, (response) => {
      console.log(response);
    });
  };

  const checkReactedPost = (user) => {
    const reacted = post.reacts.find(
      (react) => react.user === user._id.toString()
    );
    if (reacted) {
      setReactPost(reacted.reactType);
    }
  };

  const onTyping = () => {
    socket.on("typing", ({ userTyping, isTyping }) => {
      if (isTyping && userTyping !== user._id.toString()) {
        setSomeoneTyping("Someone typing ...");
      } else {
        setSomeoneTyping("");
      }
    });
  };

  const handleSendNotification = (receiverId) => {
    socket.emit("sendNotification", { receiverId: receiverId }, (response) => {});
  };

  useEffect(() => {
    socket.on("newPost", (newPost) => {
      updateListPost(newPost);
    });
    handleCountComment();
    onTyping();
  }, [socket]);

  useEffect(() => {
    joinPost(post._id.toString());
    if (user) checkReactedPost(user);
    detailAuthorPost(post, setAuthor);
  }, []);

  return (
    <>
      {post._id && (
        <div className="post">
          <div className="post__top">
            <div className="post__top__author">
              <div className="avatar">
                {author && (
                  <img
                    src={
                      author.avatars &&
                      author.avatars.length > 0 &&
                      author.avatars.find((avatar) => avatar.isActive)
                        ? author.avatars.find((avatar) => avatar.isActive).url
                        : logo
                    }
                    alt=""
                  />
                )}
              </div>
              <div className="post__top__author__right">
                <div className="post__top__author__right__name">
                  {author && author.fullName}
                </div>
                <div className="post__top__author__right__info">
                  <div className="post__top__author__right__info__date-created">
                    {format(post.createdAt)}
                  </div>
                  <div className="post__top__author__right__info__audience">
                    <box-icon name="globe"></box-icon>
                  </div>
                </div>
              </div>
            </div>
            <div className="post__setting">
              <box-icon name="dots-horizontal-rounded"></box-icon>
            </div>
          </div>
          <div className="post__content">{post.content}</div>
          <div className="post__content__medias">
            <div className="post__content__media">
              {(post.images.length > 0 &&
                post.images.map((image, index) => (
                  <img src={image} alt="" key={index} />
                ))) || <img src={logo} alt="" />}
            </div>
          </div>

          <div className="post__react">
            <div className="post__react__left">
              <div className="post__react__left__icon">
                <div
                  className={reactPost === REACT_LIKE ? "like" : ""}
                  onClick={() => handleReactPost(REACT_LIKE)}
                >
                  <box-icon name="like"></box-icon>
                </div>
                <div
                  className={reactPost === REACT_HEART ? "heart" : ""}
                  onClick={() => handleReactPost(REACT_HEART)}
                >
                  <box-icon name="heart"></box-icon>
                </div>
              </div>
              <div className="post__react__left__number">
                {post.reacts.length}
              </div>
            </div>
            <div className="post__react__right">
              <div className="post__react__right__comments">
                {countComment} Comments
              </div>
              <div className="post__react__right__shares">
                {post.shares.length} Shares
              </div>
            </div>
          </div>
          <hr />
          <div className="typing">{someoneTyping}</div>
          <div className="post__comment">
            <CommentInput
              socket={socket}
              userId={user && user._id.toString()}
              post={post}
              handleSendNotification={handleSendNotification}
            />

            {post.comments &&
              post.comments.length > 0 &&
              post.comments.map((comment, index) => (
                <div className="post__comment__list" key={index}>
                  <div className="main-comment">
                    <Comment
                      socket={socket}
                      index={index}
                      handleReplyComment={handleReplyComment}
                      commentId={comment.mainComment}
                    />
                  </div>
                  {comment.commentReplies &&
                    comment.commentReplies.length > 0 &&
                    comment.commentReplies.map((commentReply, indexReply) => (
                      <div className="sub-comment" key={indexReply}>
                        <Comment
                          socket={socket}
                          index={index}
                          handleReplyComment={handleReplyComment}
                          commentId={commentReply}
                        />
                      </div>
                    ))}
                  {replyComment.status && replyComment.index === index && (
                    <CommentInput
                      socket={socket}
                      userId={user && user._id.toString()}
                      post={post}
                      comment={comment}
                      handleSendNotification={handleSendNotification}
                    />
                  )}
                </div>
              ))}
          </div>
        </div>
      )}
    </>
  );
};

export default Post;
