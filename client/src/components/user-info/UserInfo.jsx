import React, { useContext } from "react";
import "./userInfo.scss";
import { AuthContext } from "./../../contexts/AuthContext";
import logo from "../../assets/imgs/logo.png";

const UserInfo = ({ show }) => {
  // Auth Context
  const {
    state: { user },
    logout
  } = useContext(AuthContext);

  return (
    <>
      {user && (
        <div className={`box-user ${show === true ? "show" : ""}`}>
          <div className="box-user__top">
            <div className="box-user__top__img">
              <img
                src={
                  user &&
                  user.avatars &&
                  user.avatars.length > 0 &&
                  user.avatars.find((avatar) => avatar.isActive)
                    ? user.avatars.find((avatar) => avatar.isActive).url
                    : logo
                }
                alt=""
              />
            </div>
            <div className="box-user__top__info">
              <span className="box-user__top__info__name">{user.fullName}</span>
              <span className="box-user__top__info__email">{user.email}</span>
            </div>
          </div>
          <div className="box-user__logout">
            <button onClick={() => logout()}>Logout</button>
          </div>
        </div>
      )}
    </>
  );
};

export default UserInfo;
