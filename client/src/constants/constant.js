export const apiUrl = "http://localhost:8080/api/v1";

export const LOCAL_STORAGE_ACCESS_TOKEN_NAME = "access_token";
export const LOCAL_STORAGE_REFRESH_TOKEN_NAME = "refresh_token";

// Authorization action
export const SET_AUTH = "SET_AUTH";
export const SET_LIST_POST = "SET_LIST_POST";
export const UPDATE_LIST_POST = "UPDATE_LIST_POST";
export const SET_LIST_NOTIFICATION = "SET_LIST_NOTIFICATION";
export const SEEN_NOTIFICATION = "SEEN_NOTIFICATION";