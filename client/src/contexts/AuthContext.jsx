import { createContext, useReducer, useEffect } from "react";
import { AuthReducer } from "../reducers/AuthReducers";
import {
  SET_AUTH,
  apiUrl,
  LOCAL_STORAGE_ACCESS_TOKEN_NAME,
  LOCAL_STORAGE_REFRESH_TOKEN_NAME,
  SET_LIST_POST,
  UPDATE_LIST_POST,
  SET_LIST_NOTIFICATION,
  SEEN_NOTIFICATION
} from "../constants/constant";
import axios from "axios";
import setAuthToken from "../ultil/setAuthToken";
import { io } from "socket.io-client";

const initialState = {
  isAuthenticated: false,
  user: null,
  isLoading: false,
  listPost: [],
  listNotifications: []
};

export const AuthContext = createContext(initialState);

const AuthContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AuthReducer, initialState);

  // Authenticated User
  const loadUser = async () => {
    try {
      if (localStorage[LOCAL_STORAGE_ACCESS_TOKEN_NAME]) {
        setAuthToken(localStorage[LOCAL_STORAGE_ACCESS_TOKEN_NAME]);
        const res = await axios.post(`${apiUrl}/refresh-token`, {
          refreshToken: localStorage[LOCAL_STORAGE_REFRESH_TOKEN_NAME]
        });
        dispatch({
          type: SET_AUTH,
          payload: {
            isAuthenticated: true,
            user: res.data
          }
        });
        await getNotifications(res.data._id.toString());
      }
    } catch (error) {
      localStorage.removeItem(LOCAL_STORAGE_ACCESS_TOKEN_NAME);
      localStorage.removeItem(LOCAL_STORAGE_REFRESH_TOKEN_NAME);
      setAuthToken(null);
      dispatch({
        type: SET_AUTH,
        payload: {
          isAuthenticated: false,
          user: null
        }
      });
    }
  };

  useEffect(() => {
    loadUser();
    getPosts();
  }, []);

  // Login
  const loginUser = async (user) => {
    try {
      const res = await axios.post(`${apiUrl}/auth/login`, user);
      if (res.data.message) {
        dispatch({
          type: SET_AUTH,
          payload: {
            isAuthenticated: false,
            user: res.data.user
          }
        });
        return { success: false, message: res.data.message };
      }
      localStorage.setItem(
        LOCAL_STORAGE_ACCESS_TOKEN_NAME,
        res.data.accessToken
      );
      localStorage.setItem(
        LOCAL_STORAGE_REFRESH_TOKEN_NAME,
        res.data.refreshToken
      );
      dispatch({
        type: SET_AUTH,
        payload: {
          isAuthenticated: true,
          user: res.data.user
        }
      });
      loadUser();
      getPosts();
      return { success: true };
    } catch (error) {
      if (error.response.data) return error.response.data.message;
      return { success: false };
    }
  };

  const resendOtpCode = async (user) => {
    try {
      const res = await axios.post(`${apiUrl}/user/gen-otp`, { email: user.email, phoneNumber: user.phoneNumber });
      return { success: true, message: res.data.message };
    } catch (error) {
      if (error.response.data) return error.response.data.message;
      return { success: false };
    }
  }

  // Two Step Verify
  const twoStepVerify = async (user, otp) => {
    try {
      const res = await axios.post(`${apiUrl}/auth/verify-otp`, { email: user.email, otp });
      if(res.data) {
        localStorage.setItem(
          LOCAL_STORAGE_ACCESS_TOKEN_NAME,
          res.data.accessToken
        );
        localStorage.setItem(
          LOCAL_STORAGE_REFRESH_TOKEN_NAME,
          res.data.refreshToken
        );
        dispatch({
          type: SET_AUTH,
          payload: {
            isAuthenticated: true,
            user: res.data.user
          }
        });
        loadUser();
        return { success: true };
      }
    } catch (error) {
      if (error.response.data) return error.response.data.message;
      return { success: false };
    }
  } 

  // Register
  const registerUser = async (user) => {
    try {
      const res = await axios.post(`${apiUrl}/auth/register`, user);
      localStorage.setItem(
        LOCAL_STORAGE_ACCESS_TOKEN_NAME,
        res.data.accessToken
      );
      localStorage.setItem(
        LOCAL_STORAGE_REFRESH_TOKEN_NAME,
        res.data.refreshToken
      );
      dispatch({
        type: SET_AUTH,
        payload: {
          isAuthenticated: true,
          user: res.data.user
        }
      });
      loadUser();
      return { success: true };
    } catch (error) {
      if (error.response.data) return error.response.data;
      return { success: false };
    }
  };

  // Logout
  const logout = async () => {
    localStorage.removeItem(LOCAL_STORAGE_ACCESS_TOKEN_NAME);
    localStorage.removeItem(LOCAL_STORAGE_REFRESH_TOKEN_NAME);
    dispatch({
      type: SET_AUTH,
      payload: {
        isAuthenticated: false,
        user: null
      }
    });
  };

  // Reset password
  const forgotPassword = async (email) => {
    try {
      await axios.post(`${apiUrl}/auth/forgot-password`, { email });
      return { success: true, message: "Please checked your email" };
    } catch (error) {
      if (error.response.data) return error.response.data;
    }
  };

  // Get list post
  const getPosts = async () => {
    try {
      const res = await axios.get(`${apiUrl}/post/timeline/paginate`);
      dispatch({
        type: SET_LIST_POST,
        payload: {
          listPost: res.data.listPost
        }
      });
    } catch (error) {
      if (error.response.data) return error.response.data.message;
    }
  };

  // Update list post
  const updateListPost = (post) => {
    dispatch({
      type: UPDATE_LIST_POST,
      payload: {
        postUpdated: post
      }
    });
  };

  // Get list notifications
  const getNotifications = async (userId) => {
    try {
      const res = await axios.get(
        `${apiUrl}/notification/owner-by-id/${userId}?page=1&limit=100`
      );
      dispatch({
        type: SET_LIST_NOTIFICATION,
        payload: {
          listNotifications: res.data
        }
      });
    } catch (error) {
      if (error.response.data) return error.response.data.message;
    }
  };

  // Seen notification
  const seenNotification = async (notificationId) => {
    try {
      await axios.patch(`${apiUrl}/notification/${notificationId}`);
      let notificationUpdate;
      state.listNotifications.forEach((notification) => {
        if (notification._id.toString() === notificationId) {
          notificationUpdate = notification;
        }
      });
      notificationUpdate.seen = true;
      dispatch({
        type: SEEN_NOTIFICATION,
        payload: {
          notificationUpdate: notificationUpdate
        }
      });
    } catch (error) {
      if (error.response.data) return error.response.data.message;
    }
  };

  const AuthContextData = {
    state,
    loadUser,
    loginUser,
    twoStepVerify,
    registerUser,
    logout,
    getPosts,
    forgotPassword,
    updateListPost,
    getNotifications,
    seenNotification,
    resendOtpCode,
  };

  return (
    <AuthContext.Provider value={AuthContextData}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContextProvider;
