import axios from "axios";
import { apiUrl } from "../constants/constant";

export const detailAuthorPost = async (post, setAuthor) => {
  try {
    const userDetail = await axios.get(`${apiUrl}/user/info/${post.author.toString()}`);
    setAuthor(userDetail.data);
  } catch (error) {
    return error;
  }
}