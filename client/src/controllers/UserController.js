import axios from "axios";
import { apiUrl } from "../constants/constant";

export const detailUserById = async (userId, setUser) => {
  try {
    const userDetail = await axios.get(`${apiUrl}/user/info/${userId}`)
    setUser(userDetail.data);
  } catch (error) {
    return error;
  }   
}