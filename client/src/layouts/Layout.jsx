import React, { useContext } from "react";
import "./layout.scss";
import { Navigate, Outlet } from "react-router-dom";
import { AuthContext } from "./../contexts/AuthContext";
import { Box, CircularProgress } from "@mui/material";
import TopNav from "../components/topnav/TopNav";

const Layout = () => {
  // Auth Context
  const {
    state: { isLoading, isAuthenticated }
  } = useContext(AuthContext);

  if (isLoading) {
    return (
      <Box className="spinner">
        <CircularProgress />
      </Box>
    );
  } else if (!isAuthenticated) {
    return <Navigate to="/auth/login" />;
  } else
    return (
      <>
        <TopNav />
        <div className="main">
          <div className="overlay"></div>
          <div className="main__content">
            <Outlet />
          </div>
        </div>
      </>
    );
};

export default Layout;
