import React, { useContext, useState } from "react";
import "./login.scss";
import { Navigate, Link } from "react-router-dom";
import { AuthContext } from "../../contexts/AuthContext";
import {
  Box,
  CircularProgress,
  InputLabel,
  OutlinedInput,
  FormControl,
  Button
} from "@mui/material";

const TwoStepVerification = () => {
  // Otp state
  const [otp, setOtp] = useState("");

  // Handle show password
  const handleChangeOtp = (event) => {
    setOtp(event.target.value);
  };

  // Auth Context
  const {
    state: { user, isLoading, isAuthenticated },
    twoStepVerify,
    resendOtpCode
  } = useContext(AuthContext);

  const handleSubmitForm = async (event) => {
    event.preventDefault();

    try {
      const resData = await twoStepVerify(user, otp);
      if (!resData.success) {
        alert(resData.message ? resData.message : "Login failed");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleResendCode = async () => {
    const resData = await resendOtpCode(user);
    if (resData.success) {
      alert(resData.message ? resData.message : "Resend code failed");
    } else {
      alert("Resend code failed");
    }
  }

  if (isLoading) {
    return (
      <Box className="spinner">
        <CircularProgress />
      </Box>
    );
  } else if (isAuthenticated) {
    return <Navigate to="/" />;
  } else if (!isAuthenticated && !user) {
    return <Navigate to="/auth/login" />;
  } else
    return (
      <div className="auth">
        <div className="auth__wrapper">
          <div className="auth__content">
            <div className="auth__content__top">
              <div className="auth__content__top__title">
                Two Step Verification
              </div>
              <div className="auth__content__top__sub">
                Enter 6 characters sent to your phone
              </div>
            </div>
            <form onSubmit={handleSubmitForm} className="auth__content__form">
              <FormControl fullWidth sx={{ m: 1 }}>
                <InputLabel htmlFor="email">Otp</InputLabel>
                <OutlinedInput
                  id="otp"
                  type="otp"
                  label="Otp"
                  length="6"
                  placeholder="Your otp here..."
                  value={otp}
                  onChange={handleChangeOtp}
                />
              </FormControl>
              <Button variant="contained" size="large" type="submit">
                Verify
              </Button>
            </form>
            <div className="auth__content__bottom">
              <div className="auth__content__bottom__register">
                <div className="login__content__bottom__register__text resend" onClick={handleResendCode}>
                  Resend Code?
                </div>
              </div>
            </div>
            <div className="auth__content__bottom">
              <div className="auth__content__bottom__register">
                <div className="login__content__bottom__register__text">
                  Don't have an account?
                </div>
                <Link
                  to="/auth/register"
                  className="auth__content__bottom__register__link"
                >
                  Create free account
                </Link>
              </div>
            </div>
          </div>
          <div className="auth__background-img"></div>
        </div>
      </div>
    );
};

export default TwoStepVerification;
