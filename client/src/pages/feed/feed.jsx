// import "./App.css";
// import "boxicons";
// import { io } from "socket.io-client";
// import { useEffect, useState } from "react";
// import logo from "./assets/images/logo.png";
// import Comment from "./components/comment/Comment";
// import CommentInput from "./components/comment-input/CommentInput";

// function App() {
//   const socket = io("http://localhost:8080", { transports: ["websocket"] });
//   const [post, setPost] = useState({});
//   const [author, setAuthor] = useState({});
//   const [userId, setUserId] = useState("");
//   const [postId, setPostId] = useState("");

//   const [replyComment, setReplyComment] = useState({
//     status: false,
//     index: ""
//   });

//   const handleChangeUserId = (event) => {
//     setUserId(event.target.value);
//   };

//   const handleChangePostId = (event) => {
//     setPostId(event.target.value);
//   };

//   const handleReplyComment = (index) => {
//     setReplyComment({ status: true, index: index });
//   };

//   const getUserById = (author) => {
//     socket.emit("getUserById", { userId: author }, (response) => {
//       setAuthor(response);
//     });
//   };
//   const joinPost = (postId) => {
//     socket.emit("joinPost", { postId: postId }, (response) => {
//       console.log(response);
//     });
//   };
//   const getPost = () => {
//     socket.emit(
//       "getPost",
//       { postInfo: { userId: userId, postId: postId } },
//       (response) => {
//         setPost(response);
//         getUserById(response.author);
//         joinPost(postId);
//       }
//     );
//   };

//   const handleGetPost = (event) => {
//     event.preventDefault();
//     getPost();
//   };

//   useEffect(() => {
//     socket.on("newPost", (newPost) => {
//       setPost(newPost);
//     });
//   }, [socket]);

//   return (
//     <div className="App">
//       <div className="main-text">comment post realtime</div>

//       <form className="post-inputs" onSubmit={handleGetPost}>
//         <div className="post-input">
//           <label>UserId:</label>
//           <input type="text" onChange={handleChangeUserId} value={userId} />
//         </div>
//         <div className="post-input">
//           <label>PostId:</label>
//           <input type="text" onChange={handleChangePostId} value={postId} />
//         </div>
//         <button type="submit">Get Post</button>
//       </form>

//       <hr />

//       {post._id && (
//         <div className="post">
//           <div className="post-top-content">
//             <div className="post-author">
//               <div className="avatar">
//                 <img
//                   src={
//                     (author.avatars &&
//                       author.avatars.length > 0 &&
//                       author.avatars.find((avatar) => avatar.isActive).url) ||
//                     logo
//                   }
//                   alt=""
//                 />
//               </div>
//               <div className="post-author-right">
//                 <div className="post-author-name">{author.fullName}</div>
//                 <div className="post-info">
//                   <div className="post-date-created">26m</div>
//                   <div className="post-audience">
//                     <box-icon name="globe"></box-icon>
//                   </div>
//                 </div>
//               </div>
//             </div>
//             <div className="post-setting">
//               <box-icon name="dots-horizontal-rounded"></box-icon>
//             </div>
//           </div>
//           <div className="post-content">{post.content}</div>
//           <div className="post-medias">
//             <div className="post-media">
//               {(post.images.length > 0 &&
//                 post.images.map((image, index) => (
//                   <img src={image} alt="" key={index} />
//                 ))) || <img src={logo} alt="" />}
//             </div>
//           </div>

//           <div className="post-react">
//             <div className="post-react-left">
//               <div className="post-react-left-icon">
//                 <box-icon name="like"></box-icon>
//                 <box-icon name="heart" className="heart"></box-icon>
//               </div>
//               <div className="post-react-left-number">16</div>
//             </div>
//             <div className="post-react-right">
//               <div className="post-react-right-comments">10 Comments</div>
//               <div className="post-react-right-shares">10 Shares</div>
//             </div>
//           </div>
//           <hr />

//           <div className="post-comment">
//             <CommentInput
//               socket={socket}
//               userId={userId}
//               postId={postId}
//               getPost={getPost}
//             />

//             {post.comments &&
//               post.comments.length > 0 &&
//               post.comments.map((comment, index) => (
//                 <div className="post-comment-list" key={index}>
//                   <div className="main-comment">
//                     <Comment
//                       socket={socket}
//                       index={index}
//                       handleReplyComment={handleReplyComment}
//                       commentId={comment.mainComment}
//                     />
//                   </div>
//                   {comment.commentReplies &&
//                     comment.commentReplies.length > 0 &&
//                     comment.commentReplies.map((commentReply, indexReply) => (
//                       <div className="sub-comment" key={indexReply}>
//                         <Comment
//                           socket={socket}
//                           index={index}
//                           handleReplyComment={handleReplyComment}
//                           commentId={commentReply}
//                         />
//                       </div>
//                     ))}
//                   {replyComment.status && replyComment.index === index && (
//                     <CommentInput
//                       socket={socket}
//                       userId={userId}
//                       postId={postId}
//                       getPost={getPost}
//                       commentId={comment._id}
//                     />
//                   )}
//                 </div>
//               ))}
//           </div>
//         </div>
//       )}
//     </div>
//   );
// }

// export default App;
