import {
  SEEN_NOTIFICATION,
  SET_AUTH,
  SET_LIST_NOTIFICATION,
  SET_LIST_POST,
  UPDATE_LIST_POST
} from "../constants/constant";

export const AuthReducer = (state, action) => {
  const {
    type,
    payload: {
      isAuthenticated,
      user,
      listPost,
      postUpdated,
      listNotifications,
      notificationUpdate
    }
  } = action;

  switch (type) {
    case SET_AUTH:
      return {
        ...state,
        authLoading: false,
        isAuthenticated,
        user
      };

    case SET_LIST_POST:
      return {
        ...state,
        listPost: listPost
      };

    case UPDATE_LIST_POST:
      return {
        ...state,
        listPost: state.listPost.map((post) =>
          post._id.toString() === postUpdated._id.toString()
            ? postUpdated
            : post
        )
      };

    case SEEN_NOTIFICATION:
      const newListNoti = state.listNotifications.map((notification) =>
        notification._id.toString() === notificationUpdate._id
          ? notificationUpdate
          : notification
      );
      return {
        ...state,
        listNotifications: newListNoti
      };

    case SET_LIST_NOTIFICATION:
      return {
        ...state,
        listNotifications: listNotifications
      };

    default:
      return state;
  }
};
