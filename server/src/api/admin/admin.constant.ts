import { swaggerSchemaExample } from '../../share/utils/swagger_schema';

export const SWAGGER_RESPONSE = {
  HEALTH_CHECK: swaggerSchemaExample(
    {
      message: 'OK',
      statusCode: 200,
    },
    'API for health check',
  ),

  BAD_REQUEST_EXCEPTION: swaggerSchemaExample(
    {
      message: 'Bad Request',
      statusCode: 400,
    },
    'Bad request exception',
  ),
  UNAUTHORIZED_EXCEPTION: swaggerSchemaExample(
    {
      message: 'Unauthorized',
      statusCode: 401,
    },
    'Unauthorized exception, you need to login again',
  ),

  FORBIDDEN_EXCEPTION: swaggerSchemaExample(
    {
      message: 'You do not have permission to access resource!',
      statusCode: 403,
    },
    'You do not have permission to access resource!',
  ),
  NOT_FOUND_EXCEPTION: swaggerSchemaExample(
    {
      message: 'Not found exception',
      statusCode: 404,
    },
    'Not found exception',
  ),
  INTERNAL_SERVER_EXCEPTION: swaggerSchemaExample(
    {
      message: 'Internal server error',
      statusCode: 500,
    },
    'Internal server error',
  ),
};
