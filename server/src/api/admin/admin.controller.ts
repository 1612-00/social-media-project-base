import { Body, Controller, Delete, Param, Post, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { LockAccountService } from '../../config/job-queue/lock-acc.service';
import { JwtAuthGuard } from '../../share/auth/guard/jwt.guard';
import { UserRolesGuard } from '../../share/auth/guard/user-roles.guard';
import { GetUser } from '../../share/decorators/get-user.decorator';
import { Roles } from '../../share/decorators/user-roles.decorator';
import { UserRole } from '../user/role/role.enum';
import { SWAGGER_RESPONSE } from './admin.constant';
import { AdminService } from './admin.service';
import { LockUserDto } from './dto/lock-user.dto';

@ApiBearerAuth()
@UseGuards(JwtAuthGuard, UserRolesGuard)
@Roles(UserRole.Admin)
@ApiTags('Admin')
@ApiUnauthorizedResponse(SWAGGER_RESPONSE.UNAUTHORIZED_EXCEPTION)
@ApiForbiddenResponse(SWAGGER_RESPONSE.FORBIDDEN_EXCEPTION)
@ApiNotFoundResponse(SWAGGER_RESPONSE.NOT_FOUND_EXCEPTION)
@Controller('admin')
export class AdminController {
  constructor(private readonly adminService: AdminService, private readonly LockAccountService: LockAccountService) {}

  // //#region Admin
  // @Delete('delete-user/:id')
  // @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  // deleteUser(@GetUser('id') myId: string, @Param('id') userId: string) {
  //   return this.adminService.deleteUser(myId, userId);
  // }

  @Post('lock-account')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  // @ApiConsumes('application/x-www-form-urlencoded')
  lockUserAccount(@Body() lockUserDto: LockUserDto) {
    this.LockAccountService.addLockAccJob(lockUserDto);
    return this.adminService.lockUserAccount(lockUserDto);
  }
  //#endregion Admin
}
