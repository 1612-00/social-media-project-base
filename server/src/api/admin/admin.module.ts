import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JobQueueModule } from 'src/config/job-queue/job-queue.module';
import { User, UserSchema } from 'src/schemas/UserSchema';
import { PostModule } from '../post/post.module';
import { UserModule } from '../user/user.module';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';

@Module({
  imports: [
    UserModule,
    PostModule,
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    JobQueueModule,
  ],
  controllers: [AdminController],
  providers: [AdminService],
  exports: [AdminService],
})
export class AdminModule {}
