import { BadRequestException, ForbiddenException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { PostService } from '../post/post.service';
import { UserRole } from '../user/role/role.enum';
import { UserRepository } from '../user/user.repository';
import { AdminService } from './admin.service';
import { LockUserDto } from './dto/lock-user.dto';

describe('AdminService', () => {
  let service: AdminService;

  const mockUserRepository = {
    getUserById: jest.fn((id) => ({ _id: id })),
    findOneByIdAndUpdate: jest.fn((id, data) => ({ _id: id, ...data })),
    findOneByIdAndDelete: jest.fn((id) => ({ _id: id })),
  };

  const mockPostService = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AdminService,
        {
          provide: UserRepository,
          useValue: mockUserRepository,
        },
        {
          provide: PostService,
          useValue: mockPostService,
        },
      ],
    }).compile();

    service = module.get<AdminService>(AdminService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  // describe('delete user by id', () => {
  //   const userId = '62f20fc3cceaf132e5f17a6e';
  //   const myId = '62f1dc7e072d445d7cdc6b6d';

  //   it('should deleted no exception', async () => {
  //     mockUserRepository.getUserById.mockImplementation((userId) => ({
  //       _id: userId,
  //       role: UserRole.User,
  //     }));
  //     const userDeleted = await service.deleteUser(myId, userId);
  //     expect(userDeleted).toEqual({ _id: userId });
  //   });

  //   it('should throw exception when userId equals myId', async () => {
  //     expect(service.deleteUser(myId, myId)).rejects.toThrowError(BadRequestException);
  //   });

  //   it('should throw exception for a bad userId', async () => {
  //     mockUserRepository.getUserById.mockImplementation((userId) => null);
  //     expect(service.deleteUser(myId, userId)).rejects.toThrowError(NotFoundException);
  //   });

  //   it('should throw exception when user was deleted has admin role', async () => {
  //     mockUserRepository.getUserById.mockImplementation((userId) => ({
  //       _id: userId,
  //       role: UserRole.Admin,
  //     }));
  //     expect(service.deleteUser(myId, userId)).rejects.toThrowError(ForbiddenException);
  //   });
  // });

  describe('lock user account', () => {
    const dto: LockUserDto = { userId: '62f1dc7e072d445d7cdc6b6d', time: 3, reason: 'Spam comment' };

    it('should lock user account no exception', async () => {
      const userUpdated = {
        _id: dto.userId,
        isLocked: {
          status: true,
          time: dto.time,
          reason: dto.reason,
        },
      };
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, role: UserRole.User, isLocked: null }));
      expect(await service.lockUserAccount(dto)).toEqual(userUpdated);
    });

    it('should throw error if user not found', async () => {
      mockUserRepository.getUserById.mockImplementation(() => null);
      expect(service.lockUserAccount(dto)).rejects.toBeInstanceOf(NotFoundException);
    });

    it('should throw error if this user has admin role', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, role: UserRole.Admin }));
      expect(service.lockUserAccount(dto)).rejects.toBeInstanceOf(ForbiddenException);
    });
  });
});
