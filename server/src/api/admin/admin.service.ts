import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { ERROR } from '../..//share/common';
import { UserRole } from '../user/role/role.enum';
import { InfoLocked, User, UserDocument } from '../../schemas/UserSchema';
import { UserRepository } from '../user/user.repository';
import { LockUserDto } from './dto/lock-user.dto';
import mongoose, { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { PostService } from '../post/post.service';

@Injectable()
export class AdminService {
  constructor(
    private readonly userRepository: UserRepository, // private readonly postService: PostService, // @InjectModel(User.name) private userModel: Model<UserDocument>,
  ) {}

  // async deleteUser(myId: string, userId: string) {
  //   // const db = await mongoose.createConnection(process.env.MONGODB_URL).asPromise();
  //   // let session = null;
  //   // return this.userModel
  //   //   .createCollection()
  //   //   .then(() => db.startSession())
  //   //   .then(async (_session) => {
  //   //     session = _session;
  //   //     session.startTransaction();
  //   //     if (userId.includes(myId)) throw new BadRequestException(ERROR.DONT_INTERACTIVE_MYSELF.MESSAGE);
  //   //     const userFound = await this.userRepository.getUserById(userId);
  //   //     if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);
  //   //     if (userFound.role.includes(UserRole.Admin)) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);
  //   //     return this.userRepository.findOneByIdAndDelete(userId, session);
  //   //   })
  //   //   .then((session) => {
  //   //     return this.postService.deleteAllPostOwnerById(userId, session);
  //   //   })
  //   //   .then(() => session.abortTransaction())
  //   //   .then(() => session.endSession());
  //   try {
  //     const session = await this.userModel.db.startSession();
  //     session.startTransaction();
  //     try {
  //       if (userId.includes(myId)) throw new BadRequestException(ERROR.DONT_INTERACTIVE_MYSELF.MESSAGE);
  //       const userFound = await this.userRepository.getUserById(userId);
  //       if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);
  //       if (userFound.role.includes(UserRole.Admin)) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);
  //       const userDeleted = await this.userRepository.findOneByIdAndDelete(userId);
  //       await this.postService.deleteAllPostOwnerById(userId);
  //       await session.commitTransaction();
  //       return userDeleted;
  //     } catch (error) {
  //       await session.abortTransaction();
  //       console.log(error);
  //     } finally {
  //       session.endSession();
  //     }
  //   } catch (error) {
  //     return error;
  //   }
  // }

  async lockUserAccount(lockUserDto: LockUserDto) {
    const { userId, time, reason } = lockUserDto;

    const user = await this.userRepository.detailById(userId);
    if (!user) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);
    if (user.role === UserRole.Admin) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);
    const infoLocked: InfoLocked = {
      status: true,
      time,
      reason,
    };

    const userUpdated = await this.userRepository.findOneByIdAndUpdate(userId, {
      isLocked: infoLocked,
    });

    return { status: 200, lockedState: userUpdated.isLocked };
  }
}
