import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class LockUserDto {
  @ApiProperty({ description: 'userId' })
  @IsString()
  @IsNotEmpty()
  userId: string;

  @ApiProperty({ description: 'time - second' })
  @IsNumber()
  time: number;

  @ApiProperty({ description: 'reason' })
  @IsString()
  @IsNotEmpty()
  reason: string;
}
