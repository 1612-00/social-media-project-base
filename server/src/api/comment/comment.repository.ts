import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Comment, CommentDocument } from '../../schemas/CommentSchema';
import { CreateCommentDto } from './dto';

export class CommentRepository {
  constructor(@InjectModel(Comment.name) private commentModel: Model<CommentDocument>) {}

  create(createCommentDto: CreateCommentDto) {
    try {
      return this.commentModel.create(createCommentDto);
    } catch (error) {
      throw new Error('Comment created failed');
    }
  }

  save(param) {
    return new this.commentModel(param).save();
  }

  findDistinct(condition: object, field: string) {
    return this.commentModel.find(condition).distinct(field);
  }

  findManyByCondition(condition: object) {
    return this.commentModel.find(condition).exec();
  }

  findById(id: string) {
    return this.commentModel.findById(id).exec();
  }

  findOneByCondition(condition: object) {
    return this.commentModel.findOne(condition).exec();
  }

  findOneByIdAndUpdate(id: string, dataUpdate: object) {
    return this.commentModel.findByIdAndUpdate(id, { $set: dataUpdate }, { new: true }).exec();
  }

  findOneByConditionAndUpdate(condition: object, dataUpdate: object) {
    return this.commentModel.findOneAndUpdate(condition, { $set: dataUpdate }, { new: true }).exec();
  }

  findOneByIdAndDelete(id: string) {
    return this.commentModel.findByIdAndDelete(id).exec();
  }
}
