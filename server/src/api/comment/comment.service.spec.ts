import { Test, TestingModule } from '@nestjs/testing';
import { CommentRepository } from './comment.repository';
import { CommentService } from './comment.service';

describe('CommentService', () => {
  let service: CommentService;
  const userId = '62f0d61aa5e7f04e7d5f99dc';
  const postId = '62f75a0adc91b4b2bf5ccb0c';
  const newComment = {
    author: userId,
    post: postId,
    content: expect.any(String),
  };

  const mockCommentRepository = {
    create: jest.fn().mockImplementation((newComment) => Promise.resolve({ ...newComment })),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CommentService,
        {
          provide: CommentRepository,
          useValue: mockCommentRepository,
        },
      ],
    }).compile();

    service = module.get<CommentService>(CommentService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return new user when create user', async () => {
    expect(await service.createComment(userId, postId, newComment)).toEqual({
      ...newComment,
    });
  });
});
