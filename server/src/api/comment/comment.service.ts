import { Injectable } from '@nestjs/common';
import { CommentRepository } from './comment.repository';
import { CreateCommentDto } from './dto';

@Injectable()
export class CommentService {
  constructor(private readonly commentRepository: CommentRepository) {}

  async createComment(userId: string, postId: string, createCommentDto: CreateCommentDto) {
    try {
      const newComment = {
        author: userId,
        post: postId,
        content: createCommentDto.content,
      };
      return await this.commentRepository.create(newComment);
    } catch (error) {
      throw error;
    }
  }

  async getCommentById(commentId: string) {
    try {
      return this.commentRepository.findById(commentId);
    } catch (error) {
      throw error;
    }
  }
}
