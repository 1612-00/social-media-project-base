import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString, MaxLength } from 'class-validator';
import { AppObject } from '../../../share/common';

export class CreateGroupDto {
  @ApiProperty({ description: 'name' })
  @IsString()
  @MaxLength(100)
  name: string;

  @ApiProperty({ description: 'type' })
  @IsString()
  @IsEnum(AppObject.GROUP_PRIVACY)
  type?: string = AppObject.GROUP_PRIVACY.PUBLIC;
}
