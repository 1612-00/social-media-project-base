import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty } from 'class-validator';
import { AppObject } from 'src/share/common';

export class UpdateMemberRole {
  @ApiProperty({ description: 'role' })
  @IsNotEmpty()
  @IsEnum(AppObject.GROUP_ROLE)
  role: string;
}
