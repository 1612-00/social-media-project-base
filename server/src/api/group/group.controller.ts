import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../../share/auth/guard/jwt.guard';
import { GetUser } from '../../share/decorators/get-user.decorator';
import { CreatePostDto } from '../post/dto';
import { CreateGroupDto } from './dto';
import { UpdateMemberRole } from './dto/update-member-role.dto';
import { SWAGGER_RESPONSE } from './group.constant';
import { GroupService } from './group.service';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiUnauthorizedResponse(SWAGGER_RESPONSE.UNAUTHORIZED_EXCEPTION)
@ApiForbiddenResponse(SWAGGER_RESPONSE.FORBIDDEN_EXCEPTION)
@ApiNotFoundResponse(SWAGGER_RESPONSE.NOT_FOUND_EXCEPTION)
@ApiTags('Group')
@Controller('group')
export class GroupController {
  constructor(private readonly groupService: GroupService) {}

  @Post()
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  @ApiConsumes('application/x-www-form-urlencoded')
  createGroup(@GetUser('id') userId: string, @Body() createGroupDto: CreateGroupDto) {
    return this.groupService.createGroup(userId, createGroupDto);
  }

  @Post('request-join/:groupId')
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  sendRequestJoinGroup(@GetUser('id') userId: string, @Param('groupId') groupId: string) {
    return this.groupService.sendRequestJoinGroup(userId, groupId);
  }

  @Get('request-join/:groupId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  getAllRequestJoinGroup(@GetUser('id') userId: string, @Param('groupId') groupId: string) {
    return this.groupService.getAllRequestJoinGroup(userId, groupId);
  }

  @Patch('accept-request/:groupId/:reqId')
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  acceptRequestJoinGroup(
    @GetUser('id') userId: string,
    @Param('groupId') groupId: string,
    @Param('reqId') reqId: string,
  ) {
    return this.groupService.acceptRequestJoinGroup(userId, groupId, reqId);
  }

  @Patch('reject-request/:groupId/:reqId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  rejectRequestJoinGroup(
    @GetUser('id') userId: string,
    @Param('groupId') groupId: string,
    @Param('reqId') reqId: string,
  ) {
    return this.groupService.rejectRequestJoinGroup(userId, groupId, reqId);
  }

  @Patch('change-member-role/:groupId/:memberId')
  @ApiCreatedResponse(SWAGGER_RESPONSE.UPDATE_RESPONSE)
  @ApiConsumes('application/x-www-form-urlencoded')
  changeMemberRole(
    @GetUser('id') userId: string,
    @Param('groupId') groupId: string,
    @Param('memberId') memberId: string,
    @Body() updateMemberRole: UpdateMemberRole,
  ) {
    return this.groupService.changeMemberRole(userId, groupId, memberId, updateMemberRole);
  }

  @Post('create-post/:groupId')
  @UseInterceptors(FileFieldsInterceptor([{ name: 'images' }, { name: 'videos' }]))
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  @ApiBody({
    required: true,
    schema: {
      type: 'object',
      properties: {
        audience: {
          type: 'string',
        },
        content: {
          type: 'string',
        },
        images: {
          type: 'array',
          items: {
            type: 'string',
            format: 'binary',
          },
        },
        videos: {
          type: 'array',
          items: {
            type: 'string',
            format: 'binary',
          },
        },
      },
    },
  })
  createPostInGroup(
    @GetUser('id') userId: string,
    @Param('groupId') groupId: string,
    @Body() createPostDto: CreatePostDto,
    @UploadedFiles() files?: { images: Express.Multer.File[]; videos: Express.Multer.File[] },
  ) {
    return this.groupService.createPostInGroup(userId, groupId, createPostDto, files);
  }

  @Get('release-posts/:groupId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  getReleasePostsPagination(
    @GetUser('id') userId: string,
    @Param('groupId') groupId: string,
    @Query('page') page: string,
    @Query('limit') limit: string,
  ) {
    return this.groupService.getReleasePostsPagination(userId, groupId, parseInt(page) || 1, parseInt(limit) || 5);
  }

  @Get('waiting-posts/:groupId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  getWaitingPostsPagination(
    @GetUser('id') userId: string,
    @Param('groupId') groupId: string,
    @Query('page') page: string,
    @Query('limit') limit: string,
  ) {
    return this.groupService.getWaitingPostsPagination(userId, groupId, parseInt(page) || 1, parseInt(limit) || 5);
  }

  @Put('accept-post/:groupId/:postId')
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  acceptPost(@GetUser('id') userId: string, @Param('groupId') groupId: string, @Param('postId') postId: string) {
    return this.groupService.acceptPost(userId, groupId, postId);
  }

  @Put('reject-post/:groupId/:postId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  rejectPost(@GetUser('id') userId: string, @Param('groupId') groupId: string, @Param('postId') postId: string) {
    return this.groupService.rejectPost(userId, groupId, postId);
  }

  @Delete('delete-post/:groupId/:postId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  deleteReleasePost(@GetUser('id') userId: string, @Param('groupId') groupId: string, @Param('postId') postId: string) {
    return this.groupService.deleteReleasePost(userId, groupId, postId);
  }

  @Delete('delete-member/:groupId/:memberId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  deleteMember(@GetUser('id') userId: string, @Param('groupId') groupId: string, @Param('memberId') memberId: string) {
    return this.groupService.deleteMember(userId, groupId, memberId);
  }

  @Patch('block-user/:groupId/:memberId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  blockUser(@GetUser('id') userId: string, @Param('groupId') groupId: string, @Param('memberId') memberId: string) {
    return this.groupService.blockUser(userId, groupId, memberId);
  }

  @Get('notifications/all/:groupId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  getGroupNotificationsPaginate(
    @GetUser('id') userId: string,
    @Param('groupId') groupId: string,
    @Query('page') page: string,
    @Query('limit') limit: string,
  ) {
    return this.groupService.getOwnNotificationsPaginate(userId, groupId, +page, +limit);
  }
}
