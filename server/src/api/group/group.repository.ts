import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Group, GroupDocument } from '../../schemas/GroupSchema';
import { CreateGroupDto } from './dto';

export class GroupRepository {
  constructor(@InjectModel(Group.name) private groupModel: Model<GroupDocument>) {}

  create(createGroupDto: CreateGroupDto) {
    try {
      return this.groupModel.create(createGroupDto);
    } catch (error) {
      throw new Error('Group created failed');
    }
  }

  save(param) {
    return new this.groupModel(param).save();
  }

  findByConditionDistinct(condition: object) {
    return this.groupModel.find(condition).distinct('_id').exec();
  }

  findManyByCondition(condition: object) {
    return this.groupModel.find(condition).exec();
  }

  findById(id: string) {
    return this.groupModel.findById(id).exec();
  }

  findByIdPopulate(id: string) {
    return this.groupModel.findById(id).populate('comments').exec();
  }

  findOneByCondition(condition: object) {
    return this.groupModel.findOne(condition).exec();
  }

  findOneByIdAndUpdate(id: string, dataUpdate: object) {
    return this.groupModel.findByIdAndUpdate(id, { $set: dataUpdate }, { new: true }).exec();
  }

  findOneByConditionAndUpdate(condition: object, dataUpdate: object) {
    return this.groupModel.findOneAndUpdate(condition, { $set: dataUpdate }, { new: true }).exec();
  }

  findOneByIdAndDelete(id: string) {
    return this.groupModel.findByIdAndDelete(id).exec();
  }
}
