import { BadRequestException, ForbiddenException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppObject } from '../../share/common';
import { NotificationService } from '../notification/notification.service';
import { PostRepository } from '../post/post.repository';
import { PostService } from '../post/post.service';
import { UserRepository } from '../user/user.repository';
import { CreateGroupDto, UpdateMemberRole } from './dto';
import { GroupRepository } from './group.repository';
import { GroupService } from './group.service';

describe('GroupService', () => {
  let service: GroupService;

  const userId = 'ab237728788vf2327833823';
  const groupId = 'ju899283828dhi892wgf232f';

  const mockGroupRepository = {
    create: jest.fn().mockImplementation((newGroup) => ({ ...newGroup, _id: expect.any(String) })),
    findById: jest.fn().mockImplementation((id) => ({ _id: id })),
    findOneByIdAndUpdate: jest.fn().mockImplementation((id, data) => ({ _id: id, ...data })),
    save: jest.fn().mockImplementation((data) => ({ ...data })),
  };

  const mockUserRepository = {
    getUserById: jest.fn().mockImplementation((id) => ({ _id: id })),
    findOneByIdAndUpdate: jest.fn().mockImplementation((id, data) => ({ _id: id, ...data })),
  };

  const mockPostService = {};
  const mockPostRepository = {};
  const mockNotificationService = {
    create: jest.fn(() => true),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GroupService,
        {
          provide: GroupRepository,
          useValue: mockGroupRepository,
        },
        {
          provide: UserRepository,
          useValue: mockUserRepository,
        },
        {
          provide: PostService,
          useValue: mockPostService,
        },
        {
          provide: PostRepository,
          useValue: mockPostRepository,
        },
        {
          provide: NotificationService,
          useValue: mockNotificationService,
        },
      ],
    }).compile();

    service = module.get<GroupService>(GroupService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create group', () => {
    const createGroupDto: CreateGroupDto = {
      name: 'group name',
    };
    it('should create a group no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, groupsJoined: [] }));
      expect(await service.createGroup(userId, createGroupDto)).toEqual({
        groupCreated: {
          _id: expect.any(String),
          admin: userId,
          name: createGroupDto.name,
          members: [{ user: userId, role: AppObject.GROUP_ROLE.ADMIN, date: expect.any(Date) }],
        },
        userUpdated: { _id: userId, groupsJoined: [{ group: expect.any(String), date: expect.any(Date) }] },
      });
    });
  });

  describe('send request join group', () => {
    it('should send request join group no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, groupsJoined: [] }));
      mockGroupRepository.findById.mockImplementation((id) => ({ _id: id, joinReqs: [] }));
      expect(await service.sendRequestJoinGroup(userId, groupId)).toEqual({
        _id: groupId,
        joinReqs: [{ user: userId, date: expect.any(Date) }],
      });
    });

    it('should throw exception when group not found', async () => {
      mockGroupRepository.findById.mockImplementation(() => null);
      expect(service.sendRequestJoinGroup(userId, groupId)).rejects.toThrowError(NotFoundException);
    });

    it('should throw exception when has joined group', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, groupsJoined: [{ group: groupId }] }));
      mockGroupRepository.findById.mockImplementation((id) => ({ _id: id, joinReqs: [] }));
      expect(service.sendRequestJoinGroup(userId, groupId)).rejects.toThrowError(BadRequestException);
    });

    it('should throw exception when had sent req join group', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, groupsJoined: [] }));
      mockGroupRepository.findById.mockImplementation((id) => ({ _id: id, joinReqs: [{ user: userId }] }));
      expect(service.sendRequestJoinGroup(userId, groupId)).rejects.toThrowError(BadRequestException);
    });
  });

  describe('check has sensor role in group', () => {
    it('should has sensor role in group no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, groupsJoined: [{ group: groupId }] }));
      mockGroupRepository.findById.mockImplementation((id) => ({
        _id: id,
        members: [{ user: userId, role: expect.any(!AppObject.GROUP_ROLE.BASIC) }],
      }));
      expect(await service._checkHasSensorRoleInGroup(userId, groupId)).toEqual(true);
    });

    it('should throw exception when user not found', async () => {
      mockUserRepository.getUserById.mockImplementation(() => null);
      expect(service._checkHasSensorRoleInGroup(userId, groupId)).rejects.toThrowError(NotFoundException);
    });

    it('should throw exception when group not found', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, groupsJoined: [{ group: groupId }] }));
      mockGroupRepository.findById.mockImplementation((id) => null);
      expect(service._checkHasSensorRoleInGroup(userId, groupId)).rejects.toThrowError(NotFoundException);
    });

    it('should throw exception when has not join group', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id }));
      mockGroupRepository.findById.mockImplementation((id) => ({ _id: id, members: [{ user: expect.any(!userId) }] }));
      expect(service._checkHasSensorRoleInGroup(userId, groupId)).rejects.toThrowError(BadRequestException);
    });

    it('should return false when not has sensor role', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, groupsJoined: [{ group: groupId }] }));
      mockGroupRepository.findById.mockImplementation((id) => ({
        _id: id,
        members: [{ user: userId, role: AppObject.GROUP_ROLE.BASIC }],
      }));
      expect(await service._checkHasSensorRoleInGroup(userId, groupId)).toEqual(false);
    });
  });

  describe('get all request join group', () => {
    it('should return all request join group', async () => {
      jest.spyOn(service, '_checkHasSensorRoleInGroup').mockResolvedValue(true);
      mockGroupRepository.findById.mockImplementation((id) => ({
        _id: id,
        joinReqs: [],
      }));
      expect(await service.getAllRequestJoinGroup(userId, groupId)).toEqual([]);
    });

    it('should throw exception where user does not have permission', async () => {
      jest.spyOn(service, '_checkHasSensorRoleInGroup').mockResolvedValue(false);
      expect(service.getAllRequestJoinGroup(userId, groupId)).rejects.toThrowError(ForbiddenException);
    });
  });

  describe('accept request join group', () => {
    const reqId = '6302e4c9bf75b154e03a8136';
    it('should accept request join group no exception', async () => {
      jest.spyOn(service, '_checkHasSensorRoleInGroup').mockResolvedValue(true);
      mockGroupRepository.findById.mockImplementation((id) => ({
        _id: id,
        joinReqs: [{ _id: reqId, user: userId }],
        members: [],
      }));
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, groupsJoined: [] }));
      expect(await service.acceptRequestJoinGroup(userId, groupId, reqId)).toEqual({
        groupUpdated: {
          _id: groupId,
          joinReqs: [],
          members: [{ user: userId, date: expect.any(Date), role: AppObject.GROUP_ROLE.BASIC }],
        },
        userUpdated: {
          _id: expect.any(String),
          groupsJoined: [{ group: groupId, date: expect.any(Date) }],
        },
      });
    });

    it('should throw exception when request not found', async () => {
      jest.spyOn(service, '_checkHasSensorRoleInGroup').mockResolvedValue(true);
      mockGroupRepository.findById.mockImplementation((id) => ({
        _id: id,
        joinReqs: [],
        members: [],
      }));
      expect(service.acceptRequestJoinGroup(userId, groupId, reqId)).rejects.toThrowError(NotFoundException);
    });
  });

  describe('reject request join group', () => {
    const reqId = '6302e4c9bf75b154e03a8136';
    it('should reject request join group no exception', async () => {
      jest.spyOn(service, '_checkHasSensorRoleInGroup').mockResolvedValue(true);
      mockGroupRepository.findById.mockImplementation((id) => ({
        _id: id,
        joinReqs: [{ _id: reqId, user: userId }],
      }));
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, groupsJoined: [] }));
      expect(await service.rejectRequestJoinGroup(userId, groupId, reqId)).toEqual({
        _id: groupId,
        joinReqs: [],
      });
    });

    it('should throw exception when request not found', async () => {
      jest.spyOn(service, '_checkHasSensorRoleInGroup').mockResolvedValue(true);
      mockGroupRepository.findById.mockImplementation((id) => ({
        _id: id,
        joinReqs: [],
      }));
      expect(service.rejectRequestJoinGroup(userId, groupId, reqId)).rejects.toThrowError(NotFoundException);
    });
  });

  describe('change member role', () => {
    const memberId = '62fe06aff024ef280c2f887b';
    const updateMemberRole: UpdateMemberRole = {
      role: AppObject.GROUP_ROLE.SENSOR,
    };
    it('should change member role no exception', async () => {
      mockGroupRepository.findById.mockImplementation((id) => ({
        _id: id,
        admin: userId,
        members: [{ user: memberId, role: AppObject.GROUP_ROLE.BASIC }],
      }));
      expect(await service.changeMemberRole(userId, groupId, memberId, updateMemberRole)).toEqual({
        _id: groupId,
        admin: userId,
        members: [{ user: memberId, role: updateMemberRole.role }],
      });
    });

    it('should throw exception when group not found', async () => {
      mockGroupRepository.findById.mockImplementation((id) => null);
      expect(service.changeMemberRole(userId, groupId, memberId, updateMemberRole)).rejects.toThrowError(
        NotFoundException,
      );
    });

    it('should throw exception when user not admin', async () => {
      mockGroupRepository.findById.mockImplementation((id) => ({
        _id: id,
        admin: expect.any(!userId),
        members: [{ user: memberId, role: AppObject.GROUP_ROLE.BASIC }],
      }));
      expect(service.changeMemberRole(userId, groupId, memberId, updateMemberRole)).rejects.toThrowError(
        ForbiddenException,
      );
    });

    it('should throw exception when member not found', async () => {
      mockGroupRepository.findById.mockImplementation((id) => ({
        _id: id,
        admin: userId,
        members: [{ user: expect.any(!memberId) }],
      }));
      expect(service.changeMemberRole(userId, groupId, memberId, updateMemberRole)).rejects.toThrowError(
        NotFoundException,
      );
    });

    it('should throw exception when role updated was admin', async () => {
      const updateMemberRole = {
        role: AppObject.GROUP_ROLE.ADMIN,
      };
      mockGroupRepository.findById.mockImplementation((id) => ({
        _id: id,
        admin: userId,
        members: [{ user: memberId, role: AppObject.GROUP_ROLE.BASIC }],
      }));
      expect(service.changeMemberRole(userId, groupId, memberId, updateMemberRole)).rejects.toThrowError(
        ForbiddenException,
      );
    });
  });
});
