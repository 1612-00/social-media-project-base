import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { AppObject, ERROR } from '../../share/common';
import { CreatePostDto } from '../post/dto';
import { PostRepository } from '../post/post.repository';
import { PostService } from '../post/post.service';
import { UserRepository } from '../user/user.repository';
import { CreateGroupDto, UpdateMemberRole } from './dto';
import { GroupRepository } from './group.repository';
import { Parser } from 'json2csv';
import { NotificationService } from '../notification/notification.service';
import { CreateNotificationDto } from '../notification/dto/createNotificationDto';

@Injectable()
export class GroupService {
  constructor(
    private readonly groupRepository: GroupRepository,
    private readonly userRepository: UserRepository,
    private readonly postService: PostService,
    private readonly postRepository: PostRepository,
    private readonly notificationService: NotificationService,
  ) {}

  async createGroup(userId: string, createGroupDto: CreateGroupDto) {
    try {
      const newGroup = {
        ...createGroupDto,
        admin: userId,
        members: [
          {
            user: userId,
            role: AppObject.GROUP_ROLE.ADMIN,
            date: new Date(Date.now()),
          },
        ],
      };
      const groupCreated = await this.groupRepository.create(newGroup);

      const userFound = await this.userRepository.detailById(userId);

      const userUpdated = await this.userRepository.findOneByIdAndUpdate(userId, {
        groupsJoined: [...userFound.groupsJoined, { group: groupCreated._id, date: new Date(Date.now()) }],
      });

      return { groupCreated, listGroupJoined: userUpdated.groupsJoined };
    } catch (error) {
      throw error;
    }
  }

  async detailGroupById(userId: string, groupId: string) {
    try {
      const groupDetail = await this.groupRepository.findById(groupId);
      if (!groupDetail) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      return groupDetail;
    } catch (error) {
      throw error;
    }
  }

  async sendRequestJoinGroup(userId: string, groupId: string) {
    try {
      const userFound = await this.userRepository.detailById(userId);
      const groupFound = await this.groupRepository.findById(groupId);
      if (!groupFound) throw new NotFoundException(ERROR.GROUP_NOT_FOUND.MESSAGE);

      // Already member
      const hasJoined = userFound.groupsJoined.find((groupJoined) => groupJoined.group.toString() === groupId);
      if (hasJoined) throw new BadRequestException(ERROR.HAS_JOINED_GROUP.MESSAGE);

      // Already sent request
      const hadSentReq = groupFound.joinReqs.find((req) => req.user.toString() === userId);
      if (hadSentReq) throw new BadRequestException(ERROR.HAD_SENT_JOIN_REQ.MESSAGE);

      const groupUpdated = await this.groupRepository.findOneByIdAndUpdate(groupId, {
        joinReqs: [...groupFound.joinReqs, { user: userId, date: new Date(Date.now()) }],
      });

      // Create notification send req join group
      const newReqJoinGroupNotification: CreateNotificationDto = {
        senderId: userId,
        senderType: AppObject.NOTIFICATION_OBJECT.USER,
        receiverId: groupId,
        receiverType: AppObject.NOTIFICATION_OBJECT.GROUP,
        type: AppObject.NOTIFICATION_TYPE.REQ_JOIN_GROUP,
      };
      await this.notificationService.create(newReqJoinGroupNotification);

      return groupUpdated;
    } catch (error) {
      throw error;
    }
  }

  async _checkHasSensorRoleInGroup(userId: string, groupId: string) {
    try {
      const userFound = await this.userRepository.detailById(userId);
      if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      const groupFound = await this.groupRepository.findById(groupId);
      if (!groupFound) throw new NotFoundException(ERROR.GROUP_NOT_FOUND.MESSAGE);

      const hasJoined = groupFound.members.find((members) => members.user.toString() === userId);
      if (!hasJoined) throw new BadRequestException(ERROR.HAS_NOT_JOINED_GROUP.MESSAGE);

      // Basic user don't permissions
      if (hasJoined.role === AppObject.GROUP_ROLE.BASIC) return false;

      return true;
    } catch (error) {
      throw error;
    }
  }

  async getAllRequestJoinGroup(userId: string, groupId: string) {
    try {
      const checkSensorPermission = await this._checkHasSensorRoleInGroup(userId, groupId);
      if (!checkSensorPermission) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);
      const groupFound = await this.groupRepository.findById(groupId);
      return groupFound.joinReqs;
    } catch (error) {
      throw error;
    }
  }

  async acceptRequestJoinGroup(userId: string, groupId: string, reqId: string) {
    try {
      const checkSensorPermission = await this._checkHasSensorRoleInGroup(userId, groupId);
      if (!checkSensorPermission) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const groupFound = await this.groupRepository.findById(groupId);

      // Check reqId valid
      const requestFound = groupFound.joinReqs.find((req) => req._id.toString() === reqId);
      if (!requestFound) throw new NotFoundException(ERROR.JOIN_REQ_NOT_FOUND.MESSAGE);

      // Remove request and add member
      const newListReqs = groupFound.joinReqs.filter((req) => !(req._id.toString() === reqId));
      const groupUpdated = await this.groupRepository.findOneByIdAndUpdate(groupId, {
        joinReqs: newListReqs,
        members: [
          ...groupFound.members,
          { user: requestFound.user.toString(), role: AppObject.GROUP_ROLE.BASIC, date: new Date(Date.now()) },
        ],
      });

      // Add group joined in user
      const userSentReq = await this.userRepository.detailById(requestFound.user.toString());
      const userUpdated = await this.userRepository.findOneByIdAndUpdate(userSentReq._id.toString(), {
        groupsJoined: [...userSentReq.groupsJoined, { group: groupId, date: new Date(Date.now()) }],
      });

      // Create notification accept request join group
      const newAcceptReqJoinGroupNotification: CreateNotificationDto = {
        senderId: groupId,
        senderType: AppObject.NOTIFICATION_OBJECT.GROUP,
        receiverId: requestFound.user.toString(),
        receiverType: AppObject.NOTIFICATION_OBJECT.USER,
        type: AppObject.NOTIFICATION_TYPE.ACCEPT_REQ_JOIN_GROUP,
      };
      await this.notificationService.create(newAcceptReqJoinGroupNotification);

      return {
        listGroupMember: groupUpdated.members,
        listGroupJoined: userUpdated.groupsJoined,
      };
    } catch (error) {
      throw error;
    }
  }

  async rejectRequestJoinGroup(userId: string, groupId: string, reqId: string) {
    try {
      const checkSensorPermission = await this._checkHasSensorRoleInGroup(userId, groupId);
      if (!checkSensorPermission) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const groupFound = await this.groupRepository.findById(groupId);

      // Check reqId valid
      const requestFound = groupFound.joinReqs.find((req) => req._id.toString() === reqId);
      if (!requestFound) throw new NotFoundException(ERROR.JOIN_REQ_NOT_FOUND.MESSAGE);

      // Remove request
      const newListReqs = groupFound.joinReqs.filter((req) => !(req._id.toString() === reqId));
      const groupUpdated = await this.groupRepository.findOneByIdAndUpdate(groupId, {
        joinReqs: newListReqs,
      });

      return groupUpdated;
    } catch (error) {
      throw error;
    }
  }

  async changeMemberRole(userId: string, groupId: string, memberId: string, updateMemberRole: UpdateMemberRole) {
    try {
      const groupFound = await this.groupRepository.findById(groupId);
      if (!groupFound) throw new NotFoundException(ERROR.GROUP_NOT_FOUND.MESSAGE);

      // Check user has admin role
      if (!(groupFound.admin.toString() === userId)) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const memberFound = groupFound.members.find((member) => member.user.toString() === memberId);
      if (!memberFound) throw new NotFoundException(ERROR.MEMBER_NOT_FOUND.MESSAGE);

      // role updated is admin, throw exception
      if (updateMemberRole.role === AppObject.GROUP_ROLE.ADMIN)
        throw new ForbiddenException(ERROR.NOT_CHANGE_TO_ADMIN_ROLE.MESSAGE);

      memberFound.role = updateMemberRole.role;

      const groupUpdated = await this.groupRepository.save(groupFound);

      // Create notification change member role in group
      const newChangeMemberRoleInGroupNotification: CreateNotificationDto = {
        senderId: groupId,
        senderType: AppObject.NOTIFICATION_OBJECT.GROUP,
        receiverId: memberFound.user.toString(),
        receiverType: AppObject.NOTIFICATION_OBJECT.USER,
        type: AppObject.NOTIFICATION_TYPE.CHANGE_MEMBER_ROLE,
      };
      await this.notificationService.create(newChangeMemberRoleInGroupNotification);

      return groupUpdated;
    } catch (error) {
      throw error;
    }
  }

  async createPostInGroup(
    userId: string,
    groupId: string,
    createPostDto: CreatePostDto,
    files?: { images: Express.Multer.File[]; videos: Express.Multer.File[] },
  ) {
    try {
      const groupFound = await this.groupRepository.findById(groupId);
      if (!groupFound) throw new NotFoundException(ERROR.GROUP_NOT_FOUND.MESSAGE);

      // Create new post
      const postCreated = await this.postService.createPost(userId, createPostDto, files, groupId);
      if (!postCreated) {
        throw new BadRequestException(ERROR.CREATE_POST_FAILED.MESSAGE);
      }

      const memberFound = groupFound.members.find((member) => member.user.toString() === userId);
      if (!memberFound) throw new NotFoundException(ERROR.MEMBER_NOT_FOUND.MESSAGE);

      // Admin create post
      if (memberFound.role === AppObject.GROUP_ROLE.ADMIN || memberFound.role === AppObject.GROUP_ROLE.SENSOR) {
        // Create notification create post in group
        const newCreatePostInGroupNotification: CreateNotificationDto = {
          senderId: userId,
          senderType: AppObject.NOTIFICATION_OBJECT.USER,
          receiverId: groupId,
          receiverType: AppObject.NOTIFICATION_OBJECT.GROUP,
          type: AppObject.NOTIFICATION_TYPE.ADMIN_CREATE_POST_GROUP,
        };
        await this.notificationService.create(newCreatePostInGroupNotification);
        return await this.groupRepository.findOneByIdAndUpdate(groupId, {
          releasePosts: [...groupFound.releasePosts, { post: postCreated._id, date: new Date() }],
        });
      }

      // Basic member create post
      if (memberFound.role === AppObject.GROUP_ROLE.BASIC) {
        // Create notification create post in group
        const newCreatePostInGroupNotification: CreateNotificationDto = {
          senderId: userId,
          senderType: AppObject.NOTIFICATION_OBJECT.USER,
          receiverId: groupId,
          receiverType: AppObject.NOTIFICATION_OBJECT.GROUP,
          type: AppObject.NOTIFICATION_TYPE.BASIC_CREATE_POST_GROUP,
        };
        await this.notificationService.create(newCreatePostInGroupNotification);
        return await this.groupRepository.findOneByIdAndUpdate(groupId, {
          waitingPosts: [...groupFound.waitingPosts, { post: postCreated._id, date: new Date() }],
        });
      }
    } catch (error) {
      throw error;
    }
  }

  async getReleasePostsPagination(userId: string, groupId: string, page: number, limit: number) {
    try {
      const groupFound = await this.groupRepository.findById(groupId);
      if (!groupFound) throw new NotFoundException(ERROR.GROUP_NOT_FOUND.MESSAGE);

      const memberFound = groupFound.members.find((member) => member.user.toString() === userId);

      // Group private and user not is member
      if (groupFound.privacy === AppObject.GROUP_PRIVACY.PRIVATE && !memberFound)
        throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      return groupFound.releasePosts.slice((page - 1) * limit, (page - 1) * limit + limit);
    } catch (error) {
      throw error;
    }
  }

  async getWaitingPostsPagination(userId: string, groupId: string, page: number, limit: number) {
    try {
      const checkSensorPermission = await this._checkHasSensorRoleInGroup(userId, groupId);
      if (!checkSensorPermission) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const groupFound = await this.groupRepository.findById(groupId);

      return groupFound.waitingPosts.slice((page - 1) * limit, (page - 1) * limit + limit);
    } catch (error) {
      throw error;
    }
  }

  async acceptPost(userId: string, groupId: string, postId: string) {
    try {
      const checkSensorPermission = await this._checkHasSensorRoleInGroup(userId, groupId);
      if (!checkSensorPermission) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const groupFound = await this.groupRepository.findById(groupId);

      const waitingPostFound = await groupFound.waitingPosts.find(
        (waitingPost) => waitingPost._id.toString() === postId,
      );
      if (!waitingPostFound) throw new ForbiddenException(ERROR.POST_NOT_FOUND.MESSAGE);

      const waitingPostIndex = await groupFound.waitingPosts.findIndex(
        (waitingPost) => waitingPost.post.toString() === postId,
      );
      groupFound.waitingPosts.splice(waitingPostIndex, 1);
      await this.groupRepository.save(groupFound);

      // Detail waiting post
      const waitingPostDetail = await this.postRepository.detailById(waitingPostFound.post.toString());

      // Create notification accept waiting post in group
      const newAcceptWaitingPostInGroupNotification: CreateNotificationDto = {
        senderId: groupId,
        senderType: AppObject.NOTIFICATION_OBJECT.GROUP,
        receiverId: waitingPostDetail.author.toString(),
        receiverType: AppObject.NOTIFICATION_OBJECT.USER,
        type: AppObject.NOTIFICATION_TYPE.ACCEPT_WAITING_POST,
      };
      await this.notificationService.create(newAcceptWaitingPostInGroupNotification);

      const groupUpdated = await this.groupRepository.findOneByIdAndUpdate(groupId, {
        releasePosts: [...groupFound.releasePosts, { post: waitingPostFound._id, date: new Date() }],
      });
      return groupUpdated.releasePosts;
    } catch (error) {
      throw error;
    }
  }

  async rejectPost(userId: string, groupId: string, postId: string) {
    try {
      const checkSensorPermission = await this._checkHasSensorRoleInGroup(userId, groupId);
      if (!checkSensorPermission) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const groupFound = await this.groupRepository.findById(groupId);

      const waitingPostFound = await groupFound.waitingPosts.find(
        (waitingPost) => waitingPost._id.toString() === postId,
      );
      if (!waitingPostFound) throw new ForbiddenException(ERROR.POST_NOT_FOUND.MESSAGE);

      // Delete post rejected
      await this.postService.adminGroupDeletePost(groupId, waitingPostFound.post.toString());

      const waitingPostIndex = await groupFound.waitingPosts.findIndex(
        (waitingPost) => waitingPost.post.toString() === postId,
      );
      groupFound.waitingPosts.splice(waitingPostIndex, 1);
      return await this.groupRepository.save(groupFound);
    } catch (error) {
      throw error;
    }
  }

  async deleteReleasePost(userId: string, groupId: string, postId: string) {
    try {
      const checkSensorPermission = await this._checkHasSensorRoleInGroup(userId, groupId);
      if (!checkSensorPermission) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const groupFound = await this.groupRepository.findById(groupId);

      const postFound = groupFound.releasePosts.find((releasePost) => releasePost._id.toString() === postId);
      if (!postFound) throw new NotFoundException(ERROR.POST_NOT_FOUND.MESSAGE);

      const detailPost = await this.postRepository.detailById(postFound.post.toString());
      if (!detailPost) throw new NotFoundException(ERROR.POST_IN_GROUP_NOT_FOUND.MESSAGE);

      // If this post owner is admin and user not admin, throw exception
      if (detailPost.author.toString() === groupFound.admin.toString() && groupFound.admin.toString() !== userId)
        throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      // Delete post
      await this.postService.adminGroupDeletePost(groupId, detailPost._id.toString());

      // Remove post in list releasePosts
      const deletedPostIndex = await groupFound.releasePosts.findIndex(
        (releasePost) => releasePost._id.toString() === postId,
      );
      groupFound.releasePosts.splice(deletedPostIndex, 1);
      const groupUpdated = await this.groupRepository.save(groupFound);
      return groupUpdated.releasePosts;
    } catch (error) {
      throw error;
    }
  }

  async deleteMember(userId: string, groupId: string, memberId: string) {
    try {
      const checkSensorPermission = await this._checkHasSensorRoleInGroup(userId, groupId);
      if (!checkSensorPermission) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const groupFound = await this.groupRepository.findById(groupId);

      const userFound = groupFound.members.find((member) => member.user.toString() === userId);
      const memberFound = groupFound.members.find((member) => member._id.toString() === memberId);
      if (!memberFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      if (userId === memberFound.user.toString()) throw new ForbiddenException(ERROR.NOT_TRY_LOGOUT_FROM_EARTH.MESSAGE);

      if (
        memberFound.role === AppObject.GROUP_ROLE.ADMIN ||
        (userFound.role === AppObject.GROUP_ROLE.SENSOR && memberFound.role === AppObject.GROUP_ROLE.SENSOR)
      )
        throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const memberIndex = groupFound.members.findIndex((member) => member._id.toString() === memberId);
      groupFound.members.splice(memberIndex, 1);
      return await this.groupRepository.save(groupFound);
    } catch (error) {
      throw error;
    }
  }

  async blockUser(userId: string, groupId: string, memberId: string) {
    try {
      const checkSensorPermission = await this._checkHasSensorRoleInGroup(userId, groupId);
      if (!checkSensorPermission) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);
      const groupFound = await this.groupRepository.findById(groupId);
      const memberFound = groupFound.members.find((member) => member._id.toString() === memberId);

      // Delete member
      const memberDeleted = await this.deleteMember(userId, groupId, memberId);
      if (!memberDeleted) throw new BadRequestException(ERROR.DELETE_MEMBER_FAILED.MESSAGE);

      return await this.groupRepository.findOneByIdAndUpdate(groupId, {
        blockList: [
          ...groupFound.blockList,
          {
            user: memberFound.user.toString(),
            date: new Date(),
          },
        ],
      });
    } catch (error) {
      throw error;
    }
  }

  async statisticNewGroups(startDate: Date, endDate: Date) {
    try {
      const newGroups = await this.groupRepository.findByConditionDistinct({
        createdAt: { $gte: startDate, $lte: endDate },
      });
      return newGroups;
    } catch (error) {
      throw error;
    }
  }

  async statisticGroupActionWeekly(groupId, startDate: Date, endDate: Date) {
    try {
      const groupFound = await this.groupRepository.findById(groupId);
      if (!groupFound) throw new NotFoundException(ERROR.GROUP_NOT_FOUND.MESSAGE);

      // Number of new members
      let newMembersCount = 0;
      groupFound.members.forEach((member) => {
        if (member.date >= startDate && member.date <= endDate) newMembersCount++;
      });

      // Number of release posts
      let releasePostsCount = 0;
      groupFound.releasePosts.forEach((releasePost) => {
        if (releasePost.date >= startDate && releasePost.date <= endDate) releasePostsCount++;
      });

      const data = [
        {
          Type: 'Number of new members',
          Number: newMembersCount,
        },
        {
          Type: 'Number of release post',
          Number: releasePostsCount,
        },
      ];

      const json2csv = new Parser();
      const csv = json2csv.parse(data);
      return csv;
    } catch (error) {
      throw error;
    }
  }

  async getOwnNotificationsPaginate(userId: string, groupId: string, page: number, limit: number) {
    try {
      const groupFound = await this.groupRepository.findById(groupId);
      if (!groupFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      const memberFound = groupFound.members.find((member) => member.user.toString() === userId);
      if (!memberFound) throw new NotFoundException(ERROR.MEMBER_NOT_FOUND.MESSAGE);

      if (memberFound.role === AppObject.GROUP_ROLE.BASIC)
        throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      return await this.notificationService.findOwnNotification(groupId, page, limit);
    } catch (error) {
      throw error;
    }
  }
}
