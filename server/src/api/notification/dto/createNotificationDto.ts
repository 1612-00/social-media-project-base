import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { AppObject } from 'src/share/common';

export class CreateNotificationDto {
  @IsString()
  @IsNotEmpty()
  senderId: string;

  @IsString()
  @IsNotEmpty()
  @IsEnum(AppObject.NOTIFICATION_ACTOR)
  senderType: string;

  @IsString()
  @IsNotEmpty()
  receiverId: string;

  @IsString()
  @IsNotEmpty()
  @IsEnum(AppObject.NOTIFICATION_ACTOR)
  receiverType: string;

  @IsString()
  @IsNotEmpty()
  objectId?: string = null;

  @IsString()
  @IsNotEmpty()
  @IsEnum(AppObject.NOTIFICATION_OBJECT)
  objectType?: string = null;

  @IsString()
  @IsNotEmpty()
  @IsEnum(AppObject.NOTIFICATION_TYPE)
  type: string;
}
