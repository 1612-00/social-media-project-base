import { Controller, Get, Post, Body, Param, Delete, Query, Patch } from '@nestjs/common';
import { ApiCreatedResponse, ApiForbiddenResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CreateNotificationDto } from './dto/createNotificationDto';
import { SWAGGER_RESPONSE } from './notification.constant';
import { NotificationService } from './notification.service';

@ApiForbiddenResponse(SWAGGER_RESPONSE.FORBIDDEN_EXCEPTION)
@ApiNotFoundResponse(SWAGGER_RESPONSE.NOT_FOUND_EXCEPTION)
@ApiTags('Notification')
@Controller('notification')
export class NotificationController {
  constructor(private readonly notificationService: NotificationService) {}

  @Post()
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  create(@Body() createNotificationDto: CreateNotificationDto) {
    return this.notificationService.create(createNotificationDto);
  }

  @Get('owner-by-id/:objectId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  findOwnNotification(@Param('objectId') objectId: string, @Query('page') page: string, @Query('limit') limit: number) {
    return this.notificationService.findOwnNotification(objectId, +page || 1, +limit || 10);
  }

  @Get(':notificationId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  findOne(@Param('notificationId') notificationId: string) {
    return this.notificationService.findOne(notificationId);
  }

  @Delete(':notificationId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  remove(@Param('notificationId') notificationId: string) {
    return this.notificationService.remove(notificationId);
  }

  @Patch(':notificationId')
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  handleSeenNotification(@Param('notificationId') notificationId: string) {
    return this.notificationService.handleSeenNotification(notificationId);
  }
}
