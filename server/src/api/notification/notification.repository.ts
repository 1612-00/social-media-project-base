import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Notification, NotificationDocument } from '../../schemas/NotificationSchema';

export class NotificationRepository {
  constructor(@InjectModel(Notification.name) private notificationModel: Model<NotificationDocument>) {}

  create(createNotificationDto: Notification) {
    try {
      return this.notificationModel.create(createNotificationDto);
    } catch (error) {
      throw new Error('Notification created failed');
    }
  }

  save(param) {
    return new this.notificationModel(param).save();
  }

  aggregateByCondition(condition: any) {
    return this.notificationModel.aggregate(condition);
  }
  aggregateByConditionAndPopulate(condition: any, page: number, limit: number) {
    return this.notificationModel
      .aggregate(condition)
      .skip((page - 1) * limit)
      .limit(limit)
      .sort({ updatedAt: 'desc' })
      .exec();
  }

  findManyByCondition(condition: object) {
    return this.notificationModel.find(condition).exec();
  }

  findById(id: string) {
    return this.notificationModel.findById(id).exec();
  }

  findByIdPopulate(id: string) {
    return this.notificationModel.findById(id).populate('comments').exec();
  }

  findOneByCondition(condition: object) {
    return this.notificationModel.findOne(condition).exec();
  }

  findOneByIdAndUpdate(id: string, dataUpdate: object) {
    return this.notificationModel.findByIdAndUpdate(id, { $set: dataUpdate }, { new: true }).exec();
  }

  findOneByConditionAndUpdate(condition: object, dataUpdate: object) {
    return this.notificationModel.findOneAndUpdate(condition, { $set: dataUpdate }, { new: true }).exec();
  }

  findOneByIdAndDelete(id: string) {
    return this.notificationModel.findByIdAndDelete(id).exec();
  }

  deleteMany(condition: object, session?: any) {
    return session
      ? this.notificationModel.deleteMany(condition)
      : this.notificationModel.deleteMany(condition).session(session);
  }
}
