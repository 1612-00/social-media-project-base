import { Injectable, NotFoundException } from '@nestjs/common';
import { ERROR } from '../../share/common';
import { CreateNotificationDto } from './dto/createNotificationDto';
import { NotificationRepository } from './notification.repository';

@Injectable()
export class NotificationService {
  constructor(private readonly notificationRepository: NotificationRepository) {}

  async create(createNotificationDto: CreateNotificationDto) {
    try {
      const notificationFound = await this.notificationRepository.aggregateByCondition([
        {
          $match: {
            'sender.object': createNotificationDto.senderId,
            'receiver.object': createNotificationDto.receiverId,
            'object.object': createNotificationDto.objectId,
            type: createNotificationDto.type,
          },
        },
      ]);
      if (notificationFound.length > 0) {
        return await this.notificationRepository.findOneByIdAndUpdate(notificationFound[0]._id.toString(), {
          seen: false,
          updatedAt: new Date(),
        });
        // return notificationFound[0];
      }

      const newNotification = {
        sender: {
          object: createNotificationDto.senderId,
          type: createNotificationDto.senderType,
        },
        receiver: {
          object: createNotificationDto.receiverId,
          type: createNotificationDto.receiverType,
        },
        object: createNotificationDto.objectId
          ? {
              object: createNotificationDto.objectId,
              type: createNotificationDto.objectType,
            }
          : null,
        type: createNotificationDto.type,
        seen: false,
      };
      return await this.notificationRepository.create(newNotification);
    } catch (error) {
      throw error;
    }
  }

  async findOwnNotification(objectId: string, page: number, limit: number) {
    try {
      return await this.notificationRepository.aggregateByConditionAndPopulate(
        [{ $match: { 'receiver.object': objectId } }],
        page,
        limit,
      );
    } catch (error) {
      throw error;
    }
  }

  async findOne(notificationId: string) {
    try {
      const notification = await this.notificationRepository.findById(notificationId);
      if (!notification) throw new NotFoundException(ERROR.NOTIFICATION_NOT_FOUND.MESSAGE);
      return notification;
    } catch (error) {
      throw error;
    }
  }

  async remove(notificationId: string) {
    try {
      return await this.notificationRepository.findOneByIdAndDelete(notificationId);
    } catch (error) {
      throw error;
    }
  }

  async handleSeenNotification(notificationId: string) {
    try {
      return await this.notificationRepository.findOneByIdAndUpdate(notificationId, { seen: true });
    } catch (error) {
      throw error;
    }
  }
}
