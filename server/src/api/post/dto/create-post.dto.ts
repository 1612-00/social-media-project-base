import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString } from 'class-validator';
import { AppObject } from 'src/share/common';

export class CreatePostDto {
  @ApiProperty({ description: 'audience' })
  @IsString()
  @IsEnum(AppObject.POST_AUDIENCE)
  audience?: string = AppObject.POST_AUDIENCE.PUBLIC;

  @ApiProperty({ description: 'content' })
  @IsString()
  content?: string = '';
}
