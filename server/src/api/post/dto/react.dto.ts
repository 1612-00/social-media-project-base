import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { AppObject } from 'src/share/common';

export class ReactDto {
  @ApiProperty({ description: 'reactType' })
  @IsString()
  @IsNotEmpty()
  @IsEnum(AppObject.POST_REACT)
  reactType: string;
}
