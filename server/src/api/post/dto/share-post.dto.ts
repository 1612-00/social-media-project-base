import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString } from 'class-validator';
import { AppObject } from '../../../share/common';

export class ShareDto {
  @ApiProperty({ description: 'audience' })
  @IsString()
  @IsEnum(AppObject.POST_AUDIENCE)
  audience?: string = AppObject.POST_AUDIENCE.PUBLIC;
}
