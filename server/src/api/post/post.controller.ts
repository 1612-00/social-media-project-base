import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/share/auth/guard/jwt.guard';
import { GetUser } from 'src/share/decorators/get-user.decorator';
import { CreateCommentDto } from '../comment/dto';
import { CreatePostDto, ReactDto, ShareDto, UpdatePostDto } from './dto';
import { SWAGGER_RESPONSE } from './post.constant';
import { PostService } from './post.service';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiUnauthorizedResponse(SWAGGER_RESPONSE.UNAUTHORIZED_EXCEPTION)
@ApiForbiddenResponse(SWAGGER_RESPONSE.FORBIDDEN_EXCEPTION)
@ApiNotFoundResponse(SWAGGER_RESPONSE.NOT_FOUND_EXCEPTION)
@ApiTags('Post')
@Controller('post')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @Post()
  @UseInterceptors(FileFieldsInterceptor([{ name: 'images' }, { name: 'videos' }]))
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  @ApiBody({
    required: true,
    schema: {
      type: 'object',
      properties: {
        audience: {
          type: 'string',
        },
        content: {
          type: 'string',
        },
        images: {
          type: 'array',
          items: {
            type: 'string',
            format: 'binary',
          },
        },
        videos: {
          type: 'array',
          items: {
            type: 'string',
            format: 'binary',
          },
        },
      },
    },
  })
  createPost(
    @GetUser('id') userId: string,
    @Body() createPostDto: CreatePostDto,
    @UploadedFiles() files?: { images: Express.Multer.File[]; videos: Express.Multer.File[] },
  ) {
    return this.postService.createPost(userId, createPostDto, files);
  }

  @Patch(':postId')
  @ApiNotFoundResponse(SWAGGER_RESPONSE.UPDATE_RESPONSE)
  @ApiConsumes('application/x-www-form-urlencoded')
  updateContentPost(
    @GetUser('id') userId: string,
    @Param('postId') postId: string,
    @Body() updatePostDto: UpdatePostDto,
  ) {
    return this.postService.updateContentPost(userId, postId, updatePostDto);
  }

  @Delete(':postId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  deletePost(@GetUser('id') userId: string, @Param('postId') postId: string) {
    return this.postService.deletePost(userId, postId);
  }

  @Get()
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  getAllOwnPosts(@GetUser('id') userId: string) {
    return this.postService.getAllOwnPosts(userId);
  }

  @Get(':postId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  getPostById(@GetUser('id') userId: string, @Param('postId') postId: string) {
    return this.postService.getPostById(userId, postId);
  }

  @Post('react/:postId')
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  reactPost(@GetUser('id') userId: string, @Param('postId') postId: string, @Body() reactDto: ReactDto) {
    return this.postService.reactPost(userId, postId, reactDto);
  }

  @Post('share/:postId')
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  @ApiConsumes('application/x-www-form-urlencoded')
  sharePost(@GetUser('id') userId: string, @Param('postId') postId: string, @Body() shareDto: ShareDto) {
    return this.postService.sharePost(userId, postId, shareDto);
  }

  @Post('un-share/:postId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  unSharePost(@GetUser('id') userId: string, @Param('postId') postId: string) {
    return this.postService.unSharePost(userId, postId);
  }

  @Post('comment/:postId')
  @ApiConsumes('application/x-www-form-urlencoded')
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  commentPost(
    @GetUser('id') userId: string,
    @Param('postId') postId: string,
    @Body() createCommentDto: CreateCommentDto,
  ) {
    return this.postService.commentPost(userId, postId, createCommentDto);
  }

  @Post('reply-comment/:postId/:commentId')
  @ApiConsumes('application/x-www-form-urlencoded')
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  replyPostComment(
    @GetUser('id') userId: string,
    @Param('postId') postId: string,
    @Param('commentId') commentId: string,
    @Body() createCommentDto: CreateCommentDto,
  ) {
    return this.postService.replyCommentPost(userId, postId, commentId, createCommentDto);
  }

  @Get('timeline/paginate')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  getTimeLinePost(@GetUser('id') userId: string, @Query('page') page: string, @Query('limit') limit: string) {
    return this.postService.getTimelinePost(userId, parseInt(page) || 1, parseInt(limit) || 5);
  }

  @Get('comments/:postId')
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  getAllCommentInPost(@GetUser('id') userId: string, @Param('postId') postId: string) {
    return this.postService.getAllCommentInPost(userId, postId);
  }
}
