import { ConnectedSocket, MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { CommentRepository } from '../comment/comment.repository';
import { CreateCommentDto } from '../comment/dto';
import { UserRepository } from '../user/user.repository';
import { PostService } from './post.service';

@WebSocketGateway({
  transports: ['websocket'],
  cors: {
    origin: '*',
  },
})
export class PostGateway {
  @WebSocketServer()
  server: Server;

  constructor(
    private readonly postService: PostService,
    private readonly userRepository: UserRepository,
    private readonly commentRepository: CommentRepository,
  ) {}

  @SubscribeMessage('getPost')
  getPost(@MessageBody('postInfo') postInfo: { userId: string; postId: string }) {
    return this.postService.getPostById(postInfo.userId, postInfo.postId);
  }

  @SubscribeMessage('joinPost')
  handleJoinPost(client: Socket, payload: any) {
    client.join(payload.postId);
  }

  @SubscribeMessage('getUserById')
  getUserById(@MessageBody('userId') userId: string) {
    return this.userRepository.detailById(userId);
  }

  @SubscribeMessage('getCommentById')
  getCommentById(@MessageBody('commentId') commentId: string) {
    return this.commentRepository.findById(commentId);
  }

  @SubscribeMessage('createCommentPost')
  async createCommentPost(
    @MessageBody('postInfo')
    postInfo: {
      userId: string;
      postId: string;
      content: string;
    },
  ) {
    const createCommentDto: CreateCommentDto = {
      content: postInfo.content,
    };
    const newPost = (await this.postService.commentPost(postInfo.userId, postInfo.postId, createCommentDto))
      .postUpdated;
    this.server.to([postInfo.postId]).emit('newPost', newPost);
    return newPost;
  }

  @SubscribeMessage('replyCommentPost')
  async replyCommentPost(
    @MessageBody('postInfo')
    postInfo: {
      userId: string;
      postId: string;
      content: string;
      commentId: string;
    },
  ) {
    const createCommentDto: CreateCommentDto = {
      content: postInfo.content,
    };
    const newPost = (
      await this.postService.replyCommentPost(postInfo.userId, postInfo.postId, postInfo.commentId, createCommentDto)
    ).postUpdated;
    this.server.to(postInfo.postId).emit('newPost', newPost);
    return newPost;
  }

  @SubscribeMessage('reactPost')
  async reactPost(
    @MessageBody('reactInfo')
    reactInfo: {
      userId: string;
      postId: string;
      type: string;
    },
  ) {
    const newPost = await this.postService.reactPost(reactInfo.userId, reactInfo.postId, {
      reactType: reactInfo.type,
    });
    this.server.to(reactInfo.postId).emit('newPost', newPost);
    return newPost;
  }

  @SubscribeMessage('typing')
  async typing(
    @MessageBody() typingBody: { userId: string; postId: string; status: boolean },
    @ConnectedSocket() client: Socket,
  ) {
    client.broadcast
      .to(typingBody.postId)
      .emit('typing', { userTyping: typingBody.userId, isTyping: typingBody.status });
  }
}
