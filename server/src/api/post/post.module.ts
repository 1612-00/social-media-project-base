import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { JWT_CONFIG } from 'src/config';
import { Post, PostSchema } from 'src/schemas/PostSchema';
import { CommentModule } from '../comment/comment.module';
import { NotificationModule } from '../notification/notification.module';
import { UserModule } from '../user/user.module';
import { PostController } from './post.controller';
import { PostGateway } from './post.gateway';
import { PostRepository } from './post.repository';
import { PostService } from './post.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }]),
    JwtModule.register({
      secret: JWT_CONFIG.secret,
      signOptions: {
        expiresIn: JWT_CONFIG.expiresIn,
      },
    }),
    UserModule,
    CommentModule,
    NotificationModule,
  ],
  controllers: [PostController],
  providers: [PostService, PostRepository, PostGateway],
  exports: [PostService, PostRepository],
})
export class PostModule {}
