import { MongooseRepository } from '../../libs/standard';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Post, PostDocument } from 'src/schemas/PostSchema';

export class PostRepository extends MongooseRepository {
  constructor(@InjectModel(Post.name) private userModel: Model<PostDocument>) {
    super(userModel);
  }
}
