import { BadRequestException, ForbiddenException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppObject } from '../../share/common';
import { CommentService } from '../comment/comment.service';
import { NotificationService } from '../notification/notification.service';
import { UserRepository } from '../user/user.repository';
import { CreatePostDto, ReactDto, UpdatePostDto } from './dto';
import { PostRepository } from './post.repository';
import { PostService } from './post.service';

describe('PostService', () => {
  let service: PostService;
  const userId = '62f0d61aa5e7f04e7d5f99dc';
  const postId = '62f75a0adc91b4b2bf5ccb0c';

  const mockPostRepository = {
    create: jest.fn().mockImplementation((dto) => ({ _id: 'bleble', ...dto })),
    findById: jest.fn().mockImplementation((id) => ({ _id: id })),
    findManyByCondition: jest.fn().mockImplementation((condition) => [{ ...condition }]),
    findOneByIdAndUpdate: jest.fn().mockImplementation((id, data) => ({ ...data })),
    findOneByIdAndDelete: jest.fn().mockImplementation((id) => ({ _id: id, author: expect.any(String) })),
    getTimelinePost: jest.fn().mockImplementation((condition, page, limit) => [{ ...condition }]),
  };

  const mockUserRepository = {
    getUserById: jest.fn().mockImplementation((id) => ({ _id: id })),
    findOneByIdAndUpdate: jest.fn().mockImplementation((id, data) => ({ ...data })),
  };

  const mockCommentService = {
    createComment: jest.fn().mockImplementation((userId, postId, data) => ({
      author: userId,
      post: postId,
      ...data,
    })),
  };

  const mockNotificationService = {
    create: jest.fn().mockImplementation(() => true),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostService,
        {
          provide: PostRepository,
          useValue: mockPostRepository,
        },
        {
          provide: UserRepository,
          useValue: mockUserRepository,
        },
        {
          provide: CommentService,
          useValue: mockCommentService,
        },
        {
          provide: NotificationService,
          useValue: mockNotificationService,
        },
      ],
    }).compile();

    service = module.get<PostService>(PostService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create post', () => {
    const dto: CreatePostDto = {
      content: 'post content test',
    };

    // it('should be create post no exception', async () => {
    //   expect(await service.createPost(userId, dto)).toEqual({
    //     _id: expect.any(String),
    //     ...dto,
    //     author: userId,
    //   });
    // });

    // it('should be throw exception when post content is empty', async () => {
    //   const dtoContentEmpty = {
    //     content: '',
    //     images: [],
    //     videos: [],
    //   };
    //   expect(service.createPost(userId, dtoContentEmpty)).rejects.toBeInstanceOf(BadRequestException);
    // });
  });

  describe('update post content', () => {
    const dto: UpdatePostDto = {
      content: 'post content test',
    };

    it('should update post content no exception', async () => {
      mockPostRepository.findById.mockImplementation((postId) => ({
        _id: postId,
        author: userId,
      }));
      mockPostRepository.findOneByIdAndUpdate.mockImplementation((postId, dto) => ({
        _id: postId,
        author: userId,
        ...dto,
      }));
      expect(await service.updateContentPost(userId, postId, dto)).toEqual({
        _id: postId,
        author: userId,
        ...dto,
      });
    });

    it('should throw an exception when user not found', async () => {
      mockPostRepository.findById.mockImplementation((postId) => null);
      expect(service.updateContentPost(userId, postId, dto)).rejects.toThrowError(NotFoundException);
    });

    it('should throw an exception when user not the post author', async () => {
      mockPostRepository.findById.mockImplementation((postId) => ({
        _id: postId,
        author: expect.any(!userId),
      }));
      expect(service.updateContentPost(userId, postId, dto)).rejects.toThrowError(ForbiddenException);
    });

    it('should throw an exception when post content empty', async () => {
      const dtoEmptyContent = {
        content: '',
      };
      mockPostRepository.findById.mockImplementation((postId) => ({
        _id: postId,
        author: userId,
        images: [],
        videos: [],
      }));
      expect(service.updateContentPost(userId, postId, dtoEmptyContent)).rejects.toThrowError(BadRequestException);
    });

    it('should throw an exception when post will be update no has change', async () => {
      const dtoEmptyContent = {
        content: 'content update',
      };
      mockPostRepository.findById.mockImplementation((postId) => ({
        _id: postId,
        author: userId,
        content: dtoEmptyContent.content,
      }));
      expect(service.updateContentPost(userId, postId, dtoEmptyContent)).rejects.toThrowError(BadRequestException);
    });
  });

  describe('delete post', () => {
    // it('should delete post no exception', async () => {
    //   mockPostRepository.findById.mockImplementation((postId) => ({
    //     _id: postId,
    //     author: userId,
    //   }));
    //   expect(await service.deletePost(userId, postId)).toEqual({
    //     _id: postId,
    //     author: userId,
    //   });
    // });

    it('should throw exception when post not found', async () => {
      mockPostRepository.findById.mockImplementation((postId) => null);
      expect(service.deletePost(userId, postId)).rejects.toThrowError(NotFoundException);
    });

    it('should throw exception when user not onw this post', async () => {
      mockPostRepository.findById.mockImplementation((postId) => ({
        _id: postId,
        author: expect.any(!userId),
      }));
      expect(service.deletePost(userId, postId)).rejects.toThrowError(ForbiddenException);
    });
  });

  describe('get all owner posts', () => {
    it('should return all owner posts', async () => {
      expect(await service.getAllOwnPosts(userId)).toEqual([{ author: userId }]);
    });
  });

  describe('get post by id', () => {
    it('should return a post', async () => {
      // jest
      //   .spyOn(service, '_checkUserHasPermissionWithPost')
      //   .mockImplementation(async (userId, postId) => Promise.resolve(true));
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        author: expect.any(String),
        audience: AppObject.POST_AUDIENCE.PUBLIC,
      }));
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        blockList: [],
      }));
      expect(await service.getPostById(userId, postId)).toEqual({
        _id: postId,
        author: expect.any(String),
        audience: AppObject.POST_AUDIENCE.PUBLIC,
      });
    });
    it('should throw exception when dont permission access the post', () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        blockList: [{ user: userId }],
      }));
      expect(service.getPostById(userId, postId)).rejects.toThrowError(ForbiddenException);
    });
  });

  describe('react post', () => {
    const reactDto: ReactDto = {
      reactType: AppObject.POST_REACT.LIKE,
    };
    it('should react post when list react empty', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        blockList: [],
      }));
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        author: expect.any(String),
        audience: AppObject.POST_AUDIENCE.PUBLIC,
        reacts: [],
      }));
      expect(await service.reactPost(userId, postId, reactDto)).toEqual({
        _id: postId,
        author: expect.any(String),
        reacts: [
          {
            user: userId,
            reactType: reactDto.reactType,
            date: expect.any(Date),
          },
        ],
      });
    });
    it('should react post when post had react by user', async () => {
      const listReact = [{ user: expect.any(String) }];
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        author: expect.any(String),
        audience: AppObject.POST_AUDIENCE.PUBLIC,
        reacts: listReact,
      }));
      // List don't have react of this user
      const newListReact = listReact.filter((item) => !item.user.toString().includes(userId));
      expect(await service.reactPost(userId, postId, reactDto)).toEqual({
        _id: postId,
        author: expect.any(String),
        reacts: [
          ...newListReact,
          {
            user: userId,
            reactType: reactDto.reactType,
            date: expect.any(Date),
          },
        ],
      });
    });
    it('should un react post when user react the same type', async () => {
      const listReact = [{ user: userId, reactType: reactDto.reactType }];
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        author: expect.any(String),
        audience: AppObject.POST_AUDIENCE.PUBLIC,
        reacts: listReact,
      }));
      // List don't have react of this user
      const newListReact = listReact.filter((item) => !item.user.toString().includes(userId));
      expect(await service.reactPost(userId, postId, reactDto)).toEqual({
        _id: postId,
        author: expect.any(String),
        reacts: [...newListReact],
      });
    });
  });

  describe('share post', () => {
    it('should share the post no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        blockList: [],
        postsShared: [],
      }));
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        author: expect.any(String),
        audience: AppObject.POST_AUDIENCE.PUBLIC,
        shares: [],
      }));
      expect(await service.sharePost(userId, postId, {})).toEqual({
        postUpdated: {
          _id: postId,
          author: userId,
          shares: [
            {
              audience: AppObject.POST_AUDIENCE.PUBLIC,
              date: expect.any(Date),
              user: userId,
            },
          ],
        },
        userUpdated: {
          postsShared: [
            {
              audience: AppObject.POST_AUDIENCE.PUBLIC,
              date: expect.any(Date),
              post: postId,
            },
          ],
        },
      });
    });
    it('should throw error when user own this post', async () => {
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        author: userId,
        audience: AppObject.POST_AUDIENCE.PUBLIC,
        shares: [],
      }));
      expect(service.sharePost(userId, postId, {})).rejects.toThrowError(BadRequestException);
    });
    it('should throw error when had shared this post', async () => {
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        author: expect.any(!userId),
        audience: AppObject.POST_AUDIENCE.PUBLIC,
        shares: [{ user: userId }],
      }));
      expect(service.sharePost(userId, postId, {})).rejects.toThrowError(BadRequestException);
    });
  });

  describe('unshare post', () => {
    it('should unshare the post no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        blockList: [],
        postsShared: [{ post: postId }],
      }));
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        author: expect.any(String),
        audience: AppObject.POST_AUDIENCE.PUBLIC,
        shares: [{ user: userId }],
      }));
      expect(await service.unSharePost(userId, postId)).toEqual({
        postUpdated: {
          _id: postId,
          author: userId,
          shares: [],
        },
        userUpdated: {
          postsShared: [],
        },
      });
    });
    it('should throw error when user own this post', async () => {
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        author: userId,
        audience: AppObject.POST_AUDIENCE.PUBLIC,
        shares: [{ user: userId }],
      }));
      expect(service.unSharePost(userId, postId)).rejects.toThrowError(BadRequestException);
    });
    it('should throw error when user had not shared this post', async () => {
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        author: expect.any(!userId),
        audience: AppObject.POST_AUDIENCE.PUBLIC,
        shares: [],
      }));
      expect(service.unSharePost(userId, postId)).rejects.toThrowError(BadRequestException);
    });
  });

  describe('comment post', () => {
    const createCommentDto = {
      content: 'content',
    };
    it('should comment post no exception', async () => {
      jest.spyOn(service, '_checkUserHasPermissionWithPost').mockResolvedValue(true);
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        author: expect.any(!userId),
        audience: AppObject.POST_AUDIENCE.PUBLIC,
        shares: [],
        comments: [],
      }));
      expect(await service.commentPost(userId, postId, createCommentDto)).toEqual({
        newComment: {
          author: userId,
          post: postId,
          content: 'content',
        },
        postUpdated: {
          _id: postId,
          author: userId,
          comments: [{ mainComment: { author: userId, post: postId, content: 'content' } }],
        },
      });
    });
  });

  describe('reply comment', () => {
    const commentId = '62f0d61aa5e7f04e7d5hd8e9ds';
    const createCommentDto = {
      content: 'content',
    };
    const newComment = {
      _id: expect.any(String),
      author: userId,
      post: postId,
      content: createCommentDto.content,
    };
    // it('should reply comment no exception', async () => {
    //   jest.spyOn(service, '_checkUserHasPermissionWithPost').mockResolvedValue(true);
    //   mockPostRepository.findById.mockImplementation((id) => ({
    //     _id: id,
    //     audience: AppObject.POST_AUDIENCE.PUBLIC,
    //     comments: [{ _id: commentId, mainComment: expect.any(String), commentReplies: [] }],
    //   }));
    //   mockCommentService.createComment.mockImplementation((userId, postId, createCommentDto) => newComment);
    //   expect(await service.replyCommentPost(userId, postId, commentId, createCommentDto)).toEqual({
    //     newComment,
    //     postUpdated: {
    //       _id: postId,
    //       author: userId,
    //       comments: [
    //         {
    //           _id: commentId,
    //           mainComment: expect.any(String),
    //           commentReplies: [newComment._id],
    //         },
    //       ],
    //     },
    //   });
    // });

    it('should throw exception when post not found', async () => {
      jest.spyOn(service, '_checkUserHasPermissionWithPost').mockResolvedValue(true);
      mockPostRepository.findById.mockImplementation((id) => null);
      expect(service.replyCommentPost(userId, postId, commentId, createCommentDto)).rejects.toThrowError(
        NotFoundException,
      );
    });

    it('should throw exception when comment not found', async () => {
      jest.spyOn(service, '_checkUserHasPermissionWithPost').mockResolvedValue(true);
      mockPostRepository.findById.mockImplementation((id) => ({
        _id: id,
        audience: AppObject.POST_AUDIENCE.PUBLIC,
        comments: [],
      }));
      expect(service.replyCommentPost(userId, postId, commentId, createCommentDto)).rejects.toThrowError(
        NotFoundException,
      );
    });
  });

  // describe('get timeline post', () => {
  //   const page = 1;
  //   const limit = 5;
  //   it('should return timeline post no exception', async () => {
  //     mockUserRepository.getUserById.mockImplementation((id) => ({
  //       _id: id,
  //       friends: [{ user: expect.any(String) }],
  //     }));
  //     expect(await service.getTimelinePost(userId, page, limit)).toEqual({
  //       data: [{ author: userId }],
  //       page,
  //       limit,
  //     });
  //   });
  // });
});
