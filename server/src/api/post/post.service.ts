import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { getDownloadURL, listAll, ref, uploadBytes, deleteObject } from 'firebase/storage';
import { storage } from '../../config/firebase/config';
import { postStorage } from '../../config/firebase/firebase-storage';
import { Post } from '../../schemas/PostSchema';
import { AppObject, ERROR, FolderSaveFile } from '../../share/common';
import { CommentService } from '../comment/comment.service';
import { CreateCommentDto } from '../comment/dto';
import { UserRole } from '../user/role/role.enum';
import { UserRepository } from '../user/user.repository';
import { CreatePostDto, ReactDto, ShareDto, UpdatePostDto } from './dto';
import { PostRepository } from './post.repository';
import { CreateNotificationDto } from '../notification/dto/createNotificationDto';
import { NotificationService } from '../notification/notification.service';

@Injectable()
export class PostService {
  constructor(
    private readonly postRepository: PostRepository,
    private readonly userRepository: UserRepository,
    private readonly commentService: CommentService,
    private readonly notificationService: NotificationService,
  ) {}

  async createPost(
    userId: string,
    createPostDto: CreatePostDto,
    files?: { images: Express.Multer.File[]; videos: Express.Multer.File[] },
    groupId?: string,
  ) {
    try {
      if (
        (!createPostDto.content && !files.images && !files.videos) ||
        (createPostDto.content === '' && !files.images && !files.videos)
      ) {
        throw new BadRequestException(ERROR.POST_CONTENT_EMPTY.MESSAGE);
      }

      // Create new post
      const createDto = {
        ...createPostDto,
        author: userId,
        groupId: groupId ? groupId : null,
      };
      const postCreated = await this.postRepository.create(createDto);

      // Folder save files
      const folderSave = postStorage(postCreated._id);

      // Upload file to firebase server and update list url to post created
      if (files.images) {
        postCreated.images = await this._uploadFilesToFirebase(
          files.images,
          {
            mainFolder: folderSave,
            subFolder: 'images',
            fileExtension: 'jpeg',
          },
          'image/jpeg',
        );
      }
      if (files.videos) {
        postCreated.videos = await this._uploadFilesToFirebase(
          files.videos,
          {
            mainFolder: folderSave,
            subFolder: 'videos',
            fileExtension: 'mp4',
          },
          'video/mp4',
        );
      }
      if (!groupId) {
        const userFound = await this.userRepository.detailById(userId);
        await this.userRepository.findOneByIdAndUpdate(userId, {
          posts: [...userFound.posts, { post: postCreated._id, date: new Date() }],
        });
      }
      return await this.postRepository.save(postCreated);
    } catch (error) {
      throw error;
    }
  }

  /**
   * @param listFiles: Express.Multer.File[] - lists file updated to firebase
   * @param folderOption FolderSaveFile - interface folder save file
   * @param fileType string - content type of the file
   * @returns list url of files
   */
  async _uploadFilesToFirebase(
    listFiles: Express.Multer.File[],
    folderOption: FolderSaveFile,
    fileType: string,
  ): Promise<string[]> {
    try {
      // Upload list videos
      await Promise.all(
        listFiles.map(async (item, index) => {
          // Upload video to firebase server
          const saveAvatarRef = ref(
            storage,
            `${folderOption.mainFolder}/${folderOption.subFolder}/${index}.${folderOption.fileExtension}`,
          );
          const uploaded = await uploadBytes(saveAvatarRef, item.buffer, {
            contentType: fileType,
          });
          if (!uploaded) {
            throw new BadRequestException(ERROR.UPLOAD_AVATAR_FAILED.MESSAGE);
          }
          return uploaded;
        }),
      );

      // Retrieve urls from firebase server
      const getRef = ref(storage, `${folderOption.mainFolder}/${folderOption.subFolder}/`);
      const list = await listAll(getRef);
      const listUrls: Array<string> = [];
      for (let i = 0; i < list.items.length; i++) {
        const itemUrl = await getDownloadURL(list.items[i]);
        listUrls.push(itemUrl);
      }

      return listUrls;
    } catch (error) {
      throw error;
    }
  }

  async updateContentPost(userId: string, postId: string, updatePostDto: UpdatePostDto): Promise<Post> {
    try {
      const postFound = await this.postRepository.detailById(postId);
      if (!postFound) throw new NotFoundException(ERROR.POST_NOT_FOUND.MESSAGE);

      // User isn't the post author
      if (!postFound.author.toString().includes(userId))
        throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      // Check if the post updated empty
      if (updatePostDto.content === '' && postFound.images.length === 0 && postFound.videos.length === 0) {
        throw new BadRequestException(ERROR.POST_CONTENT_EMPTY.MESSAGE);
      }

      // Not have any changes
      if (updatePostDto.content === postFound.content) {
        throw new BadRequestException(ERROR.NOT_HAVE_ANY_CHANGE.MESSAGE);
      }

      return await this.postRepository.findOneByIdAndUpdate(postId, updatePostDto);
    } catch (error) {
      throw error;
    }
  }

  async deletePost(userId: string, postId: string) {
    const postFound = await this.postRepository.detailById(postId);
    if (!postFound) throw new NotFoundException(ERROR.POST_NOT_FOUND.MESSAGE);

    // User isn't the post author
    if (!postFound.author.toString().includes(userId))
      throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

    try {
      postFound.images.forEach(async (image) => {
        const fileRef = ref(storage, image);
        await deleteObject(fileRef);
      });
      postFound.videos.forEach(async (video) => {
        const fileRef = ref(storage, video);
        await deleteObject(fileRef);
      });
      return await this.postRepository.findByIdAndDelete(postId);
    } catch (error) {
      throw Error(error);
    }
  }

  async adminGroupDeletePost(groupId: string, postId: string) {
    const postFound = await this.postRepository.detailById(postId);
    if (!postFound) throw new NotFoundException(ERROR.POST_NOT_FOUND.MESSAGE);

    // Post not in group
    if (!(postFound.groupId.toString() === groupId)) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

    try {
      postFound.images &&
        postFound.images.forEach(async (image) => {
          const fileRef = ref(storage, image);
          await deleteObject(fileRef);
        });
      postFound.videos &&
        postFound.videos.forEach(async (video) => {
          const fileRef = ref(storage, video);
          await deleteObject(fileRef);
        });
      return await this.postRepository.findByIdAndDelete(postId);
    } catch (error) {
      throw Error(error);
    }
  }

  async getAllOwnPosts(userId: string): Promise<Array<Post>> {
    try {
      return await this.postRepository.findManyByCondition({ author: userId });
    } catch (error) {
      throw error;
    }
  }

  async getPostById(userId: string, postId: string): Promise<Post> {
    try {
      const checkPermission = await this._checkUserHasPermissionWithPost(userId, postId);
      if (!checkPermission) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const postFound = await this.postRepository.detailById(postId);

      return postFound;
    } catch (error) {
      throw error;
    }
  }

  /**
   * @Description Check user has access and interactive with post
   * @param userId
   * @param postId
   * @Returns {boolean}
   */
  async _checkUserHasPermissionWithPost(userId: string, postId: string): Promise<boolean> {
    try {
      const userFound = await this.userRepository.detailById(userId);
      if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);
      const postFound = await this.postRepository.detailById(postId);
      if (!postFound) throw new NotFoundException(ERROR.POST_NOT_FOUND.MESSAGE);

      // If is author
      if (postFound.author.toString().includes(userId)) return true;

      if (userFound.role === UserRole.Admin) return true;

      // If not author, check has blocked by owner
      const owner = await this.userRepository.detailById(postFound.author.toString());
      const hasBlockedByOwner = owner.blockList.find((blockedUser) => blockedUser.user.toString().includes(userId));
      if (hasBlockedByOwner) return false;

      // Check audience
      switch (postFound.audience) {
        case AppObject.POST_AUDIENCE.ONLY_ME:
          return false;
        case AppObject.POST_AUDIENCE.FRIENDS:
          const isFriend = userFound.friends.find((friend) =>
            friend.user.toString().includes(postFound.author.toString()),
          );
          return isFriend ? true : false;
        case AppObject.POST_AUDIENCE.PUBLIC:
          return true;
      }
    } catch (error) {
      throw error;
    }
  }

  /**
   * @Description Handle event the user react a post
   * @param userId
   * @param postId
   * @param reactDto
   * @returns {Post}
   */
  async reactPost(userId: string, postId: string, reactDto: ReactDto) {
    try {
      const checkPermission = await this._checkUserHasPermissionWithPost(userId, postId);
      if (!checkPermission) throw new NotFoundException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const newReact = {
        user: userId,
        reactType: reactDto.reactType,
        date: new Date(Date.now()),
      };

      const postFound = await this.postRepository.detailById(postId);

      // List reacts of post is empty, add this user's react
      if (postFound.reacts.length === 0) {
        const postUpdated = await this.postRepository.findOneByIdAndUpdate(postId, {
          reacts: [newReact],
        });
        this._createNotificationWhenReactPost(userId, postFound.author.toString(), postId, reactDto.reactType);
        return postUpdated;
      }

      // Check this user had react this post
      const hadReact = postFound.reacts.find((react) => react.user.toString() === userId);

      // List react has't have this user react
      const newListReact = postFound.reacts.filter((react) => !(react.user.toString() === userId));

      // This user had react this post and new react is the same type, delete react
      if (hadReact && hadReact.reactType.includes(reactDto.reactType)) {
        const postUpdated = await this.postRepository.findOneByIdAndUpdate(postId, {
          reacts: newListReact,
        });
        return postUpdated;
      }

      // Else
      const postUpdated = await this.postRepository.findOneByIdAndUpdate(postId, {
        reacts: [...newListReact, newReact],
      });

      this._createNotificationWhenReactPost(userId, postFound.author.toString(), postId, reactDto.reactType);

      return postUpdated;
    } catch (error) {
      throw error;
    }
  }

  async _createNotificationWhenReactPost(
    userIdReacted: string,
    userIdPostAuthor: string,
    postId: string,
    reactType: string,
  ) {
    try {
      // Create notification react post
      if (userIdReacted !== userIdPostAuthor) {
        const newReactPostNotification: CreateNotificationDto = {
          senderId: userIdReacted,
          senderType: AppObject.NOTIFICATION_ACTOR.USER,
          receiverId: userIdPostAuthor,
          receiverType: AppObject.NOTIFICATION_ACTOR.USER,
          objectId: postId,
          objectType: AppObject.NOTIFICATION_OBJECT.POST,
          type:
            reactType === AppObject.POST_REACT.LIKE
              ? AppObject.NOTIFICATION_TYPE.LIKE_POST
              : AppObject.NOTIFICATION_TYPE.LOVE_POST,
        };
        await this.notificationService.create(newReactPostNotification);
      }
    } catch (error) {
      throw error;
    }
  }

  /**
   * @Description Handle event the user share a post
   * @param userId
   * @param postId
   * @param shareDto
   */
  async sharePost(userId: string, postId: string, shareDto: ShareDto) {
    try {
      const checkPermission = await this._checkUserHasPermissionWithPost(userId, postId);
      if (!checkPermission) throw new NotFoundException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const postFound = await this.postRepository.detailById(postId);
      const userFound = await this.userRepository.detailById(userId);

      if (postFound.author.toString() === userId) throw new BadRequestException(ERROR.NOT_SHARE_OWN_POST.MESSAGE);

      // Check had shared this post
      const hadShared = postFound.shares.find((share) => share.user.toString() === userId);

      if (hadShared) throw new BadRequestException(ERROR.HAD_SHARED_THIS_POST.MESSAGE);

      // Add user in list shares of this post
      const postUpdated = await this.postRepository.findOneByIdAndUpdate(postId, {
        shares: [
          ...postFound.shares,
          {
            user: userId,
            audience: shareDto.audience ? shareDto.audience : AppObject.POST_AUDIENCE.PUBLIC,
            date: new Date(Date.now()),
          },
        ],
      });

      // Add post in list postsShared of user
      const userUpdated = await this.userRepository.findOneByIdAndUpdate(userId, {
        postsShared: [
          ...userFound.postsShared,
          {
            post: postId,
            audience: shareDto.audience ? shareDto.audience : AppObject.POST_AUDIENCE.PUBLIC,
            date: new Date(Date.now()),
          },
        ],
      });

      // Create notification share post
      const newSharePostNotification: CreateNotificationDto = {
        senderId: userId,
        senderType: AppObject.NOTIFICATION_ACTOR.USER,
        receiverId: postFound.author.toString(),
        receiverType: AppObject.NOTIFICATION_ACTOR.USER,
        objectId: postFound._id.toString(),
        objectType: AppObject.NOTIFICATION_OBJECT.POST,
        type: AppObject.NOTIFICATION_TYPE.SHARE_POST,
      };
      await this.notificationService.create(newSharePostNotification);

      return {
        postUpdated,
        userUpdated,
      };
    } catch (error) {
      throw error;
    }
  }

  /**
   * @Description Handle event the user share a post
   * @param userId
   * @param postId
   */
  async unSharePost(userId: string, postId: string) {
    try {
      const checkPermission = await this._checkUserHasPermissionWithPost(userId, postId);
      if (!checkPermission) throw new NotFoundException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const userFound = await this.userRepository.detailById(userId);
      const postFound = await this.postRepository.detailById(postId);

      if (postFound.author.toString().includes(userId)) throw new BadRequestException(ERROR.NOT_SHARE_OWN_POST.MESSAGE);

      // Check had shared this post
      const hadShared = postFound.shares.find((share) => share.user.toString().includes(userId));

      if (!hadShared) throw new BadRequestException(ERROR.HAVE_NOT_SHARED_THIS_POST_YET.MESSAGE);

      // Remove user in list shares of this post
      const newListSharer = postFound.shares.filter((share) => !share.user.toString().includes(userId));
      const postUpdated = await this.postRepository.findOneByIdAndUpdate(postId, {
        shares: newListSharer,
      });

      // Remove post in list postsShared of user
      const newListPostShared = userFound.postsShared.filter((share) => !share.post.toString().includes(postId));
      const userUpdated = await this.userRepository.findOneByIdAndUpdate(userId, {
        postsShared: newListPostShared,
      });

      return {
        postUpdated,
        userUpdated,
      };
    } catch (error) {
      throw error;
    }
  }

  /**
   * @Description Handle event user comment the post
   * @param userId
   * @param postId
   * @param createCommentDto
   * @returns {newPost, postUpdated}
   */
  async commentPost(userId: string, postId: string, createCommentDto: CreateCommentDto) {
    try {
      const checkPermission = await this._checkUserHasPermissionWithPost(userId, postId);
      if (!checkPermission) throw new NotFoundException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const postFound = await this.postRepository.detailById(postId);

      const newComment = await this.commentService.createComment(userId, postId, createCommentDto);

      const postUpdated = await this.postRepository.findOneByIdAndUpdate(postId, {
        comments: [...postFound.comments, { mainComment: newComment }],
      });

      // Create notification comment post
      const newCommentPostNotification: CreateNotificationDto = {
        senderId: userId,
        senderType: AppObject.NOTIFICATION_ACTOR.USER,
        receiverId: postFound.author.toString(),
        receiverType: AppObject.NOTIFICATION_ACTOR.USER,
        objectId: postFound._id.toString(),
        objectType: AppObject.NOTIFICATION_OBJECT.POST,
        type: AppObject.NOTIFICATION_TYPE.COMMENT_POST,
      };
      await this.notificationService.create(newCommentPostNotification);

      return {
        newComment,
        postUpdated,
      };
    } catch (error) {
      throw error;
    }
  }

  /**
   * @Description Handle reply a comment in post
   * @param userId
   * @param postId
   * @param commentId
   * @param createCommentDto
   * @returns {newComment, postUpdated}
   */
  async replyCommentPost(userId: string, postId: string, commentId: string, createCommentDto: CreateCommentDto) {
    try {
      const checkPermission = await this._checkUserHasPermissionWithPost(userId, postId);
      if (!checkPermission) throw new NotFoundException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const postFound = await this.postRepository.detailById(postId);
      if (!postFound) throw new NotFoundException(ERROR.POST_NOT_FOUND.MESSAGE);

      const hasComment = postFound.comments.find((comment) => comment._id.toString() == commentId);
      if (!hasComment) throw new NotFoundException(ERROR.COMMENT_NOT_FOUND.MESSAGE);

      const newComment = await this.commentService.createComment(userId, postId, createCommentDto);

      hasComment.commentReplies.push(newComment._id);
      const newListComment = postFound.comments.map((comment) =>
        comment._id.toString() == commentId ? hasComment : comment,
      );
      const postUpdated = await this.postRepository.findOneByIdAndUpdate(postId, {
        comments: newListComment,
      });

      // Create notification comment post
      const newCommentPostNotification: CreateNotificationDto = {
        senderId: userId,
        senderType: AppObject.NOTIFICATION_ACTOR.USER,
        receiverId: postFound.author.toString(),
        receiverType: AppObject.NOTIFICATION_ACTOR.USER,
        objectId: postFound._id.toString(),
        objectType: AppObject.NOTIFICATION_OBJECT.USER,
        type: AppObject.NOTIFICATION_TYPE.COMMENT_POST,
      };
      await this.notificationService.create(newCommentPostNotification);

      // Create notification reply comment post
      const detailHasComment = await this.commentService.getCommentById(hasComment.mainComment.toString());
      if (userId !== detailHasComment.author.toString()) {
        const newReplyCommentPostNotification: CreateNotificationDto = {
          senderId: userId,
          senderType: AppObject.NOTIFICATION_ACTOR.USER,
          receiverId: detailHasComment.author.toString(),
          receiverType: AppObject.NOTIFICATION_ACTOR.USER,
          objectId: detailHasComment._id.toString(),
          objectType: AppObject.NOTIFICATION_OBJECT.COMMENT,
          type: AppObject.NOTIFICATION_TYPE.REPLY_COMMENT_POST,
        };
        await this.notificationService.create(newReplyCommentPostNotification);
      }

      return {
        newComment,
        postUpdated,
      };
    } catch (error) {
      throw error;
    }
  }

  /**
   * @Description Handle get posts of user'friends, paginate by page and limit
   * @param userId
   * @param page
   * @param limit
   * @returns {listPost, page, limit}
   */
  async getTimelinePost(userId: string, page: number, limit: number) {
    try {
      const userFound = await this.userRepository.detailById(userId);

      const data = await Promise.all(
        userFound.friends.map((friend) => {
          return this.postRepository.findByConditionPaginateAndSort(
            { author: friend.user, groupId: null },
            page,
            limit,
          );
        }),
      );

      // const data = await this.postRepository.getTimelinePost(page, limit, options);

      return { listPost: [].concat(...data), page, limit };
    } catch (error) {
      throw error;
    }
  }

  async getAllCommentInPost(userId: string, postId: string) {
    try {
      const checkPermission = await this._checkUserHasPermissionWithPost(userId, postId);
      if (!checkPermission) throw new NotFoundException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const postFound = await this.postRepository.findByIdPopulate(postId, 'comments');

      const data = await Promise.all(
        postFound.comments.map(async (comment) => {
          return {
            mainComment: await this.commentService.getCommentById(comment.mainComment.toString()),
            commentReplies: await Promise.all(
              comment.commentReplies.map((reply) => {
                return this.commentService.getCommentById(reply.toString());
              }),
            ),
          };
        }),
      );

      return data;
    } catch (error) {
      throw error;
    }
  }

  // async deleteAllPostOwnerById(ownerId: string, session?: any) {
  //   try {
  //     return await this.postRepository.deleteMany({ author: ownerId }, session);
  //   } catch (error) {
  //     throw error;
  //   }
  // }
}
