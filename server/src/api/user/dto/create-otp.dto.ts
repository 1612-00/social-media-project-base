import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class CreateOTPDto {
  @ApiProperty({ description: 'email' })
  @IsEmail()
  @IsString()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ description: 'phoneNumber' })
  @IsString()
  @IsNotEmpty()
  phoneNumber: string;
}
