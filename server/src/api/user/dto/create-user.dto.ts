import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { UserRole } from '../role/role.enum';

export class CreateUserDto {
  @ApiProperty({ description: 'email' })
  @IsEmail()
  @IsString()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ description: 'password' })
  @IsString()
  @IsNotEmpty()
  password: string;

  @ApiProperty({ description: 'fullName' })
  @IsString()
  @IsNotEmpty()
  fullName: string;

  // @ApiProperty({ description: 'role', default: UserRole.User })
  // @IsString()
  // @IsEnum(UserRole)
  // role?: string = UserRole.User;
}
