import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty } from 'class-validator';

export class SetOpen2StepVerify {
  @ApiProperty({ description: 'isOpen' })
  @IsBoolean()
  @IsNotEmpty()
  isOpen: boolean;
}
