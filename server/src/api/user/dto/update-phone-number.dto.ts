import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Matches } from 'class-validator';

export class UpdatePhoneNumberDto {
  @ApiProperty({ description: 'phoneNumber' })
  @IsString()
  @Matches(/(84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  phoneNumber: string;

  @ApiProperty({ description: 'otp' })
  @IsString()
  @IsNotEmpty()
  otp: string;
}
