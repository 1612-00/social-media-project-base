import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class UpdateUserDto {
  @ApiProperty({ description: 'address' })
  @IsString()
  address: string;

  @ApiProperty({ description: 'job' })
  @IsString()
  job?: string = '';
}
