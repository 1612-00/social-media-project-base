import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';

export class VerifyOTPDto {
  @ApiProperty({ description: 'email' })
  @IsEmail()
  @IsString()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ description: 'otp' })
  @IsString()
  @IsNotEmpty()
  @Length(6)
  otp: string;
}
