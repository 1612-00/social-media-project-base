import {
  Body,
  Controller,
  Delete,
  FileTypeValidator,
  Get,
  MaxFileSizeValidator,
  Param,
  ParseFilePipe,
  Patch,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { GetUser } from '../../share/decorators/get-user.decorator';
import { JwtAuthGuard } from '../../share/auth/guard/jwt.guard';
import { UserService } from './user.service';
import {
  CreateOTPDto,
  SetOpen2StepVerify,
  VerifyPhoneNumberDto,
  UpdateUserDto,
  VerifyOTPDto,
  UpdatePhoneNumberDto,
} from './dto';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { SWAGGER_RESPONSE } from './user.constant';

@ApiTags('User')
@ApiUnauthorizedResponse(SWAGGER_RESPONSE.UNAUTHORIZED_EXCEPTION)
@ApiForbiddenResponse(SWAGGER_RESPONSE.FORBIDDEN_EXCEPTION)
@ApiNotFoundResponse(SWAGGER_RESPONSE.NOT_FOUND_EXCEPTION)
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  //#region CRUD User
  @ApiTags('Confirm Account')
  @Get('/confirm-account/:token')
  confirmAccount(@Param('token') token: string) {
    return this.userService.confirmAccount(token);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Get('me')
  getMyInfo(@GetUser('id') id: string) {
    return this.userService.getUserById(id);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Get('info/:userId')
  getUserById(@GetUser('id') myId: string, @Param('userId') userId: string) {
    return this.userService.getOtherUserById(myId, userId);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiConsumes('application/x-www-form-urlencoded')
  @ApiCreatedResponse(SWAGGER_RESPONSE.UPDATE_RESPONSE)
  @Put('update-info')
  updateMyInfo(@GetUser('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.updateMyInfo(id, updateUserDto);
  }

  /**
   * @description Create a otp code and send to this phone number
   * @param id - id of user
   * @param verifyPhoneNumberDto - dto contain phone number
   * @returns otp code
   */
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiConsumes('application/x-www-form-urlencoded')
  @ApiCreatedResponse(SWAGGER_RESPONSE.UPDATE_RESPONSE)
  @Post('verify-phoneNumber')
  verifyPhoneNumberByOTPCode(@GetUser('id') id: string, @Body() verifyPhoneNumberDto: VerifyPhoneNumberDto) {
    return this.userService.verifyPhoneNumberByOTPCode(id, verifyPhoneNumberDto);
  }

  /**
   * @description Verify otp code and update phone number
   * @param id - id of user
   * @param updatePhoneNumberDto - dto contain phone number and otp code
   * @returns user updated
   */
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiConsumes('application/x-www-form-urlencoded')
  @ApiCreatedResponse(SWAGGER_RESPONSE.UPDATE_RESPONSE)
  @Post('update-phoneNumber')
  updatePhoneNumberByOTPCode(@GetUser('id') id: string, @Body() updatePhoneNumberDto: UpdatePhoneNumberDto) {
    return this.userService.updatePhoneNumberByOTPCode(id, updatePhoneNumberDto);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Patch('block/:userId')
  blockUser(@GetUser('id') myId: string, @Param('userId') userId: string) {
    return this.userService.blockUser(myId, userId);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Patch('unblock/:userId')
  unBlockUser(@GetUser('id') myId: string, @Param('userId') userId: string) {
    return this.userService.unBlockUser(myId, userId);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Patch('set-two-step-verify')
  setTwoStepVerify(@GetUser('id') myId: string, @Body() setOpen2StepVerify: SetOpen2StepVerify) {
    return this.userService.setTwoStepVerify(myId, setOpen2StepVerify);
  }
  //#endregion CRUD User

  //#region Friend
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  @Patch('friend/add/:id')
  addFriend(@GetUser('id') myId: string, @Param('id') friendId: string) {
    return this.userService.addFriend(myId, friendId);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  @Patch('friend/accept-req/:reqId')
  acceptFriendRequest(@GetUser('id') myId: string, @Param('reqId') reqId: string) {
    return this.userService.acceptFriendRequest(myId, reqId);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiCreatedResponse(SWAGGER_RESPONSE.UPDATE_RESPONSE)
  @Patch('friend/reject-req/:reqId')
  rejectFriendRequest(@GetUser('id') myId: string, @Param('reqId') reqId: string) {
    return this.userService.rejectFriendRequest(myId, reqId);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Patch('friend/unfriend/:friendId')
  unfriend(@GetUser('id') myId: string, @Param('friendId') friendId: string) {
    return this.userService.unfriend(myId, friendId);
  }
  //#endregion Friend

  //#region User Avatar
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    required: true,
    schema: {
      type: 'object',
      properties: {
        avatar: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_RESPONSE)
  @Post('upload-avatar')
  @UseInterceptors(FileInterceptor('avatar'))
  public uploadAvatar(
    @GetUser('id') userId: string,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: Math.pow(1024, 2) }),
          new FileTypeValidator({ fileType: '(jpg|jpeg|png|JPG|JPEG|PNG)' }),
        ],
      }),
    )
    file: Express.Multer.File,
  ) {
    return this.userService.uploadAvatar(userId, file);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Get('avatars/all')
  public getAllAvatars(@GetUser('id') userId: string) {
    return this.userService.getAllAvatars(userId);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiCreatedResponse(SWAGGER_RESPONSE.UPDATE_RESPONSE)
  @Patch('set-avatar/:avatarId')
  public setIsActiveForOldAvatar(@GetUser('id') userId: string, @Param('avatarId') avatarId: string) {
    return this.userService.setIsActiveForOldAvatar(userId, avatarId);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Delete('delete-avatar/:avatarId')
  public deleteAvatar(@GetUser('id') userId: string, @Param('avatarId') avatarId: string) {
    return this.userService.deleteAvatar(userId, avatarId);
  }
  // #endregion User Avatar

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Get('stat-user/:userId')
  public statisticUserActionWeekly(@Param('userId') userId: string) {
    const now = new Date();
    const startDate = new Date(now.setDate(now.getDate() - 7));
    const endDate = new Date();
    return this.userService.statisticUserActionWeekly(userId, startDate, endDate);
  }

  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Get('stat/new-users')
  public getNewUsers() {
    const now = new Date();
    const startDate = new Date(now.setDate(now.getDate() - 7));
    const endDate = new Date();
    return this.userService.statisticNewUsers(startDate, endDate);
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Get('notifications/all')
  getOwnNotificationsPaginate(
    @GetUser('id') userId: string,
    @Query('page') page: string,
    @Query('limit') limit: string,
  ) {
    return this.userService.getOwnNotificationsPaginate(userId, +page, +limit);
  }

  @Post('gen-otp')
  genOtp(@Body() createOTPDto: CreateOTPDto) {
    return this.userService.createOTP2StepVerification(createOTPDto);
  }

  @Post('verify-otp')
  verifyOtp(@Body() verifyOTPDto: VerifyOTPDto) {
    return this.userService.verifyOTP2StepVerification(verifyOTPDto);
  }
}
