import { User, UserDocument } from 'src/schemas/UserSchema';
import { MongooseRepository } from '../../libs/standard';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

export class UserRepository extends MongooseRepository {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {
    super(userModel);
  }
}
