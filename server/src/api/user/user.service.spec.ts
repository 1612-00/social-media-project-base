import { Test } from '@nestjs/testing';
import { UserRepository } from './user.repository';
import { UserService } from './user.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { BadRequestException, ForbiddenException, NotFoundException } from '@nestjs/common';
import { UserRole } from './role/role.enum';
import { CommentRepository } from '../comment/comment.repository';
import { NotificationService } from '../notification/notification.service';
import { SmsService } from '../../share/services/sms/sms.service';
import { RedisService } from '../../share/services/redis/redis.service';

describe(' Test suite', () => {
  let service: UserService;
  const myId = '62f1dc7e072d445d7cdc6b6d';
  const userId = 'ab237728788vf2327833823';

  const mockUserRepository = {
    findOneByCondition: jest.fn((condition) => ({ ...condition })),
    findOneByConditionAndProtectField: jest.fn((condition) => null),
    hashPassword: jest.spyOn(bcrypt, 'hashSync').mockImplementation((password) => password),
    create: jest.fn((dto) => ({
      ...dto,
      password: bcrypt.hashSync(dto.password, 10),
    })),
    findOneByConditionAndUpdate: jest.fn((condition, data) => ({ ...data })),
    findOneByIdAndUpdate: jest.fn((id, data) => ({ ...data })),
    getUserById: jest.fn((id) => ({ _id: id })),
    save: jest.fn((data) => ({ ...data })),
  };

  const mockJwtService = {
    verify: jest.fn((token) => expect.any(Object)),
  };

  const mockCommentRepository = {};
  const mockNotificationService = {
    create: jest.fn(() => true),
  };

  const mockSmsService = {};
  const mockRedisService = {};

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: UserRepository,
          useValue: mockUserRepository,
        },
        {
          provide: JwtService,
          useValue: mockJwtService,
        },
        {
          provide: CommentRepository,
          useValue: mockCommentRepository,
        },
        {
          provide: NotificationService,
          useValue: mockNotificationService,
        },
        {
          provide: SmsService,
          useValue: mockSmsService,
        },
        {
          provide: RedisService,
          useValue: mockRedisService,
        },
      ],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  describe('get user by id', () => {
    const id = '62f1dc7e072d445d7cdc6b6d';
    it('should return user by id', async () => {
      expect(await service.getUserById(id)).toEqual({ _id: id });
    });

    it('exception: user not exist', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => null);
      expect(service.getUserById(id)).rejects.toThrowError(NotFoundException);
    });
  });

  describe('get user by condition', () => {
    const condition = { email: 'email@gmail.com', phoneNumber: '0987654321' };
    it('should return user by condition', async () => {
      expect(await service.getUserByCondition(condition)).toEqual({
        ...condition,
      });
    });

    it('exception: user not exist', async () => {
      mockUserRepository.findOneByCondition.mockImplementation((condition) => null);
      expect(service.getUserByCondition(condition)).rejects.toThrowError(NotFoundException);
      mockUserRepository.findOneByCondition.mockImplementation((condition) => {
        role: UserRole.Admin;
      });
      expect(service.getUserByCondition(condition)).rejects.toThrowError(NotFoundException);
    });
  });

  describe('get other user by id', () => {
    const myId = '62f1dc7e072d445d7cdc6b6d';
    const userId = '62f1dc7e072d445d7cdc6b6d';

    it('should return user by id', async () => {
      const userGet = {
        _id: userId,
        isVerified: true,
        role: UserRole.User,
        blockList: [],
      };
      mockUserRepository.getUserById.mockImplementation((userId) => userGet);
      expect(await service.getOtherUserById(myId, userId)).toEqual(userGet);
    });

    // it('should throw exception when user is not verified', async () => {
    //   mockUserRepository.getUserById.mockImplementation((id) => ({
    //     _id: id,
    //     isVerified: false,
    //     role: UserRole.User,
    //   }));
    //   expect(service.getOtherUserById(myId, userId)).rejects.toThrowError(NotFoundException);
    // });

    // it('should throw exception when user has admin role', async () => {
    //   mockUserRepository.getUserById.mockImplementation((id) => ({
    //     _id: id,
    //     isVerified: true,
    //     role: UserRole.Admin,
    //   }));
    //   expect(service.getOtherUserById(myId, userId)).rejects.toThrowError(NotFoundException);
    // });

    it('should throw exception when has blocked by this user', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        isVerified: true,
        role: UserRole.User,
        blockList: [{ user: myId }],
      }));
      expect(service.getOtherUserById(myId, userId)).rejects.toThrowError(BadRequestException);
    });
  });

  describe('should be created user', () => {
    const dto = {
      email: 'nguyenducanh1.ldb@gmail.com',
      password: 'ducanh',
      fullName: 'Nguyen Duc Anh',
    };

    it('create user no exception', async () => {
      expect(await service.create(dto)).toEqual({
        ...dto,
        password: bcrypt.hashSync(dto.password, 10),
      });
    });

    it('exception: email already exist', async () => {
      mockUserRepository.findOneByConditionAndProtectField.mockResolvedValue((condition) =>
        Promise.resolve({ ...condition }),
      );
      try {
        await service.create(dto);
      } catch (error) {
        expect(error).toBeInstanceOf(BadRequestException);
      }
    });
  });

  describe('should be confirm user account', () => {
    const condition = { email: 'email@gmail.com' };
    const data = { isVerified: true };

    it('confirm account no exception', async () => {
      mockUserRepository.findOneByConditionAndProtectField.mockResolvedValue((condition) =>
        Promise.resolve({ ...condition }),
      );
      expect(await service.confirmAccount('token')).toEqual('Account verified successfully');
    });

    it('exception: token invalid', async () => {
      mockJwtService.verify.mockRejectedValue(new Error());
      try {
        await service.confirmAccount('token');
      } catch (error) {
        expect(error).toBeInstanceOf(Error);
      }
    });

    it('confirm failed', async () => {
      mockUserRepository.findOneByConditionAndUpdate.mockImplementation((condition, data) => ({
        ...data,
        isVerified: false,
      }));
      expect(service.confirmAccount('token')).rejects.toThrowError(Error);
    });
  });

  describe('should be reset password', () => {
    const dto = { token: 'token', newPassword: 'password' };

    it('reset password no exception', async () => {
      mockJwtService.verify.mockResolvedValue((token) => expect.any(Object));
      expect(await service.resetPassword(dto)).toEqual(expect.objectContaining({ password: dto.newPassword }));
    });

    it('exception: token invalid', async () => {
      mockJwtService.verify.mockRejectedValue(new Error());
      try {
        await service.resetPassword(dto);
      } catch (error) {
        expect(error).toBeInstanceOf(Error);
      }
    });
  });

  describe('update by id', () => {
    const dto = {
      address: 'Ha Noi',
      phoneNumber: '0123456789',
    };
    it('should updated no exception', async () => {
      const userUpdated = await service.updateMyInfo('myId', dto);
      expect(userUpdated).toEqual({ ...dto });
    });
  });

  describe('add friend', () => {
    const friendId = '62f20fc3cceaf132e5f17a6e';
    const myId = '62f1dc7e072d445d7cdc6b6d';
    it('should sent friend request no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        friends: [],
        friendReqs: [],
        blockList: [],
      }));
      expect(await service.addFriend(myId, friendId)).toEqual({
        friendReqs: [{ user: myId, date: expect.any(Date) }],
      });
    });
    it('should sent request to myself', async () => {
      expect(service.addFriend(myId, myId)).rejects.toThrowError(BadRequestException);
    });
    it('should throw error with bad ids', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => null);
      expect(service.addFriend(myId, friendId)).rejects.toThrowError(NotFoundException);
    });
    it('should throw error when already friend relationships', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => ({
        _id: myId,
        friends: [{ user: friendId, date: expect.any(Date) }],
        friendReqs: [],
        blockList: [],
      }));
      expect(service.addFriend(myId, friendId)).rejects.toThrowError(BadRequestException);
    });
    it('should throw error when this friend was blocked', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => ({
        _id: myId,
        friends: [],
        friendReqs: [],
        blockList: [{ user: friendId, date: expect.any(Date) }],
      }));
      expect(service.addFriend(myId, friendId)).rejects.toThrowError(BadRequestException);
    });
    it('should throw error when me was blocked by this friend', async () => {
      mockUserRepository.getUserById.mockImplementation((friendId) => ({
        _id: friendId,
        friends: [],
        friendReqs: [],
        blockList: [{ user: myId, date: expect.any(Date) }],
      }));
      expect(service.addFriend(myId, friendId)).rejects.toThrowError(NotFoundException);
    });
    it('should throw error when already sent friend request', async () => {
      mockUserRepository.getUserById.mockImplementation((friendId) => ({
        _id: friendId,
        friends: [],
        friendReqs: [{ user: myId, date: expect.any(Date) }],
        blockList: [],
      }));
      expect(service.addFriend(myId, friendId)).rejects.toThrowError(BadRequestException);
    });
    it('should throw error when this friend sent request for me', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => ({
        _id: myId,
        friends: [],
        friendReqs: [{ user: friendId, date: expect.any(Date) }],
        blockList: [],
      }));
      expect(service.addFriend(myId, friendId)).rejects.toThrowError(BadRequestException);
    });
  });

  describe('accept friend requets', () => {
    const reqId = '62f20fc3cceaf132e5f17a6e';
    const myId = '62f1dc7e072d445d7cdc6b6d';
    const friendId = 'ab237728788vf2327833823';
    it('should accept friend requests no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => ({
        _id: myId,
        friends: [],
        friendReqs: [{ _id: reqId, user: friendId }],
        blockList: [],
      }));
      expect(await service.acceptFriendRequest(myId, reqId)).toEqual({
        userUpdated: {
          friendReqs: [],
        },
        myListFriend: [
          {
            date: expect.any(Date),
            user: friendId,
          },
        ],
        yourListFriend: [
          {
            date: expect.any(Date),
            user: myId,
          },
        ],
      });
    });
    it('should throw an error with bad user id', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => null);
      expect(service.acceptFriendRequest(myId, reqId)).rejects.toThrowError(NotFoundException);
    });
    it('should throw an error with bad request id', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => ({
        _id: myId,
        friends: [],
        friendReqs: [],
        blockList: [],
      }));
      expect(service.acceptFriendRequest(myId, reqId)).rejects.toThrowError(NotFoundException);
    });
  });

  describe('reject friend requets', () => {
    const reqId = '62f20fc3cceaf132e5f17a6e';
    const myId = '62f1dc7e072d445d7cdc6b6d';
    const friendId = 'ab237728788vf2327833823';
    it('should accept friend requests no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => ({
        _id: myId,
        friends: [],
        friendReqs: [{ _id: reqId, user: friendId }],
        blockList: [],
      }));
      expect(await service.rejectFriendRequest(myId, reqId)).toEqual({
        userUpdated: {
          friendReqs: [],
        },
      });
    });
    it('should throw an error with bad user id', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => null);
      expect(service.rejectFriendRequest(myId, reqId)).rejects.toThrowError(NotFoundException);
    });
    it('should throw an error with bad request id', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => ({
        _id: myId,
        friends: [],
        friendReqs: [],
        blockList: [],
      }));
      expect(service.rejectFriendRequest(myId, reqId)).rejects.toThrowError(NotFoundException);
    });
  });

  describe('unfriend', () => {
    const myId = '62f1dc7e072d445d7cdc6b6d';
    const friendId = 'ab237728788vf2327833823';

    it('should be unfriend no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({ _id: id, friends: [{ user: friendId }] }));
      expect(await service.unfriend(myId, friendId)).toEqual({ success: true });
    });

    it('should sent request to myself', async () => {
      expect(service.addFriend(myId, myId)).rejects.toThrowError(BadRequestException);
    });

    it('should throw error with bad id', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => null);
      const myInfo = service.unfriend(myId, friendId);
      expect(myInfo).rejects.toThrowError(NotFoundException);

      mockUserRepository.getUserById.mockImplementation((friendId) => null);
      const friendInfo = service.unfriend(myId, friendId);
      expect(friendInfo).rejects.toThrowError(NotFoundException);
    });

    it('should throw error when they not in friend relation', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => ({
        _id: myId,
        friends: [],
        friendReqs: [],
        blockList: [],
      }));
      expect(service.unfriend(myId, friendId)).rejects.toThrowError(BadRequestException);

      mockUserRepository.getUserById.mockImplementation((friendId) => ({
        _id: friendId,
        friends: [],
        friendReqs: [],
        blockList: [],
      }));
      expect(service.unfriend(myId, friendId)).rejects.toThrowError(BadRequestException);
    });
  });

  describe('block user', () => {
    it('should block user no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        role: expect.any(!UserRole.Admin),
        blockList: [],
        friends: [],
      }));
      mockUserRepository.findOneByIdAndUpdate.mockImplementation((myId) => ({
        _id: myId,
        blockList: [{ user: userId }],
      }));
      expect(await service.blockUser(myId, userId)).toEqual({
        _id: myId,
        blockList: [{ user: userId }],
      });
    });

    it('should throw exception when user not found', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => null);
      expect(service.blockUser(myId, userId)).rejects.toThrowError(NotFoundException);
    });

    it('should throw exception when this user has admin role', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        role: UserRole.Admin,
      }));
      expect(service.blockUser(myId, userId)).rejects.toThrowError(ForbiddenException);
    });

    it('should throw exception when has blocked this user', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        role: expect.any(!UserRole.Admin),
        blockList: [{ user: userId }],
      }));
      expect(service.blockUser(myId, userId)).rejects.toThrowError(NotFoundException);
    });

    it('should throw exception when has blocked by this user', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        role: expect.any(!UserRole.Admin),
        blockList: [{ user: myId }],
      }));
      expect(service.blockUser(myId, userId)).rejects.toThrowError(NotFoundException);
    });
  });

  describe('unblock user', () => {
    it('should unblock user no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        role: expect.any(!UserRole.Admin),
        blockList: [{ user: userId }],
      }));
      mockUserRepository.findOneByIdAndUpdate.mockImplementation((myId) => ({
        _id: myId,
        blockList: [],
      }));
      expect(await service.unBlockUser(myId, userId)).toEqual({
        _id: myId,
        blockList: [],
      });
    });

    it('should throw exception when user not found', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => null);
      expect(service.unBlockUser(myId, userId)).rejects.toThrowError(NotFoundException);
    });

    it('should throw exception when has unblocked this user', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        blockList: [],
      }));
      expect(service.unBlockUser(myId, userId)).rejects.toThrowError(NotFoundException);
    });

    it('should throw exception when has unblocked by this user', async () => {
      mockUserRepository.getUserById.mockImplementation((id) => ({
        _id: id,
        role: expect.any(!UserRole.Admin),
        blockList: [{ user: myId }],
      }));
      expect(service.unBlockUser(myId, userId)).rejects.toThrowError(NotFoundException);
    });
  });

  describe('get all avatar', () => {
    it('should return all avatar', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => ({
        _id: myId,
        avatars: [{ url: expect.any(String) }],
      }));
      expect(await service.getAllAvatars(myId)).toEqual([{ url: expect.any(String) }]);
    });
  });

  describe('set isActive for old avatar', () => {
    const avatarId = '6301fd44c1a2a2cc3f17e95f';
    it('should set isActive for old avatar no exception', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => ({
        _id: myId,
        avatars: [{ _id: avatarId, isActive: false }],
      }));
      expect(await service.setIsActiveForOldAvatar(userId, avatarId)).toEqual({
        _id: userId,
        avatars: [{ _id: avatarId, isActive: true }],
      });
    });

    it('should throw exception when this is current avatar', async () => {
      mockUserRepository.getUserById.mockImplementation((myId) => ({
        _id: myId,
        avatars: [{ _id: avatarId, isActive: true }],
      }));
      expect(service.setIsActiveForOldAvatar(userId, avatarId)).rejects.toThrowError(BadRequestException);
    });
  });
});
