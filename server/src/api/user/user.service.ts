import * as bcrypt from 'bcrypt';
import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { UserRepository } from './user.repository';
import {
  CreateOTPDto,
  CreateUserDto,
  SetOpen2StepVerify,
  VerifyPhoneNumberDto,
  UpdateUserDto,
  VerifyOTPDto,
  UpdatePhoneNumberDto,
} from './dto';
import { AppObject, ERROR } from '../../share/common';
import { JwtService } from '@nestjs/jwt';
import { Avatar, Relation, User } from '../../schemas/UserSchema';
import { ResetPasswordDto } from '../../share/auth/dto';
import { UserRole } from './role/role.enum';
import { getDownloadURL, listAll, ref, uploadBytes, deleteObject } from 'firebase/storage';
import { firebaseImageName, avatarStorage } from '../../config/firebase/firebase-storage';
import { storage } from '../../config/firebase/config';
import { Parser } from 'json2csv';
import { CommentRepository } from '../comment/comment.repository';
import { NotificationService } from '../notification/notification.service';
import { CreateNotificationDto } from '../notification/dto/createNotificationDto';
import * as otpGen from 'otp-generator';
import { SmsService } from '../../share/services/sms/sms.service';
import { RedisService } from '../../share/services/redis/redis.service';
import { SetRedisDto } from '../../share/services/redis/dto/set-redit.dto';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly jwtService: JwtService,
    private readonly commentRepository: CommentRepository,
    private readonly notificationService: NotificationService,
    private readonly smsService: SmsService,
    private readonly redisService: RedisService,
  ) {}

  async getAll() {
    try {
      return await this.userRepository.find();
    } catch (error) {
      throw error;
    }
  }

  async getUserById(id: string) {
    try {
      const fieldsSelect = 'email fullName address phoneNumber job friends';
      const userFound = await this.userRepository.findByIdAndSelectFields(id, fieldsSelect);
      if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);
      return userFound;
    } catch (error) {
      throw error;
    }
  }

  async saveRefreshToken(userId: string, refreshToken: string) {
    try {
      const userFound = await this.userRepository.detailById(userId);
      if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);
      userFound.refreshToken = refreshToken;
      await this.userRepository.save(userFound);
      return refreshToken;
    } catch (error) {
      throw error;
    }
  }

  async getUserByCondition(condition: object) {
    try {
      const userFound = await this.userRepository.detailByCondition(condition);
      if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);
      return userFound;
    } catch (error) {
      throw error;
    }
  }

  async getOtherUserById(myId: string, userId: string) {
    try {
      const fieldsSelect = 'email fullName address phoneNumber job blockList';
      const user = await this.userRepository.findByIdAndSelectFields(userId, fieldsSelect);
      // if (!user.isVerified || user.role.includes(UserRole.Admin)) {
      //   throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);
      // }

      const hasBlocked = user.blockList.find((blockedUser) => blockedUser.user.toString().includes(myId));
      if (hasBlocked) throw new BadRequestException(ERROR.HAD_BLOCKED_BY_THIS_USER.MESSAGE);

      return user;
    } catch (error) {
      throw error;
    }
  }

  async create(createUserDto: CreateUserDto) {
    try {
      const userFound = await this.userRepository.detailByCondition({
        email: createUserDto.email,
      });
      if (userFound) throw new BadRequestException(ERROR.EMAIL_ALREADY_EXIST.MESSAGE);

      const passwordHash = bcrypt.hashSync(createUserDto.password, 10);
      const userCreated = await this.userRepository.create({
        ...createUserDto,
        password: passwordHash,
      });

      return userCreated;
    } catch (error) {
      throw error;
    }
  }

  async confirmAccount(token: string) {
    try {
      // Verify token
      const user = await this.jwtService.verify(token);

      // Token valid
      const userFound: User = await this.userRepository.detailByCondition({
        email: user.email,
        isVerified: false,
      });
      if (!userFound) throw new NotFoundException(ERROR.CONFIRM_ACCOUNT_FAILURE.MESSAGE);

      // Change state to true
      const userUpdated = await this.userRepository.findOneByConditionAndUpdate(
        { email: user.email },
        { isVerified: true },
      );
      if (userUpdated.isVerified) {
        return 'Account verified successfully';
      } else {
        throw new NotFoundException(ERROR.CONFIRM_ACCOUNT_FAILURE.MESSAGE);
      }
    } catch (error) {
      throw error;
    }
  }

  async resetPassword(resetPasswordDto: ResetPasswordDto) {
    try {
      // Verify token
      const user = await this.jwtService.verify(resetPasswordDto.token);

      const hash = bcrypt.hashSync(resetPasswordDto.newPassword, 10);

      const userUpdated = await this.userRepository.findOneByConditionAndUpdate(
        { email: user.email },
        { password: hash },
      );

      return userUpdated;
    } catch (error) {
      throw error;
    }
  }

  async updateMyInfo(id: string, updateUserDto: UpdateUserDto) {
    try {
      await this.userRepository.findOneByIdAndUpdate(id, updateUserDto);
      return await this.getUserById(id);
    } catch (error) {
      throw error;
    }
  }

  async verifyPhoneNumberByOTPCode(id: string, verifyPhoneNumberDto: VerifyPhoneNumberDto) {
    try {
      const userFound = await this.userRepository.detailById(id);
      if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      // Create new otp code and send to phone number
      const createOTPDto: CreateOTPDto = {
        email: userFound.email,
        phoneNumber: verifyPhoneNumberDto.phoneNumber,
      };
      await this.createOTP2StepVerification(createOTPDto);
      return { status: 200, message: 'Please check your phone and confirm OTP code to login' };
    } catch (error) {
      throw error;
    }
  }

  async updatePhoneNumberByOTPCode(id: string, updatePhoneNumberDto: UpdatePhoneNumberDto) {
    try {
      const userFound = await this.userRepository.detailById(id);
      if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      // Verify otp code and update phone number
      const verifyOTPDto: VerifyOTPDto = {
        email: userFound.email,
        otp: updatePhoneNumberDto.otp,
      };
      const verifyStatus = await this.verifyOTP2StepVerification(verifyOTPDto);

      if (!verifyStatus) throw new BadRequestException(ERROR.OTP_INVALID.MESSAGE);

      userFound.phoneNumber = updatePhoneNumberDto.phoneNumber;

      const userUpdated = await this.userRepository.save(userFound);

      return userUpdated.phoneNumber;
    } catch (error) {
      throw error;
    }
  }

  async addFriend(myId: string, friendId: string) {
    try {
      if (myId === friendId) throw new BadRequestException(ERROR.DONT_INTERACTIVE_MYSELF.MESSAGE);

      // Get info about the friend and myself
      const myInfo = await this.userRepository.detailById(myId);
      const friendInfo = await this.userRepository.detailById(friendId);

      if (!myInfo || !friendInfo) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      // Check is already friend
      myInfo.friends.forEach((friend: Relation) => {
        if (friend.user.toString().includes(friendInfo._id.toString()))
          throw new BadRequestException(ERROR.ALREADY_FRIEND.MESSAGE);
      });

      // Check this friend was blocked
      myInfo.blockList.forEach((userBlocked: Relation) => {
        if (userBlocked.user.toString().includes(friendInfo._id.toString()))
          throw new BadRequestException(ERROR.USER_HAS_BLOCKED.MESSAGE);
      });

      // Check me was blocked by this friend
      friendInfo.blockList.forEach((userBlocked: Relation) => {
        if (userBlocked.user.toString().includes(myInfo._id.toString()))
          throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);
      });

      // Check already sent friend request
      friendInfo.friendReqs.forEach((friendReq: Relation) => {
        if (friendReq.user.toString().includes(myInfo._id.toString()))
          throw new BadRequestException(ERROR.ALREADY_SENT_REQ.MESSAGE);
      });

      // Check this friend sent friend request for me
      myInfo.friendReqs.forEach((friendReq: Relation) => {
        if (friendReq.user.toString().includes(friendInfo._id.toString()))
          throw new BadRequestException(ERROR.ALREADY_HAVE_REQ.MESSAGE);
      });

      // Sent friend request
      const friendUpdated = await this.userRepository.findOneByIdAndUpdate(friendId, {
        friendReqs: [...friendInfo.friendReqs, { user: myInfo._id, date: new Date(Date.now()) }],
      });

      // Create notification add friend
      const newAddFriendNotification: CreateNotificationDto = {
        senderId: myId,
        senderType: AppObject.NOTIFICATION_OBJECT.USER,
        receiverId: friendId,
        receiverType: AppObject.NOTIFICATION_OBJECT.USER,
        objectId: myId,
        objectType: AppObject.NOTIFICATION_OBJECT.USER,
        type: AppObject.NOTIFICATION_TYPE.ADD_FRIEND,
      };
      await this.notificationService.create(newAddFriendNotification);

      return { listFriendReq: friendUpdated.friendReq };
    } catch (error) {
      throw error;
    }
  }

  async getOwnNotificationsPaginate(userId: string, page: number, limit: number) {
    try {
      const userFound = await this.userRepository.detailById(userId);
      if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      return await this.notificationService.findOwnNotification(userId, page, limit);
    } catch (error) {
      throw error;
    }
  }

  async acceptFriendRequest(myId: string, reqId: string) {
    try {
      // Get info about the friend and myself
      const myInfo = await this.userRepository.detailById(myId);
      if (!myInfo) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      const requestFound = myInfo.friendReqs.find((request) => request._id.toString().includes(reqId));

      if (!requestFound) throw new NotFoundException(ERROR.FRIEND_REQ_NOT_FOUND.MESSAGE);

      const friendInfo = await this.userRepository.detailById(requestFound.user.toString());

      // Remove friend request
      const newListFriend = myInfo.friendReqs.filter((req) => !req._id.toString().includes(reqId));
      await this.userRepository.findOneByIdAndUpdate(myId, { friendReqs: newListFriend });

      // Update list friend of me and this friend
      const myInfoUpdate = await this.userRepository.findOneByIdAndUpdate(myId, {
        friends: [...myInfo.friends, { user: friendInfo._id, date: new Date(Date.now()) }],
      });
      const friendInfoUpdate = await this.userRepository.findOneByIdAndUpdate(friendInfo._id.toString(), {
        friends: [...friendInfo.friends, { user: myInfo._id, date: new Date(Date.now()) }],
      });

      // Create notification add friend
      const newAcceptFriendReqNotification: CreateNotificationDto = {
        senderId: myId,
        senderType: AppObject.NOTIFICATION_OBJECT.USER,
        receiverId: requestFound.user.toString(),
        receiverType: AppObject.NOTIFICATION_OBJECT.USER,
        objectId: myId,
        objectType: AppObject.NOTIFICATION_OBJECT.USER,
        type: AppObject.NOTIFICATION_TYPE.ACCEPT_FRIEND_REQ,
      };
      await this.notificationService.create(newAcceptFriendReqNotification);

      return {
        myListFriend: myInfoUpdate.friends,
        yourListFriend: friendInfoUpdate.friends,
      };
    } catch (error) {
      throw error;
    }
  }

  async rejectFriendRequest(myId: string, reqId: string) {
    try {
      // Get info about the friend and myself
      const myInfo = await this.userRepository.detailById(myId);
      if (!myInfo) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      const requestFound = myInfo.friendReqs.find((request) => request._id.toString().includes(reqId));

      if (!requestFound) throw new NotFoundException(ERROR.FRIEND_REQ_NOT_FOUND.MESSAGE);

      // Remove friend request
      const newListFriend = myInfo.friendReqs.filter((req) => !req._id.toString().includes(reqId));
      await this.userRepository.findOneByIdAndUpdate(myId, { friendReqs: newListFriend });

      return {
        status: 204,
        message: 'Rejected friend request',
      };
    } catch (error) {
      throw error;
    }
  }

  async removeFriend(myInfo, friendInfo) {
    try {
      // Remove friend
      const newMyFriendList = myInfo.friends.filter(
        (friend) => !friend.user.toString().includes(friendInfo._id.toString()),
      );
      await this.userRepository.findOneByIdAndUpdate(myInfo._id.toString(), {
        friends: newMyFriendList,
      });

      const newYourFriendList = friendInfo.friends.filter(
        (friend) => !friend.user.toString().includes(myInfo._id.toString()),
      );
      await this.userRepository.findOneByIdAndUpdate(friendInfo._id.toString(), {
        friends: newYourFriendList,
      });
      return {
        status: 204,
        message: 'Remove friend successfully',
      };
    } catch (error) {
      throw error;
    }
  }

  async unfriend(myId: string, friendId: string) {
    try {
      if (myId === friendId) throw new BadRequestException(ERROR.DONT_INTERACTIVE_MYSELF.MESSAGE);

      // Get info about the friend and myself
      const myInfo = await this.userRepository.detailById(myId);
      const friendInfo = await this.userRepository.detailById(friendId);

      if (!myInfo || !friendInfo) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      // Check has friend
      const isMyIdInList = myInfo.friends.find((friend) => friend.user.toString().includes(friendId));
      if (!isMyIdInList) throw new BadRequestException(ERROR.NOT_FRIEND.MESSAGE);

      this.removeFriend(myInfo, friendInfo);

      return {
        status: 204,
        message: 'Unfriend successfully',
      };
    } catch (error) {
      throw error;
    }
  }

  async blockUser(myId: string, userId: string) {
    try {
      if (myId === userId) throw new BadRequestException(ERROR.DONT_INTERACTIVE_MYSELF.MESSAGE);

      // Get info about the friend and myself
      const myInfo = await this.userRepository.detailById(myId);
      const userInfo = await this.userRepository.detailById(userId);

      if (!myInfo || !userInfo) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      if (userInfo.role === UserRole.Admin) throw new ForbiddenException(ERROR.DONT_PERMISSION_ACCESS.MESSAGE);

      const hasBlockedByUser = userInfo.blockList.find((userBlocked) => userBlocked.user.toString().includes(myId));
      if (hasBlockedByUser) throw new NotFoundException(ERROR.HAD_BLOCKED_BY_THIS_USER.MESSAGE);

      const userBlocked = myInfo.blockList.find((userBlocked) => userBlocked.user.toString().includes(userId));
      if (userBlocked) throw new NotFoundException(ERROR.USER_HAS_BLOCKED.MESSAGE);

      const hasFriend = myInfo.friends.find((friend) => friend.user.toString().includes(userId));
      if (hasFriend) {
        this.removeFriend(myInfo, userInfo);
      }

      const myInfoUpdated = await this.userRepository.findOneByIdAndUpdate(myId, {
        blockList: [...myInfo.blockList, { user: userInfo._id, date: new Date(Date.now()) }],
      });

      return { blockList: myInfoUpdated.blockList };
    } catch (error) {
      throw error;
    }
  }

  async unBlockUser(myId: string, userId: string) {
    try {
      if (myId === userId) throw new BadRequestException(ERROR.DONT_INTERACTIVE_MYSELF.MESSAGE);

      // Get info about the friend and myself
      const myInfo = await this.userRepository.detailById(myId);
      const userInfo = await this.userRepository.detailById(userId);

      if (!myInfo || !userInfo) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      // Check has blocked by this user
      const hasBlockedByUser = userInfo.blockList.find((userBlocked) => userBlocked.user.toString().includes(myId));
      if (hasBlockedByUser) throw new NotFoundException(ERROR.HAD_BLOCKED_BY_THIS_USER.MESSAGE);

      // Check this user was blocked
      const userBlocked = myInfo.blockList.find((userBlocked) => userBlocked.user.toString().includes(userId));
      if (!userBlocked) throw new NotFoundException(ERROR.USER_HAS_NOT_BLOCKED.MESSAGE);

      // Remove this user to my blocklist
      const newBlockList = myInfo.blockList.filter((userBlocked) => !userBlocked.user.toString().includes(userId));
      const myInfoUpdated = await this.userRepository.findOneByIdAndUpdate(myId, {
        blockList: newBlockList,
      });

      return { blockList: myInfoUpdated.blockList };
    } catch (error) {
      throw error;
    }
  }

  async uploadAvatar(userId: string, file: Express.Multer.File): Promise<User> {
    try {
      const folderSave = avatarStorage(userId);
      const imageName = firebaseImageName(userId, 'avatar');
      const linkImage = folderSave + imageName;

      // Upload image to firebase server
      const saveAvatarRef = ref(storage, linkImage);
      const uploaded = await uploadBytes(saveAvatarRef, file.buffer, {
        contentType: 'image/jpeg',
      });
      if (!uploaded) {
        throw new BadRequestException(ERROR.UPLOAD_AVATAR_FAILED.MESSAGE);
      }

      // Retrieve image url from firebase server and save to db
      const getAvatarRef = ref(storage, folderSave);
      const list = await listAll(getAvatarRef);
      let avatarUrl = '';
      for (let i = 0; i < list.items.length; i++) {
        if (list.items[i].fullPath === linkImage) {
          avatarUrl = await getDownloadURL(list.items[i]);
        }
      }
      if (avatarUrl === '') {
        throw new BadRequestException(ERROR.RETRIEVE_AVATAR_FAILED.MESSAGE);
      }

      // Set isActive current avatar false
      const userFound = await this.userRepository.detailById(userId);
      const currAvatar = userFound.avatars.find((avatar) => avatar.isActive);
      if (currAvatar) {
        currAvatar.isActive = false;
        userFound.save();
      }

      // Add new avatar
      const userUpdated = await this.userRepository.findOneByIdAndUpdate(userId, {
        avatars: [...userFound.avatars, { url: avatarUrl, isActive: true, date: new Date() }],
      });

      return userUpdated;
    } catch (error) {
      throw error;
    }
  }

  async getAllAvatars(userId: string): Promise<Avatar[]> {
    try {
      const userFound = await this.userRepository.detailById(userId);
      return userFound.avatars;
    } catch (error) {
      throw error;
    }
  }

  async setIsActiveForOldAvatar(userId: string, avatarId: string): Promise<User> {
    try {
      const userFound = await this.userRepository.detailById(userId);

      const avatarFound = userFound.avatars.find((avatar) => avatar._id.toString() === avatarId);

      if (avatarFound.isActive) throw new BadRequestException(ERROR.IS_CURRENT_AVATAR.MESSAGE);

      // Set isActive current avatar false
      const currAvatar = userFound.avatars.find((avatar) => avatar.isActive);

      if (currAvatar) {
        currAvatar.isActive = false;
      }

      avatarFound.isActive = true;
      const userUpdated = await this.userRepository.save(userFound);

      return userUpdated.avatars;
    } catch (error) {
      throw error;
    }
  }

  async deleteAvatar(userId: string, avatarId: string) {
    try {
      const userFound = await this.userRepository.detailById(userId);

      const avatarFound = userFound.avatars.find((avatar) => avatar._id.toString() === avatarId);
      if (!avatarFound) throw new NotFoundException(ERROR.AVATAR_NOT_FOUND.MESSAGE);

      const avatarRef = ref(storage, avatarFound.url);
      await deleteObject(avatarRef)
        .then(() => {
          const newListAvatar = userFound.avatars.filter((avatar) => avatar._id.toString() !== avatarId);
          userFound.avatars = newListAvatar;
          userFound.save();
          return userFound;
        })
        .catch((err) => {
          throw new Error(err);
        });
      return userFound.avatars;
    } catch (error) {
      throw error;
    }
  }

  async statisticUserActionWeekly(userId: string, startDate: Date, endDate: Date) {
    try {
      const userFound = await this.userRepository.detailById(userId);
      if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);
      // User related actions - new friends, blocked users count
      let friendsCount = 0,
        blockCount = 0;
      userFound.friends.forEach((friend) => {
        if (friend.date >= startDate && friend.date <= endDate) friendsCount++;
      });
      userFound.blockList.forEach((userBlocked) => {
        if (userBlocked.date >= startDate && userBlocked.date <= endDate) blockCount++;
      });

      // Post related actions - created post, shared post count, number of likes, loves post
      let createdPostCount = 0,
        sharedPostCount = 0;
      userFound.posts.forEach((createdPost) => {
        if (createdPost.date >= startDate && createdPost.date <= endDate) createdPostCount++;
      });
      userFound.postsShared.forEach((postShared) => {
        if (postShared.date >= startDate && postShared.date <= endDate) sharedPostCount++;
      });

      // Group related actions - joined group count
      let joinedGroupCount = 0;
      userFound.groupsJoined.forEach((groupJoined) => {
        if (groupJoined.date >= startDate && groupJoined.date <= endDate) joinedGroupCount++;
      });

      // Comment post weekly
      const comments = await this.commentRepository.findDistinct({ author: userId.toString() }, 'post');

      const data = [
        {
          Type: 'New friends',
          Number: friendsCount,
        },
        {
          Type: 'Blocked user',
          Number: blockCount,
        },
        {
          Type: 'Created post',
          Number: createdPostCount,
        },
        {
          Type: 'Shared post',
          Number: sharedPostCount,
        },
        {
          Type: 'Group joined',
          Number: joinedGroupCount,
        },
        {
          Type: 'Number of posts commented',
          Number: comments.length,
        },
      ];

      // const json2csv = new Parser();
      // const csv = json2csv.parse(data);
      // return csv;
      return data;
    } catch (error) {
      throw error;
    }
  }

  async statisticNewUsers(startDate: Date, endDate: Date) {
    try {
      const newUsers = await this.userRepository.findByConditionDistinct({
        createdAt: { $gte: startDate, $lte: endDate },
      });
      return newUsers;
    } catch (error) {
      throw error;
    }
  }

  async setTwoStepVerify(userId: string, setOpen2StepVerify: SetOpen2StepVerify) {
    try {
      const userFound = await this.userRepository.detailById(userId);
      if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

      if (userFound.phoneNumber === '') throw new ForbiddenException(ERROR.PHONE_NUMBER_NOT_FOUND.MESSAGE);

      userFound.twoStepVerify = setOpen2StepVerify.isOpen;
      const userUpdated = await this.userRepository.save(userFound);
      return { twoStepVerify: userUpdated.twoStepVerify };
    } catch (error) {
      throw error;
    }
  }

  async createOTP2StepVerification(createOTPDto: CreateOTPDto) {
    try {
      const newOtp = otpGen.generate(6, { lowerCaseAlphabets: false, upperCaseAlphabets: false, specialChars: false });
      await this.smsService.sendOTP2StepVerification(createOTPDto.phoneNumber, newOtp);
      const setRedisDto: SetRedisDto = {
        key: 'otp - ' + createOTPDto.email,
        value: newOtp,
        ttl: 600,
      };
      await this.redisService.setKey(setRedisDto);
      return { status: 200, message: 'Please check your phone and confirm OTP code to login', newOtp };
    } catch (error) {
      throw error;
    }
  }

  async verifyOTP2StepVerification(verifyOTPDto: VerifyOTPDto) {
    try {
      const valueGetCache = await this.redisService.getKey('otp - ' + verifyOTPDto.email);
      if (!valueGetCache) throw new NotFoundException('otp not found');
      if (verifyOTPDto.otp === valueGetCache) return true;
      return false;
    } catch (error) {
      throw error;
    }
  }
}
