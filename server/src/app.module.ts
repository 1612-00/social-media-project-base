import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './api/user/user.module';
import { AuthModule } from './share/auth/auth.module';
import { MailModule } from './config/mail/mail.module';
import { GoogleAuthModule } from './config/google-auth/google-auth.module';
import { ConfigModule } from '@nestjs/config';
import { config } from 'dotenv';
import { AdminModule } from './api/admin/admin.module';
import { JobQueueModule } from './config/job-queue/job-queue.module';
import { PostModule } from './api/post/post.module';
import { CommentModule } from './api/comment/comment.module';
import { GroupModule } from './api/group/group.module';
import { ScheduleModule } from '@nestjs/schedule';
import { NotificationCronjob } from './share/services/cronjob/notification.cronjob';
import { RefreshTokenModule } from './share/refresh-token/refresh-token.module';
import { NotificationModule } from './api/notification/notification.module';
import { GGSheetModule } from './share/services/gg-sheet/gg-sheet.module';
import { SmsModule } from './share/services/sms/sms.module';
import { RedisModule } from './share/services/redis/redis.module';
import { QrCodeModule } from './share/services/qr-code/qr-code.module';
config();

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({ isGlobal: true }),
    MongooseModule.forRoot(process.env.MONGODB_URL),
    UserModule,
    AuthModule,
    MailModule,
    GoogleAuthModule,
    AdminModule,
    JobQueueModule,
    PostModule,
    CommentModule,
    GroupModule,
    RefreshTokenModule,
    NotificationModule,
    GGSheetModule,
    SmsModule,
    RedisModule,
    QrCodeModule,
  ],
  providers: [NotificationCronjob],
})
export class AppModule {}
