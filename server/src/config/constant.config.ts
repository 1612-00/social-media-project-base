import { config } from 'dotenv';
config();

export const JWT_CONFIG = {
  secret: process.env.TOKEN_SECRET,
  expiresIn: process.env.TOKEN_EXPIRED_IN,
  refreshSecret: process.env.REFRESH_TOKEN_SECRET,
  refreshExpiresIn: process.env.REFRESH_TOKEN_EXPIRED_IN,
};

export const FIREBASE_CONFIG = {
  apiKey: process.env.API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  projectId: process.env.PROJECT_ID,
  storageBucket: process.env.STORAGE_BUCKET,
  messagingSenderId: process.env.MESSAGING_SENDER_ID,
  appId: process.env.API_ID,
  measurementId: process.env.MEASUREMENT_ID,
};

export const GS_CONFIG = {
  private_key: process.env.PRIVATE_KEY.replace(/\\n/gm, '\n'),
  client_email: process.env.CLIENT_EMAIL,
  sheep_id: process.env.SHEEP_ID,
};
