export const avatarStorage = (userId) => {
  return `avatar-storage/${userId}/`;
};

export const firebaseImageName = (userId, folderName) => {
  return `${userId}_${Date.now()}_${folderName}.jpeg`;
};

export const postStorage = (postId) => {
  return `post-storage/${postId}/`;
};
