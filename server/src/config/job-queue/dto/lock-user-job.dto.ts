import { IsNotEmpty, IsString } from 'class-validator';

export class LockUserJobDto {
  @IsString()
  @IsNotEmpty()
  userId: string;
}
