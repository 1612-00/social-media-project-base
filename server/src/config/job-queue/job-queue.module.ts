import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { UserModule } from 'src/api/user/user.module';
import { JobQueueController } from './job-queue.controller';
import { LockAccountConsumer } from './lock-acc.consumer';
import { LockAccountService } from './lock-acc.service';

@Module({
  imports: [
    UserModule,
    BullModule.forRoot({
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
    BullModule.registerQueue({
      name: 'lock-user-queue',
    }),
  ],
  controllers: [JobQueueController],
  providers: [LockAccountService, LockAccountConsumer],
  exports: [LockAccountService],
})
export class JobQueueModule {}
