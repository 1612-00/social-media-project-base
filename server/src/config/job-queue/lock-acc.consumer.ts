import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';
import { UserRepository } from 'src/api/user/user.repository';
import { LockUserJobDto } from './dto/lock-user-job.dto';

@Processor('lock-user-queue')
export class LockAccountConsumer {
  constructor(private readonly userRepository: UserRepository) {}

  @Process('lock-user-job')
  async lockAccJob(job: Job<LockUserJobDto>) {
    await this.userRepository.findOneByIdAndUpdate(job.data.userId, { isLocked: null });
    return {
      success: true,
      message: `User ${job.data.userId} was had unlock account`,
    };
  }
}
