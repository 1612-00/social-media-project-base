import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { LockUserDto } from 'src/api/admin/dto/lock-user.dto';
import { LockUserJobDto } from './dto/lock-user-job.dto';

export class LockAccountService {
  constructor(@InjectQueue('lock-user-queue') private queue: Queue) {}

  async addLockAccJob(lockUserDto: LockUserDto) {
    const dataJob: LockUserJobDto = {
      userId: lockUserDto.userId,
    };
    await this.queue.add('lock-user-job', dataJob, { delay: lockUserDto.time * 1000 });
  }
}
