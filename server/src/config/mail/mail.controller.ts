import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { MailService } from './mail.service';

@ApiTags('Mail')
@Controller('mail')
export class MailController {
  constructor(private readonly mailService: MailService) {}

  @Get()
  sendMail(): any {
    return this.mailService.getHello();
  }

  @Get('template')
  sendTemplate(): any {
    return this.mailService.example2();
  }
}
