import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { AppKey, AppObject } from '../share/common';
import { Post, React } from './PostSchema';
import { User } from './UserSchema';

export type CommentDocument = Comment & Document;

@Schema({ timestamps: true })
export class Comment {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: AppKey.TABLES.USER,
  })
  author: User;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: AppKey.TABLES.POST,
  })
  post: Post;

  @Prop({
    type: String,
    default: '',
  })
  content: string;

  @Prop({
    type: [
      {
        user: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.USER },
        react: { type: String, enum: AppObject.POST_REACT },
        date: { type: Date },
      },
    ],
    default: [],
  })
  reacts: React;
}

const CommentSchema = SchemaFactory.createForClass(Comment);

CommentSchema.index({
  author: 'text',
  post: 'text',
});

export { CommentSchema };
