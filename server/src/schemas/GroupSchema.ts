import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { Relation, User } from './UserSchema';
import { AppKey, AppObject } from '../share/common';
import { Post } from './PostSchema';

export type GroupDocument = Group & Document;

@Schema({ timestamps: true })
export class Group {
  @Prop({ default: '' })
  coverPicture: string;

  @Prop({ required: true })
  name: string;

  @Prop({ default: '' })
  description: string;

  @Prop({
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: AppKey.TABLES.USER,
  })
  admin: User;

  @Prop({
    enum: AppObject.GROUP_PRIVACY,
    default: AppObject.GROUP_PRIVACY.PUBLIC,
  })
  privacy: string;

  @Prop({
    type: [
      {
        user: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.USER },
        role: { type: String, enum: AppObject.GROUP_ROLE, default: AppObject.GROUP_ROLE.BASIC },
        date: { type: Date },
      },
    ],
    default: [],
  })
  members: Member[];

  @Prop({
    type: [
      {
        user: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.USER },
        date: { type: Date },
      },
    ],
    default: [],
  })
  blockList: Relation[];

  @Prop({
    type: [
      {
        user: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.USER },
        date: { type: Date },
      },
    ],
  })
  joinReqs: Relation[];

  @Prop({
    type: [
      {
        post: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.POST },
        date: { type: Date },
      },
    ],
  })
  waitingPosts: GroupPost[];

  @Prop({
    type: [
      {
        post: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.POST },
        date: { type: Date },
      },
    ],
  })
  releasePosts: GroupPost[];
}

const GroupSchema = SchemaFactory.createForClass(Group);

GroupSchema.index({
  name: 'text',
});

export interface Member {
  _id: mongoose.Schema.Types.ObjectId;
  user: User;
  role: string;
  date: Date;
}

export interface GroupPost {
  _id: mongoose.Schema.Types.ObjectId;
  post: Post;
  date: Date;
}

export { GroupSchema };
