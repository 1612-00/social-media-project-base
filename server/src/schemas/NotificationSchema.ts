import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { AppObject } from '../share/common';

export type NotificationDocument = Notification & Document;

@Schema({ timestamps: true })
export class Notification {
  @Prop({
    object: { type: String },
    type: { type: String, enum: AppObject.NOTIFICATION_ACTOR, required: true },
  })
  sender: NotificationObject;

  @Prop({
    object: { type: String },
    type: { type: String, enum: AppObject.NOTIFICATION_ACTOR, required: true },
  })
  receiver: NotificationObject;

  @Prop({
    object: { type: String },
    type: { type: String, enum: AppObject.NOTIFICATION_OBJECT, required: true },
  })
  object: NotificationObject | null;

  @Prop({
    required: true,
    type: String,
    enum: AppObject.NOTIFICATION_TYPE,
  })
  type: string;

  @Prop({
    required: true,
    type: Boolean,
    default: false,
  })
  seen: boolean;
}

const NotificationSchema = SchemaFactory.createForClass(Notification);

NotificationSchema.index({
  sender: 'text',
  receiver: 'text',
  type: 'text',
});

export interface NotificationObject {
  object: string;
  type: string;
}

export { NotificationSchema };
