import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { User } from './UserSchema';
import { AppKey, AppObject } from '../share/common';
import { Group } from './GroupSchema';

export type PostDocument = Post & Document;

@Schema({ timestamps: true })
export class Post {
  @Prop({
    required: true,
    type: String,
    enum: AppObject.POST_AUDIENCE,
    default: AppObject.POST_AUDIENCE.PUBLIC,
  })
  audience: string;

  @Prop({
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: AppKey.TABLES.USER,
  })
  author: User;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: AppKey.TABLES.GROUP,
    default: null,
  })
  groupId?: Group = null;

  @Prop({ type: String, default: '' })
  content: string;

  @Prop({ type: Array, default: [] })
  images: string[];

  @Prop({ type: Array, default: [] })
  videos: string[];

  @Prop({
    type: [
      {
        user: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.USER },
        reactType: { type: String, enum: AppObject.POST_REACT },
        date: { type: Date },
      },
    ],
    default: [],
  })
  reacts: React[];

  @Prop({
    type: [
      {
        user: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.USER },
        audience: { type: String, enum: AppObject.POST_AUDIENCE },
        date: { type: Date },
      },
    ],
    default: [],
  })
  shares: Share[];

  @Prop({
    type: [
      {
        mainComment: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.COMMENT },
        commentReplies: [{ type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.COMMENT, default: [] }],
      },
    ],
    default: [],
  })
  comments: ListComment[];
}

const PostSchema = SchemaFactory.createForClass(Post);

PostSchema.index({
  author: 'text',
});

export interface React {
  _id: mongoose.Schema.Types.ObjectId;
  user: User;
  reactType: string;
  date: Date;
}

export interface Share {
  _id: mongoose.Schema.Types.ObjectId;
  user: User;
  audience: string;
  date: Date;
}

export interface ListComment {
  _id: mongoose.Schema.Types.ObjectId;
  mainComment: mongoose.Schema.Types.ObjectId;
  commentReplies: mongoose.Schema.Types.ObjectId[];
}

export { PostSchema };
