import mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { UserRole } from '../api/user/role/role.enum';
import { AppKey, AppObject } from '../share/common';
import { Post } from './PostSchema';
import { Group } from './GroupSchema';

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
  @Prop({
    type: [
      {
        url: { type: String },
        isActive: { type: Boolean },
        date: { type: Date },
      },
    ],
    default: [],
  })
  avatars: Avatar[];

  @Prop({ required: true })
  email: string;

  @Prop()
  password: string | null;

  @Prop({ required: true })
  fullName: string;

  @Prop({ default: '' })
  address: string;

  @Prop({ default: '' })
  phoneNumber: string;

  @Prop()
  dateOfBirth: string;

  @Prop({ default: '' })
  job: string;

  @Prop({ default: false })
  isVerified: boolean;

  @Prop({
    required: true,
    type: String,
    enum: UserRole,
    default: UserRole.User,
  })
  role: string;

  @Prop({ default: false })
  twoStepVerify: boolean;

  @Prop({
    type: [
      {
        post: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.POST },
        date: { type: Date },
      },
    ],
  })
  posts: PostCreated[];

  @Prop({
    type: [
      {
        user: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.USER },
        date: { type: Date },
      },
    ],
  })
  friends: Relation[];

  @Prop({
    type: [
      {
        user: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.USER },
        date: { type: Date },
      },
    ],
  })
  friendReqs: Relation[];

  @Prop({
    type: [
      {
        user: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.USER },
        date: { type: Date },
      },
    ],
  })
  blockList: Relation[];

  @Prop({
    type: { status: { type: Boolean }, time: { type: Number }, reason: { type: String } },
  })
  isLocked: InfoLocked | null;

  @Prop({
    type: [
      {
        post: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.POST },
        audience: { type: String, enum: AppObject.POST_AUDIENCE, default: AppObject.POST_AUDIENCE.PUBLIC },
        date: { type: Date },
      },
    ],
  })
  postsShared: PostShared[];

  @Prop({
    type: [
      {
        group: { type: mongoose.Schema.Types.ObjectId, ref: AppKey.TABLES.GROUP },
        date: { type: Date },
      },
    ],
  })
  groupsJoined: GroupJoined[];

  @Prop({ type: String })
  refreshToken: string | null;
}

const UserSchema = SchemaFactory.createForClass(User);

UserSchema.index({
  email: 'text',
  phoneNumber: 'text',
});

export interface Avatar {
  _id: mongoose.Schema.Types.ObjectId;
  url: string;
  isActive: boolean;
  date: Date;
}

export interface PostCreated {
  _id: mongoose.Schema.Types.ObjectId;
  post: Post;
  date: Date;
}

export interface Relation {
  _id: mongoose.Schema.Types.ObjectId;
  user: User;
  date: Date;
}

export interface PostShared {
  _id: mongoose.Schema.Types.ObjectId;
  post: Post;
  audience: string;
  date: Date;
}

export interface InfoLocked {
  status: boolean;
  time: number;
  reason: string;
}

export interface GroupJoined {
  _id: mongoose.Schema.Types.ObjectId;
  group: Group;
  date: Date;
}

export { UserSchema };
