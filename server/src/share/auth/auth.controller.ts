import { Body, Controller, Post } from '@nestjs/common';
import { ApiConsumes, ApiTags } from '@nestjs/swagger';
import { CreateUserDto, VerifyOTPDto } from 'src/api/user/dto';
import { AuthService } from './auth.service';
import { ForgotPasswordDto, LoginUserDto, ResetPasswordDto } from './dto';

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/register')
  @ApiConsumes('application/x-www-form-urlencoded')
  register(@Body() createUserDto: CreateUserDto) {
    return this.authService.register(createUserDto);
  }

  @Post('/login')
  @ApiConsumes('application/x-www-form-urlencoded')
  login(@Body() loginUserDto: LoginUserDto) {
    return this.authService.login(loginUserDto);
  }

  @Post('/verify-otp')
  @ApiConsumes('application/x-www-form-urlencoded')
  verifyOtp(@Body() verifyOtpDto: VerifyOTPDto) {
    return this.authService.verifyOtp(verifyOtpDto);
  }

  @Post('forgot-password')
  @ApiConsumes('application/x-www-form-urlencoded')
  forgotPassword(@Body() forgotPasswordDto: ForgotPasswordDto) {
    return this.authService.forgotPassword(forgotPasswordDto);
  }

  @Post('reset-password')
  @ApiConsumes('application/x-www-form-urlencoded')
  resetPassword(@Body() resetPasswordDto: ResetPasswordDto) {
    return this.authService.resetPassword(resetPasswordDto);
  }
}
