import { SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { AuthService } from './auth.service';

@WebSocketGateway({
  transports: ['websocket'],
  cors: {
    origin: '*',
  },
})
export class AuthGateway {
  @WebSocketServer()
  server: Server;

  constructor(private readonly authService: AuthService) {}

  // handleConnection(client: Socket & { userId?: string }) {
  //   // console.log(`Client ${client.id} connected`);
  //   if (client.userId) {
  //     client.join(client.userId);
  //   }
  //   console.log(client.rooms);
  // }

  @SubscribeMessage('joinRoomLogin')
  handleJoinRoomLogin(client: Socket, payload: any) {
    client.join(payload.userId);
  }

  async handleConnection(client: Socket) {
    client.setMaxListeners(500);
  }
}
