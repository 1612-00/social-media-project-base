import { JwtService } from '@nestjs/jwt';
import { Test } from '@nestjs/testing';
import { UserService } from '../../api/user/user.service';
import { MailService } from '../../config/mail/mail.service';
import { AuthService } from './auth.service';
import * as bcrypt from 'bcrypt';
import { BadRequestException, ForbiddenException, NotFoundException } from '@nestjs/common';
import { ForgotPasswordDto, ResetPasswordDto } from './dto';

describe(' Test suite', () => {
  let service: AuthService;

  const mockUserService = {
    create: jest.fn((dto) => ({ ...dto, _id: expect.any(String) })),
    getUserByCondition: jest.fn((condition) => ({
      _id: expect.any(String),
      ...condition,
      password: expect.any(String),
    })),
    hashPassword: jest.spyOn(bcrypt, 'compareSync').mockImplementation((passwordInput, hashPassword) => true),
    resetPassword: jest.fn(({ newPassword }) => ({ password: newPassword })),
    saveRefreshToken: jest.fn(() => expect.any(String)),
  };
  const mockJwtService = {
    signAsync: jest.fn((payload, {}) => expect.any(String)),
  };
  const mockMailService = {
    confirmAccount: jest.fn(() => true),
    forgotPassword: jest.fn(() => true),
  };

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UserService,
          useValue: mockUserService,
        },
        {
          provide: JwtService,
          useValue: mockJwtService,
        },
        {
          provide: MailService,
          useValue: mockMailService,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  describe('should register user', () => {
    const dto = {
      email: 'nguyenducanh.ldb@gmail.com',
      password: 'ducanh',
      fullName: 'Duc Anh Nguyen',
    };
    it('should register user no exception', async () => {
      expect(await service.register(dto)).toEqual({
        user: {
          _id: expect.any(String),
          email: dto.email,
          fullName: dto.fullName,
        },
        accessToken: expect.any(String),
        refreshToken: expect.any(String),
      });
    });

    it('should send mail failed', async () => {
      mockMailService.confirmAccount.mockImplementation(() => false);
      try {
        await service.register(dto);
      } catch (error) {
        expect(error).toBeInstanceOf(ForbiddenException);
      }
    });
  });

  describe('should login user', () => {
    const dto = {
      email: 'nguyenducanh.ldb@gmail.com',
      password: 'ducanh',
    };
    // it('should login user', async () => {
    //   expect(await service.login(dto)).toEqual({
    //     accessToken: expect.any(String),
    //     refreshToken: expect.any(String),
    //   });
    // });
    it('should throw error if password not exist', async () => {
      mockUserService.getUserByCondition.mockImplementation((condition) => ({
        ...condition,
        password: null,
      }));
      expect(service.login(dto)).rejects.toThrowError(NotFoundException);
    });
    it('should throw error if password not matches', async () => {
      jest.spyOn(bcrypt, 'compareSync').mockImplementation((passwordInput, hashPassword) => false);
      expect(service.login(dto)).rejects.toThrowError(NotFoundException);
    });
    it('should user account not verify', async () => {
      mockUserService.getUserByCondition(({ email: email }) => ({
        email,
        isVerified: false,
      }));
      try {
        expect(await service.login(dto)).toThrow(BadRequestException);
      } catch (error) {}
    });
  });

  describe('forgot password', () => {
    const dto: ForgotPasswordDto = { email: 'nguyenducanh.ldb@gmail.com' };
    it('should send email forgot password no exception', async () => {
      mockUserService.getUserByCondition.mockImplementation((condition) => ({
        ...condition,
        _id: expect.any(String),
        isVerified: true,
      }));
      expect(await service.forgotPassword(dto)).toEqual({ status: 200, message: 'please check your email' });
    });
    it('should throw error when user was not verified', async () => {
      mockUserService.getUserByCondition.mockImplementation((condition) => ({
        ...condition,
        _id: expect.any(String),
        isVerified: false,
      }));
      expect(service.forgotPassword(dto)).rejects.toThrowError(BadRequestException);
    });
    it('should throw error when send mail failed', async () => {
      mockMailService.forgotPassword.mockImplementation(() => false);
      expect(service.forgotPassword(dto)).rejects.toThrowError(BadRequestException);
    });
  });

  describe('reset password', () => {
    const dto: ResetPasswordDto = { token: expect.any(String), newPassword: expect.any(String) };
    it('should reset password', async () => {
      expect(await service.resetPassword(dto)).toEqual({ password: dto.newPassword });
    });
  });
});
