import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateOTPDto, CreateUserDto, VerifyOTPDto } from '../../api/user/dto';
import { UserService } from '../../api/user/user.service';
import { JWT_CONFIG } from '../../config';
import { RegisterResponseDto } from './dto/register-response.dto';
import { JwtPayload } from './payloads/jwt-payload';
import { JwtService } from '@nestjs/jwt';
import { ForgotPasswordDto, LoginResponseDto, LoginUserDto } from './dto';
import { ERROR } from '../common';
import * as bcrypt from 'bcrypt';
import { MailService } from '../../config/mail/mail.service';
import { UserRole } from 'src/api/user/role/role.enum';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly mailService: MailService,
  ) {}

  async register(createUserDto: CreateUserDto) {
    const user = await this.userService.create(createUserDto);
    const payload: JwtPayload = {
      id: user._id.toString(),
      email: user.email,
      role: UserRole.User,
    };
    const jwtExpiresIn = parseInt(JWT_CONFIG.expiresIn);
    const refreshJwtExpiresIn = parseInt(JWT_CONFIG.refreshExpiresIn);

    const accessToken = await this.jwtService.signAsync(payload, {
      secret: JWT_CONFIG.secret,
      expiresIn: jwtExpiresIn,
    });
    const refreshToken = await this.jwtService.signAsync(payload, {
      secret: JWT_CONFIG.refreshSecret,
      expiresIn: refreshJwtExpiresIn,
    });
    delete user.password;

    await this.userService.saveRefreshToken(user._id.toString(), refreshToken);

    const sentMailConfirm = this.mailService.confirmAccount(user, accessToken);

    if (!sentMailConfirm) throw new ForbiddenException(ERROR.SEND_MAIL_FAILURE.MESSAGE);

    return {
      user,
      // accessToken,
      // refreshToken,
    };
  }

  async login(loginUserDto: LoginUserDto): Promise<LoginResponseDto | object> {
    try {
      const user = await this.userService.getUserByCondition({
        email: loginUserDto.email,
      });
      if (!user.password) {
        throw new NotFoundException(ERROR.USERNAME_OR_PASSWORD_INCORRECT.MESSAGE);
      }
      const matchesPassword = bcrypt.compareSync(loginUserDto.password, user.password);
      if (!matchesPassword) {
        throw new NotFoundException(ERROR.USERNAME_OR_PASSWORD_INCORRECT.MESSAGE);
      }

      // Check user'account has been locked
      if (user.isLocked && user.isLocked.status) {
        throw new BadRequestException(
          `Your account has been locked in ${user.isLocked.time / 1000 / 60} minute because you ${
            user.isLocked.reason
          }`,
        );
      }

      if (user.twoStepVerify && user.phoneNumber !== '') {
        try {
          const createOtpDto: CreateOTPDto = {
            email: user.email,
            phoneNumber: user.phoneNumber,
          };
          await this.userService.createOTP2StepVerification(createOtpDto);
          return { status: 200, message: 'Please check your phone and confirm OTP code to login', user };
        } catch (error) {
          throw error;
        }
      }

      const payload: JwtPayload = {
        id: user._id.toString(),
        email: user.email,
        role: user.role,
      };
      const jwtExpiresIn = parseInt(JWT_CONFIG.expiresIn);
      const refreshJwtExpiresIn = parseInt(JWT_CONFIG.refreshExpiresIn);

      const accessToken = await this.jwtService.signAsync(payload, {
        secret: JWT_CONFIG.secret,
        expiresIn: jwtExpiresIn,
      });
      const refreshToken = await this.jwtService.signAsync(payload, {
        secret: JWT_CONFIG.refreshSecret,
        expiresIn: refreshJwtExpiresIn,
      });

      await this.userService.saveRefreshToken(user._id.toString(), refreshToken);

      // if user unauthenticated, throw an error
      if (!user.isVerified) {
        const sentMailConfirm = this.mailService.confirmAccount(user, accessToken);
        if (!sentMailConfirm) throw new ForbiddenException(ERROR.SEND_MAIL_FAILURE.MESSAGE);

        throw new BadRequestException(ERROR.UNAUTHENTICATED_USER.MESSAGE);
      }

      return {
        accessToken,
        refreshToken,
      };
    } catch (error) {
      throw error;
    }
  }

  async verifyOtp(verifyOTPDto: VerifyOTPDto) {
    try {
      // verify email
      const user = await this.userService.getUserByCondition({
        email: verifyOTPDto.email,
      });
      if (!user) {
        throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);
      }

      // verify otp
      const otpVerify = await this.userService.verifyOTP2StepVerification(verifyOTPDto);
      if (!otpVerify) throw new ForbiddenException(ERROR.OTP_INVALID.MESSAGE);

      const payload: JwtPayload = {
        id: user._id.toString(),
        email: user.email,
        role: user.role,
      };
      const jwtExpiresIn = parseInt(JWT_CONFIG.expiresIn);
      const refreshJwtExpiresIn = parseInt(JWT_CONFIG.refreshExpiresIn);

      const accessToken = await this.jwtService.signAsync(payload, {
        secret: JWT_CONFIG.secret,
        expiresIn: jwtExpiresIn,
      });
      const refreshToken = await this.jwtService.signAsync(payload, {
        secret: JWT_CONFIG.refreshSecret,
        expiresIn: refreshJwtExpiresIn,
      });

      await this.userService.saveRefreshToken(user._id.toString(), refreshToken);

      return {
        accessToken,
        refreshToken,
      };
    } catch (error) {
      throw error;
    }
  }

  async signAccessToken(payload) {
    const jwtExpiresIn = parseInt(JWT_CONFIG.expiresIn);
    const accessToken = await this.jwtService.signAsync(payload, {
      secret: JWT_CONFIG.secret,
      expiresIn: jwtExpiresIn,
    });
    return { accessToken, accessTokenExpire: jwtExpiresIn };
  }

  async forgotPassword(forgotPasswordDto: ForgotPasswordDto) {
    const user = await this.userService.getUserByCondition({
      email: forgotPasswordDto.email,
    });

    if (!user.isVerified) {
      throw new BadRequestException(ERROR.ACCOUNT_IS_NOT_VERIFIED.MESSAGE);
    }

    const payload: JwtPayload = {
      id: user._id.toString(),
      email: user.email,
      role: user.role,
    };
    const jwtExpiresIn = parseInt(JWT_CONFIG.expiresIn);

    const accessToken = await this.jwtService.signAsync(payload, {
      secret: JWT_CONFIG.secret,
      expiresIn: jwtExpiresIn,
    });

    const sentMailConfirm = this.mailService.forgotPassword(user.email, accessToken);
    if (!sentMailConfirm) throw new BadRequestException(ERROR.SEND_MAIL_FAILURE.MESSAGE);
    return { status: 200, message: 'please check your email' };
  }

  async resetPassword(resetPasswordDto) {
    return this.userService.resetPassword(resetPasswordDto);
  }
}
