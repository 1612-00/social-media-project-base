import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class ForgotPasswordDto {
  @ApiProperty({ description: 'email' })
  @IsEmail()
  @IsString()
  @IsNotEmpty()
  email: string;
}
