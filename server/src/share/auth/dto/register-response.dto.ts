export interface RegisterResponseDto {
  user: object;
  accessToken: string;
  accessTokenExpire?: number | string;
  refreshToken: string;
  refreshTokenExpire?: number | string;
}
