import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class ResetPasswordDto {
  @ApiProperty({ description: 'token' })
  @IsString()
  @IsNotEmpty()
  token: string;

  @ApiProperty({ description: 'newPassword' })
  @IsString()
  @IsNotEmpty()
  newPassword: string;
}
