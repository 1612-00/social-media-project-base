import { CanActivate, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/api/user/user.service';

@Injectable()
export class WsGuard implements CanActivate {
  constructor(private userService: UserService, private readonly jwtService: JwtService) {}

  canActivate(context: any): boolean | any | Promise<boolean | any> {
    const bearerToken = context.args[0].handshake.headers.authorization.split(' ')[1];
    console.log(bearerToken);
    try {
      const decoded = this.jwtService.verify(bearerToken) as any;
      return new Promise((resolve, reject) => {
        return this.userService.getUserByCondition({ email: decoded.email }).then((user) => {
          if (user) {
            resolve(user);
          } else {
            reject(false);
          }
        });
      });
    } catch (ex) {
      console.log(ex);
      return false;
    }
  }
}
