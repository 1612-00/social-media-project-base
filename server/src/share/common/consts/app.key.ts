export class AppKey {
  static readonly TABLES = {
    /* PLOP_INJECT_TABLE */
    USER: 'User',
    POST: 'Post',
    COMMENT: 'Comment',
    GROUP: 'Group',
    NOTIFICATION: 'Notification',
    NOTIFICATION_TYPE: 'NotificationType',
  };
}
