export class AppObject {
  static readonly RELATIONSHIPS = {
    FRIEND: 'friend',
    PENDING: 'pending',
    REJECT: 'rejected',
    BLOCK: 'block',
  };
  static readonly PROFILE_PICTURE = {
    NON_ACTIVE: 'non active',
    ACTIVE: 'active',
  };
  static readonly POST_AUDIENCE = {
    PUBLIC: 'public',
    FRIENDS: 'friends',
    ONLY_ME: 'only me',
  };
  static readonly POST_REACT = {
    LIKE: 'like',
    HEART: 'heart',
  };
  static readonly GROUP_PRIVACY = {
    PUBLIC: 'public',
    PRIVATE: 'private',
  };
  static readonly GROUP_ROLE = {
    ADMIN: 'admin',
    SENSOR: 'sensor',
    BASIC: 'basic',
  };
  static readonly NOTIFICATION_TYPE = {
    ADD_FRIEND: 'addFriend',
    ACCEPT_FRIEND_REQ: 'acceptFriendRequest',
    REQ_JOIN_GROUP: 'requestJoinGroup',
    ADMIN_CREATE_POST_GROUP: 'adminCreatePostGroup',
    BASIC_CREATE_POST_GROUP: 'basicCreatePostGroup',
    ACCEPT_REQ_JOIN_GROUP: 'acceptReqJoinGroup',
    CHANGE_MEMBER_ROLE: 'changeMemberRole',
    ACCEPT_WAITING_POST: 'acceptWaitingPost',
    LIKE_POST: 'likePost',
    LOVE_POST: 'lovePost',
    SHARE_POST: 'sharePost',
    COMMENT_POST: 'commentPost',
    REPLY_COMMENT_POST: 'replyCommentPost',
  };
  static readonly NOTIFICATION_ACTOR = {
    GROUP: 'group',
    USER: 'user',
  };
  static readonly NOTIFICATION_OBJECT = {
    GROUP: 'group',
    USER: 'user',
    POST: 'post',
    COMMENT: 'comment',
  };
  static readonly QR_TYPE = {
    USER: 'user',
    GROUP: 'group',
  };
}
