export const ERROR = {
  COMMON_SYSTEM_ERROR: {
    CODE: 'sys00001',
    MESSAGE: 'An error has arisen from the system. Please try again later or contact us for a fix.',
  },

  // Authentication user
  CONFIRM_ACCOUNT_FAILURE: {
    MESSAGE: 'Invalid or expired email.',
  },
  ACCOUNT_IS_NOT_VERIFIED: {
    MESSAGE: 'Account is not verified.',
  },
  SEND_MAIL_FAILURE: {
    MESSAGE: 'Send email failure.',
  },
  REFRESH_TOKEN_NOT_FOUND: {
    MESSAGE: 'Refresh token not found, please try login again.',
  },
  REFRESH_TOKEN_IN_VALID: {
    MESSAGE: 'Refresh token invalid.',
  },

  // user
  USER_NOT_FOUND: {
    MESSAGE: 'User not found, disabled or locked',
  },
  PHONE_NUMBER_NOT_FOUND: {
    MESSAGE: 'Phone number not found, please update your phone number',
  },
  OTP_INVALID: {
    MESSAGE: 'Otp invalid',
  },
  USER_HAD_BLOCKED: {
    MESSAGE: 'User had blocked',
  },
  USERNAME_OR_PASSWORD_INCORRECT: {
    MESSAGE: 'Username or password is incorrect',
  },
  EMAIL_ALREADY_EXIST: {
    MESSAGE: 'Email already exist',
  },
  USER_ID_ALREADY_EXIST: {
    MESSAGE: 'User id already exist',
  },
  DONT_PERMISSION_ACCESS: {
    MESSAGE: 'You dont have permission to access',
  },
  DONT_PERMISSION_DELETE: {
    MESSAGE: 'You dont have permission to delete',
  },
  NOT_TRY_LOGOUT_FROM_EARTH: {
    MESSAGE: 'Not try logout from earth!!!',
  },
  UNAUTHENTICATED_USER: {
    MESSAGE: 'Unauthenticated user',
  },

  // friend
  ALREADY_SENT_REQ: {
    MESSAGE: 'You already sent friend request for this friend',
  },
  ALREADY_HAVE_REQ: {
    MESSAGE: 'You already have a friend request for this friend',
  },
  ALREADY_FRIEND: {
    MESSAGE: 'Already friend',
  },
  FRIEND_REQ_NOT_FOUND: {
    MESSAGE: 'Friend request not found',
  },
  NOT_FRIEND: {
    MESSAGE: 'Not friend',
  },
  INVALID_REQ: {
    MESSAGE: 'Invalid request',
  },

  // Black list
  DONT_INTERACTIVE_MYSELF: {
    MESSAGE: "Don't interactive myself",
  },
  USER_HAS_BLOCKED: {
    MESSAGE: 'User has blocked',
  },
  USER_HAS_NOT_BLOCKED: {
    MESSAGE: 'User has not blocked',
  },
  HAD_BLOCKED_BY_THIS_USER: {
    MESSAGE: 'Had blocked by this user',
  },

  // Post
  POST_CONTENT_EMPTY: {
    MESSAGE: 'Post content is empty',
  },
  NOT_HAVE_ANY_CHANGE: {
    MESSAGE: 'Do not have any changes',
  },
  CREATE_POST_FAILED: {
    MESSAGE: 'Create post failed',
  },
  POST_NOT_FOUND: {
    MESSAGE: 'Post not found',
  },
  HAD_SHARED_THIS_POST: {
    MESSAGE: 'You had shared this post',
  },
  HAVE_NOT_SHARED_THIS_POST_YET: {
    MESSAGE: 'You have not shared this post yet',
  },
  NOT_SHARE_OWN_POST: {
    MESSAGE: 'Not share own post',
  },
  COMMENT_NOT_FOUND: {
    MESSAGE: 'Comment not found',
  },
  UPLOAD_POST_FAILED: {
    MESSAGE: 'Upload post failed',
  },

  // Avatar
  AVATAR_NOT_FOUND: {
    MESSAGE: 'Avatar not found',
  },
  IS_CURRENT_AVATAR: {
    MESSAGE: 'Is current avatar',
  },
  UPLOAD_AVATAR_FAILED: {
    MESSAGE: 'Upload avatar failed',
  },
  RETRIEVE_AVATAR_FAILED: {
    MESSAGE: 'Retrieve avatar failed',
  },

  // Group
  GROUP_NOT_FOUND: {
    MESSAGE: 'Group not found',
  },
  HAS_JOINED_GROUP: {
    MESSAGE: 'Has joined group',
  },
  HAS_NOT_JOINED_GROUP: {
    MESSAGE: 'Has not joined group',
  },
  HAD_SENT_JOIN_REQ: {
    MESSAGE: 'Had sent join request',
  },
  JOIN_REQ_NOT_FOUND: {
    MESSAGE: 'Join request not found',
  },
  MEMBER_NOT_FOUND: {
    MESSAGE: 'Member not found',
  },
  NOT_CHANGE_TO_ADMIN_ROLE: {
    MESSAGE: 'Not change to admin role',
  },
  POST_IN_GROUP_NOT_FOUND: {
    MESSAGE: 'Post in group not found',
  },
  DELETE_MEMBER_FAILED: {
    MESSAGE: 'Delete member failed',
  },

  // Notification
  NOTIFICATION_NOT_FOUND: {
    MESSAGE: 'Notification not found',
  },
};
