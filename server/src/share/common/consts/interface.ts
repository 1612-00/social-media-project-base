export interface FolderSaveFile {
  mainFolder: string;
  subFolder: string;
  fileExtension: string;
}
