import * as jwt from 'jsonwebtoken';

const _accessTokenSecret = (): string => {
  return process.env.TOKEN_SECRET || 'super_secret';
};

export const verifyAccessToken = (token: string): any => {
  return jwt.verify(token, _accessTokenSecret());
};
