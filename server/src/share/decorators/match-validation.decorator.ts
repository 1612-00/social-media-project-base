import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

export function Match(property: string, validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [property],
      validator: new MatchConstraint(property),
    });
  };
}

@ValidatorConstraint({ name: 'Match' })
export class MatchConstraint implements ValidatorConstraintInterface {
  constructor(private property: string) {}

  validate(value: any, args?: ValidationArguments): boolean | Promise<boolean> {
    const [relatedPropertyName] = args.constraints;
    const relatedValue = (args.object as any)[relatedPropertyName];
    return value === relatedValue;
  }
  defaultMessage?(validationArguments?: ValidationArguments): string {
    throw new Error(`Not match with ${this.property}`);
  }
}

export function NotMatch(property: string, validationOptions?: ValidationOptions) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [property],
      validator: new NotMatchConstraint(property),
    });
  };
}

@ValidatorConstraint({ name: 'Not Match' })
export class NotMatchConstraint implements ValidatorConstraintInterface {
  constructor(private property: string) {}

  validate(value: any, args?: ValidationArguments): boolean | Promise<boolean> {
    const [relatedPropertyName] = args.constraints;
    const relatedValue = (args.object as any)[relatedPropertyName];
    return value !== relatedValue;
  }
  defaultMessage?(validationArguments?: ValidationArguments): string {
    throw new Error(`Match with ${this.property}`);
  }
}
