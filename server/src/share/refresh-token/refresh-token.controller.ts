import { Controller, Post, Body, Get } from '@nestjs/common';
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { RefreshTokenService } from './refresh-token.service';

@ApiTags('RefreshToken')
@Controller('refresh-token')
export class RefreshTokenController {
  constructor(private readonly refreshTokenService: RefreshTokenService) {}

  @Post('/')
  verifyRefreshToken(@Body('refreshToken') refreshToken: string) {
    return this.refreshTokenService.verifyRefreshToken(refreshToken);
  }

  @Post('/get-access-token')
  @ApiConsumes('application/x-www-form-urlencoded')
  @ApiBody({
    required: true,
    schema: {
      type: 'object',
      properties: {
        refreshToken: {
          type: 'string',
        },
      },
    },
  })
  handleSignAccessTokenFromRefreshToken(@Body('refreshToken') refreshToken: string) {
    return this.refreshTokenService.handleSignAccessTokenFromRefreshToken(refreshToken);
  }
}
