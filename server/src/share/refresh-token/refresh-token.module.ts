import { Module } from '@nestjs/common';
import { RefreshTokenService } from './refresh-token.service';
import { RefreshTokenController } from './refresh-token.controller';
import { JwtModule } from '@nestjs/jwt';
import { JWT_CONFIG } from 'src/config';
import { UserModule } from 'src/api/user/user.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    JwtModule.register({
      secret: JWT_CONFIG.refreshSecret,
      signOptions: {
        expiresIn: JWT_CONFIG.refreshExpiresIn,
      },
    }),
    UserModule,
    AuthModule,
  ],
  controllers: [RefreshTokenController],
  providers: [RefreshTokenService],
})
export class RefreshTokenModule {}
