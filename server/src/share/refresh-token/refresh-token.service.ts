import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserRepository } from 'src/api/user/user.repository';
import { JWT_CONFIG } from 'src/config';
import { AuthService } from '../auth/auth.service';
import { ERROR } from '../common';

@Injectable()
export class RefreshTokenService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userRepository: UserRepository,
    private readonly authService: AuthService,
  ) {}

  async verifyRefreshToken(refreshToken: string) {
    const user = await this.jwtService.verify(refreshToken);

    const userFound = await this.userRepository.findByIdAndSelectFields(user.id);
    if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

    if (!userFound.refreshToken) throw new ForbiddenException(ERROR.REFRESH_TOKEN_NOT_FOUND.MESSAGE);
    if (userFound.refreshToken !== refreshToken) throw new BadRequestException(ERROR.REFRESH_TOKEN_IN_VALID.MESSAGE);

    delete userFound.password;
    return userFound;
  }

  async handleSignAccessTokenFromRefreshToken(refreshToken: string) {
    const user = await this.jwtService.verify(refreshToken);

    const userFound = await this.userRepository.findByIdAndSelectFields(user.id);
    if (!userFound) throw new NotFoundException(ERROR.USER_NOT_FOUND.MESSAGE);

    if (!userFound.refreshToken) throw new ForbiddenException(ERROR.REFRESH_TOKEN_NOT_FOUND.MESSAGE);
    if (userFound.refreshToken !== refreshToken) throw new BadRequestException(ERROR.REFRESH_TOKEN_IN_VALID.MESSAGE);

    // Token valid
    const { accessToken, accessTokenExpire } = await this.authService.signAccessToken({
      id: user.id,
      email: user.email,
      role: user.role,
    });

    return {
      accessToken,
      accessTokenExpire,
    };
  }
}
