import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { UserRepository } from 'src/api/user/user.repository';
import { UserService } from 'src/api/user/user.service';
import { MailService } from 'src/config/mail/mail.service';

@Injectable()
export class NotificationCronjob {
  constructor(
    private readonly mailService: MailService,
    private readonly userRepository: UserRepository,
    private readonly userService: UserService,
  ) {}

  // @Cron(CronExpression.EVERY_30_SECONDS)
  testCronjob() {
    console.log('run ...');
  }

  // @Cron(CronExpression.EVERY_WEEK)
  // @Cron(CronExpression.EVERY_MINUTE)
  async sendMailStatisticWeeklyForUser() {
    const users = await this.userRepository.find();
    const now = new Date();
    const startDate = new Date(now.setDate(now.getDate() - 7));
    const endDate = new Date();

    await Promise.all(
      users.map(async (user) => {
        if (user.isVerified) {
          const content = await this.userService.statisticUserActionWeekly(user._id.toString(), startDate, endDate);
          this.mailService.sendMailStatisticWeekly(user.email, { startDate, endDate, content });
          console.log('send mail ...');
        }
      }),
    );
  }
}
