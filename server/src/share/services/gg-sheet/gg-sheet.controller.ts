import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { GGSheetService } from './gg-sheet.service';

@ApiTags('GG Sheet')
@Controller('gg-sheet')
export class GGSheetController {
  constructor(private readonly ggSheetService: GGSheetService) {}

  @Get('')
  register() {
    return this.ggSheetService.exportGG();
  }
}
