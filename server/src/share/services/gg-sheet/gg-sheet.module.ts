import { Module } from '@nestjs/common';
import { UserModule } from 'src/api/user/user.module';
import { GGSheetController } from './gg-sheet.controller';
import { GGSheetService } from './gg-sheet.service';

@Module({
  imports: [UserModule],
  controllers: [GGSheetController],
  providers: [GGSheetService],
  exports: [GGSheetService],
})
export class GGSheetModule {}
