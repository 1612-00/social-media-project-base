import { GS_CONFIG } from '../../../config/constant.config';
import { HttpException, HttpStatus, Injectable, StreamableFile } from '@nestjs/common';
import { appendFile, createReadStream, writeFile } from 'fs';
import { google } from 'googleapis';
import * as fs from 'fs';
import { join } from 'path';
import { json2csvAsync } from 'json-2-csv';
import { convertCSVToArray } from 'convert-csv-to-array';
import { UserService } from 'src/api/user/user.service';

@Injectable()
export class GGSheetService {
  constructor(private readonly userService: UserService) {}

  async exportFile() {
    const now = new Date();
    const startDate = new Date(now.setDate(now.getDate() - 7));
    const endDate = new Date();
    const data = await this.userService.statisticNewUsers(startDate, endDate);
    const file = await json2csvAsync(await data);
    appendFile(join(__dirname, 'data.csv'), file, (err) => {
      if (err) {
        throw new HttpException("Couldn't create the csv", HttpStatus.NOT_FOUND);
      }
    });

    const sendFile = createReadStream(join(__dirname, 'data.csv'));
    return new StreamableFile(sendFile);
  }

  async exportGG() {
    const client = new google.auth.JWT(GS_CONFIG.client_email, null, GS_CONFIG.private_key, [
      'https://www.googleapis.com/auth/spreadsheets',
    ]);

    client.authorize((err, tokens) => {
      if (err) {
        return;
      }
    });

    return this.gsun(client);
  }

  async gsun(client) {
    const data1 = await this.userService.getAll();
    // console.log(data1);
    const data2 = [
      {
        id: '1',
        deletedAt: null,
        fullName: 'Nguyen Gia Khiem',
        firstname: 'khiem',
        lastname: 'khiem',
        email: 'khiem@gmail.com',
        numberphone: '099999999',
        address: 'HN',
        age: '12',
        avatar: '',
        cmnd: '4242',
        soBHSK: '5245252',
      },
      {
        id: '2',
        deletedAt: null,
        fullName: 'Vu Trong Chuong',
        firstname: 'Duy',
        lastname: 'Chuong',
        email: 'chuongpm210@gmail.com',
        numberphone: '099999999',
        address: 'HN',
        age: '21',
        avatar: 'localhost:9000/nest/a598fd8c1bb5a9c82f5f95ab51acb99a.jpg',
        cmnd: '424235',
        soBHSK: '5245252435',
      },
    ];

    const file = await json2csvAsync(data2);
    writeFile(join(__dirname, 'data.csv'), file, (err) => {
      if (err) {
        throw new HttpException("Couldn't create the csv", HttpStatus.NOT_FOUND);
      }
    });
    const gsapi = google.sheets({ version: 'v4', auth: client });
    const metaData = await gsapi.spreadsheets.get({
      auth: client,
      spreadsheetId: GS_CONFIG.sheep_id,
    });

    const data = fs.readFileSync(join(__dirname, 'data.csv'), 'utf8');
    console.log(data);
    const arrayData = convertCSVToArray(data, {
      type: 'array',
      separator: ',',
    });
    console.log(arrayData);

    const updateOptions = {
      spreadsheetId: GS_CONFIG.sheep_id,
      range: 'data!A1',
      valueInputOption: 'USER_ENTERED',
      resource: { values: arrayData },
    };

    gsapi.spreadsheets.values.update(updateOptions, function (err, response) {
      if (err) {
        return;
      }
    });

    return { url: metaData.data.spreadsheetUrl };
  }
}
