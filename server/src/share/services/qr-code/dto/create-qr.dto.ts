import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { AppObject } from 'src/share/common';

export class CreateQRDto {
  @ApiProperty({ description: 'QR title' })
  @IsString()
  @IsEnum(AppObject.QR_TYPE)
  @IsNotEmpty()
  title: string;

  @ApiProperty({ description: 'QR content' })
  @IsString()
  @IsNotEmpty()
  content: string;
}
