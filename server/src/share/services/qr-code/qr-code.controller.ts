import {
  Body,
  Controller,
  FileTypeValidator,
  Get,
  MaxFileSizeValidator,
  ParseFilePipe,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/share/auth/guard/jwt.guard';
import { GetUser } from 'src/share/decorators/get-user.decorator';
import { CreateQRDto } from './dto/create-qr.dto';
import { QrCodeService } from './qr-code.service';

@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
@ApiTags('QR Code')
@Controller('qr-code')
export class QrCodeController {
  constructor(private readonly qrCodeService: QrCodeService) {}

  @Post()
  createQRCode(@Body() createQRDto: CreateQRDto) {
    return this.qrCodeService.createQRCode(createQRDto);
  }

  @ApiConsumes('multipart/form-data')
  @ApiBody({
    required: true,
    schema: {
      type: 'object',
      properties: {
        QRCode: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor('QRCode'))
  @Post('/read')
  readQRCode(
    @GetUser('id') userId: string,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: Math.pow(1024, 2) }),
          new FileTypeValidator({ fileType: '(jpg|jpeg|png|JPG|JPEG|PNG)' }),
        ],
      }),
    )
    file: Express.Multer.File,
  ) {
    return this.qrCodeService.readQRCode(userId, file);
  }
}
