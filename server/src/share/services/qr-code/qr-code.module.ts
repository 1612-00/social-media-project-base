import { Module } from '@nestjs/common';
import { GroupModule } from 'src/api/group/group.module';
import { UserModule } from 'src/api/user/user.module';
import { QrCodeController } from './qr-code.controller';
import { QrCodeService } from './qr-code.service';

@Module({
  imports: [UserModule, GroupModule],
  controllers: [QrCodeController],
  providers: [QrCodeService],
})
export class QrCodeModule {}
