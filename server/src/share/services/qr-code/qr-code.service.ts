import { Injectable } from '@nestjs/common';
import * as QRCode from 'qrcode';
import { CreateQRDto } from './dto/create-qr.dto';
import * as Jimp from 'jimp';
import * as qrCodeReader from 'qrcode-reader';
import { AppObject } from 'src/share/common';
import { UserService } from 'src/api/user/user.service';
import { GroupService } from 'src/api/group/group.service';

@Injectable()
export class QrCodeService {
  constructor(private readonly userService: UserService, private readonly groupService: GroupService) {}

  async createQRCode(createQRDto: CreateQRDto) {
    try {
      let img = '';
      const qr = await QRCode.toDataURL(`${createQRDto.title}_${createQRDto.content}`);
      img = `<image src= " ` + qr + `" />`;
      return img;
    } catch (error) {
      throw error;
    }
  }

  async readQRCode(userId: string, file: Express.Multer.File) {
    try {
      let valueGot;
      const data = await Jimp.read(file.buffer);
      const qrcode = new qrCodeReader();
      qrcode.callback = async function (err, value) {
        if (err) {
          console.error(err);
        }
        // Printing the decrypted value
        valueGot = value.result;
      };
      qrcode.decode(data.bitmap);

      const arrDataSplit = valueGot.split('_');
      switch (arrDataSplit[0]) {
        case AppObject.QR_TYPE.USER:
          return await this.userService.getUserById(arrDataSplit[1]);
        case AppObject.QR_TYPE.GROUP:
          return await this.groupService.detailGroupById(userId, arrDataSplit[1]);
      }
    } catch (error) {
      throw error;
    }
  }
}
