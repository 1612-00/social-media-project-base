import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class SetRedisDto {
  @ApiProperty({ description: 'key' })
  @IsString()
  @IsNotEmpty()
  key: string;

  @ApiProperty({ description: 'value' })
  @IsString()
  @IsNotEmpty()
  value: string;

  @ApiProperty({ description: 'ttl' })
  @IsNumber()
  @IsNotEmpty()
  ttl: number;
}
