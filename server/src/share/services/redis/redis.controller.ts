import { Body, CacheInterceptor, Controller, Get, Param, Post, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { SetRedisDto } from './dto/set-redit.dto';
import { RedisService } from './redis.service';

@Controller('redis')
@ApiTags('Redis')
@UseInterceptors(CacheInterceptor)
export class RedisController {
  constructor(private readonly redisService: RedisService) {}

  @Post()
  setKey(@Body() setRedisDto: SetRedisDto) {
    return this.redisService.setKey(setRedisDto);
  }

  @Get('/:key')
  getKey(@Param('key') key: string) {
    return this.redisService.getKey(key);
  }
}
