import { Module, CacheModule } from '@nestjs/common';
import { RedisController } from './redis.controller';
import { RedisService } from './redis.service';
import * as redisStore from 'cache-manager-redis-store';

@Module({
  imports: [CacheModule.register({ store: redisStore, host: 'localhost', port: 6379 })],
  controllers: [RedisController],
  providers: [RedisService],
  exports: [RedisService],
})
export class RedisModule {}
