import { Injectable, Inject, CACHE_MANAGER, NotFoundException } from '@nestjs/common';
import { Cache } from 'cache-manager';
import { SetRedisDto } from './dto/set-redit.dto';

@Injectable()
export class RedisService {
  constructor(@Inject(CACHE_MANAGER) private cacheService: Cache) {}

  async setKey(setRedisDto: SetRedisDto) {
    return await this.cacheService.set(setRedisDto.key, setRedisDto.value, { ttl: setRedisDto.ttl });
  }

  async getKey(key: string) {
    const value = await this.cacheService.get(key);
    if (!value) throw new NotFoundException('Value not found');
    return value;
  }
}
