import { Body, ClassSerializerInterceptor, Controller, Post, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/share/auth/guard/jwt.guard';
import { SmsService } from './sms.service';

@Controller('sms')
@ApiTags('Sms')
@UseInterceptors(ClassSerializerInterceptor)
export class SmsController {
  constructor(private readonly smsService: SmsService) {}

  @Post('test')
  @UseGuards(JwtAuthGuard)
  async sendOTP2StepVerification(@Body('phoneNumber') phoneNumber: string, @Body('otp') otp: string) {
    await this.smsService.sendOTP2StepVerification(phoneNumber, otp);
  }
}
