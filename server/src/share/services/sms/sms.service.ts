import { Injectable } from '@nestjs/common';
import { Twilio } from 'twilio';
import { config } from 'dotenv';
config();

@Injectable()
export class SmsService {
  private twilioClient: Twilio;
  constructor() {
    const accountSid = process.env.TWILIO_ACCOUNT_SID;
    const authToken = process.env.TWILIO_AUTH_TOKEN;

    this.twilioClient = new Twilio(accountSid, authToken);
  }

  sendOTP2StepVerification(phoneNumber: string, otp: string) {
    // const serviceSid = process.env.TWILIO_VERIFICATION_SERVICE_SID;

    const formatPhoneNumber = '+84' + phoneNumber.slice(1);

    return this.twilioClient.messages.create({
      body: `Your verification code is: ${otp}`,
      from: process.env.TWILIO_PHONE_NUMBER,
      to: formatPhoneNumber,
    });

    // return this.twilioClient.verify
    //   .services(serviceSid)
    //   .verifications.create({ to: phoneNumber, channel: 'sms', locale: 'en' });
  }
}
